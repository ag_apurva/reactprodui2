export const departmentData = `query departmentData($id: String!){
  departmentData(collegeId : $id){
    departmentName
    departmentCode
    error
  }
}`;

export const createDepartmentData = `mutation createDepartment($input: createDepartmentInput){
  createDepartment(input: $input){
    departmentName
    departmentCode
    error
  }
}`;

export const createAdmissionData = `mutation createAdmissionData($input: inputCreateAdmission){
  createAdmission(input: $input){
    admissionCode
    admissionYear
    departmentCode
    error
  }
}`;

export const createClassData = `mutation createClass($input: inputCreateClass){
  createClass(input: $input){
	idClass
	admissionCode
	subYear
	division
	semester
	quarter
	subDivision
	error
  }
}`;

export const createStudentMutation = `mutation createStudent($input: studentCreationInput){
  createStudent(input: $input){
	email
	name
	adminCode
	phoneNumber
	registrationNumber
	parentPhoneNumber
	rollNo
	dateOfbirth
	parentName
	address
	pincode
	bloodGroup
  error
  }
}`;

/*
	email: String
	name: String
	adminCode: String
	admissionYear: String
	departmentCode: String
	division: String
	phoneNumber: String
	registrationNumber: String
	parentPhoneNumber: String
	rollNo: String
	dateOfbirth: String
	parentName: String
	address: String
	pincode: String
	bloodGroup: String


*/
export const admissionData = `query admissionData($id: String!){
  admissionData(collegeId : $id){
    admissionCode
    admissionYear
    departmentCode
  }
}`;

export const classData = `query classData($id: String!){
  classData(collegeId: $id){
    idClass
    admissionCode
    subYear
    division
    semester
    quarter
    subDivision
    error
  }
}`;

export const deleteStudentMutation = `mutation deleteStudentMutation($input: studentDeletionChange){
  deleteStudentMutation(input:$input){
    email
  }
}`;

export const updateStudentMutation = `mutation updateStudent($input:studentUpdateInput){
  updateStudent(input:$input){
    email
    error
  }
}`;

export const mutationUpdateProfile = `mutation updateProfile($input: updateAdminProfile){
  updateProfile(input:$input){
    success
    error
  }
}`;

export const studentsData = `
query studentData($id: String!){
  studentData(collegeId: $id){
	email
	name
	phoneNumber
	registrationNumber
	parentPhoneNumber
	rollNo
	dateOfbirth
	parentName
	address
	pincode
	bloodGroup
	classId
	subYear
	semester
	division
  error
  UserCode
  }
}`;

export const scheduleDataRespectiveClassId = `
query scheduleDataRespectiveClassId($id: String!,$classId: String!){
  scheduleDataRespectiveClassId(collegeId: $id,classId: $classId){
    ClassSchMonId
    SchCode 
    ClassId
    UniCode
    Date
    StartTime
    EndTime
    SubjCode
    SubjCode
    SubjName
    ProfCode
    ProfName 
    Location
    Link
    LastUpdatedEpochTime
    ChangeId
    newStartTime
	newEndTime
	error
  }
}`;
export const scheduleDataUniversity = `query scheduleDataRespectiveUniversity($id: String!){
  scheduleDataRespectiveUniversity(collegeId:$id){
    Locations
    ProfNames
    Subject
    error
  }
}`;
/*
query scheduleDataRespectiveClassId{
  scheduleDataRespectiveClassId(classId:"13",collegeId:"TR021"){
    ClassSchMonId
    SchCode 
    ClassId
    UniCode
    Date
    StartTime
    EndTime
    SubjCode
    SubjCode
    SubjName
    ProfCode
    ProfName 
    Location
    Link
    LastUpdatedEpochTime
    ChangeId
    newStartTime
    newEndTime
  }
}


*/
