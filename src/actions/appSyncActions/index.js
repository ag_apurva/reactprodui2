import { API, graphqlOperation } from "aws-amplify";
import moment from "moment";
import axios from "axios";
import {
  createAdmissionData,
  createClassData,
  createStudentMutation,
  deleteStudentMutation,
  scheduleDataRespectiveClassId,
  updateStudentMutation,
} from "../../mutation";

export const createDepartment = (departmentData, departments) => {
  return async function (dispatch) {
    try {
      console.log("createDepartment", departmentData, departments);

      var dataInput = JSON.stringify({
        UniCode: departmentData.universityCode,
        DepartmentName: departmentData.departmentName,
        DepartmentHead: departmentData.departmentHead,
      });

      var config = {
        method: "post",
        url:
          "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/createdepartments",
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
          "Content-Type": "application/json",
        },
        data: dataInput,
      };

      let data = await axios(config);
      console.log("data", data);
      if (data.data.hasOwnProperty("error")) {
        console.log("error");
        return new Promise((resolve, reject) => reject(data.data.error));
      } else {
        console.log("success");

        dispatch({
          type: "DEPARTMENTS_DATA",
          payload: [
            ...departments,
            {
              departmentName: departmentData.departmentName,
              departmentCode: data.data.data,
              UniDeptHead: departmentData.departmentHead,
            },
          ],
        });
        return new Promise((resolve, reject) => resolve(""));
      }
    } catch (e) {
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 10000);
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const createAdmission = (admissionData, admissions) => {
  return async function (dispatch) {
    try {
      console.log("admissions", admissions);
      let createAdmission = {
        admissionYear: admissionData.admissionYear,
        departmentCode: admissionData.departmentCode,
        universityCode: admissionData.universityCode,
      };
      let admissionCheck = admissions.find((item) => {
        return (
          item.admissionYear === admissionData.admissionYear &&
          item.departmentCode === admissionData.departmentCode
        );
      });

      if (admissionCheck !== undefined) {
        setTimeout(function () {
          dispatch({ type: "REMOVE_ALERT", payload: "" });
        }, 10000);
        return new Promise((resolve, reject) =>
          reject("This AdmissionYear DepartmentCode exists ")
        );
      }
      const result = await API.graphql(
        graphqlOperation(createAdmissionData, { input: createAdmission })
      );
      if (result.data.createAdmission.error === null) {
        let admissionData = result.data.createAdmission;
        let admissionDataItem = {
          admissionCode: admissionData.admissionCode,
          admissionYear: admissionData.admissionYear,
          departmentCode: admissionData.departmentCode,
        };
        dispatch({ type: "ADD_ADMISSION", payload: admissionDataItem });
        return new Promise((resolve) => resolve("batchCreated"));
      } else {
        console.log("batch creation failed 1");
        setTimeout(function () {
          dispatch({ type: "REMOVE_ALERT", payload: "" });
        }, 10000);
        return new Promise((resolve, reject) =>
          reject(result.data.createAdmission.error)
        );
      }
    } catch (e) {
      console.log("batch creation failed 2", e);
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 10000);
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const createClass = (classData, classes) => {
  return async function (dispatch) {
    try {
      let classCheck = classes.find((item) => {
        return (
          item.admissionCode === classData.admissionCode &&
          item.subYear === classData.subYear &&
          item.division === classData.division &&
          item.semester === classData.semester &&
          item.subDivision === classData.subDivision
        );
      });
      if (classCheck !== undefined) {
        setTimeout(function () {
          dispatch({ type: "REMOVE_ALERT", payload: "" });
        }, 10000);
        return new Promise((resolve, reject) =>
          reject("This Configuration exists ")
        );
      }

      const createClass = {
        admissionCode: classData.admissionCode,
        subYear: classData.subYear,
        division: classData.division,
        semester: classData.semester,
        quarter: classData.quarter,
        subDivision: classData.subDivision,
      };
      /**/

      const result = await API.graphql(
        graphqlOperation(createClassData, { input: createClass })
      );
      console.log("result", result);

      if (result.data.createClass.error === null) {
        let ClassDataItem = {
          idClass: result.data.createClass.idClass,
          admissionCode: result.data.createClass.admissionCode,
          subYear: result.data.createClass.subYear,
          division: result.data.createClass.division,
          semester: result.data.createClass.semester,
          quarter: result.data.createClass.quarter,
          subDivision: result.data.createClass.subDivision,
        };
        dispatch({ type: "ADD_CLASS", payload: ClassDataItem });
      } else {
        console.log("class creation failed 1");
        setTimeout(function () {
          dispatch({ type: "REMOVE_ALERT", payload: "" });
        }, 10000);
        return new Promise((resolve, reject) =>
          reject(result.data.createClass.error)
        );
      }
    } catch (e) {
      console.log("class creation failed 2", e);
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 10000);
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const createStudent = (studentData, classes) => {
  return async function (dispatch) {
    try {
      let classData2 = classes.find((item) => {
        return (
          item.admissionCode === studentData.admissionCode &&
          item.division === studentData.division &&
          item.semester === studentData.semester
        );
      });
      console.log("classData2", classData2.idClass, classData2.subYear);
      let studentDataitem = {
        email: studentData.email,
        name: studentData.name,
        phoneNumber: studentData.phoneNumber,
        registrationNumber: studentData.registrationNumber,
        parentPhoneNumber: studentData.parentPhoneNumber,
        rollNo: studentData.rollNo,
        dateOfbirth: studentData.dateOfbirth,
        parentName: studentData.parentName,
        address: studentData.address,
        pincode: studentData.pincode,
        bloodGroup: studentData.bloodGroup,
        classId: classData2.idClass,
        subYear: classData2.subYear,
        semester: studentData.semester,
        division: studentData.division,
        error: null,
        UserImage: "",
      };

      let studentdata = {
        email: studentData.email,
        name: studentData.name,
        adminCode: studentData.adminCode,
        phoneNumber: studentData.phoneNumber,
        registrationNumber: studentData.registrationNumber,
        parentPhoneNumber: studentData.parentPhoneNumber,
        rollNo: studentData.rollNo,
        dateOfbirth: studentData.dateOfbirth,
        parentName: studentData.parentName,
        address: studentData.address,
        pincode: studentData.pincode,
        bloodGroup: studentData.bloodGroup,
        admissionCode: studentData.admissionCode,
        semester: studentData.semester,
        division: studentData.division,
      };
      //console.log("studentData", studentData);

      const result = await API.graphql(
        graphqlOperation(createStudentMutation, { input: studentdata })
      );
      console.log("result", result);

      if (result.data.createStudent.error === null) {
        dispatch({ type: "ADD_STUDENTS", payload: studentDataitem });
        return new Promise((resolve) => resolve("studentCreated"));
      } else {
        console.log("class creation failed 1");
        setTimeout(function () {
          dispatch({ type: "REMOVE_ALERT", payload: "" });
        }, 10000);
        return new Promise((resolve, reject) =>
          reject(result.data.createStudent.error)
        );
      }
    } catch (e) {
      console.log("student creation failed 2", e);
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 10000);
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const deleteStudent = (code, email, students) => {
  return async function (dispatch) {
    try {
      let studentDelete = {
        email,
        code,
      };
      const result = await API.graphql(
        graphqlOperation(deleteStudentMutation, { input: studentDelete })
      );
      console.log("result", result);

      students = students.filter((item) => {
        return item.email !== email;
      });

      dispatch({
        type: "STUDENTS_DATA",
        payload: students,
      });

      console.log("new studnets", students);
      return new Promise((resolve) => {
        resolve("student deleted");
      });
    } catch (e) {
      console.log("error delete student", e);
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);

      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

export const updateStudent = (formvalues, students) => {
  return async function (dispatch) {
    const elementsIndex = students.findIndex(
      (element) => element.email === formvalues.username
    );

    let studentObject = students[elementsIndex];
    studentObject.address = formvalues.address;
    studentObject.bloodGroup = formvalues.bloodGroup;
    studentObject.name = formvalues.name;
    studentObject.parentName = formvalues.parentName;
    studentObject.phoneNumber = "+" + formvalues.phone_number;
    studentObject.parentPhoneNumber = "+" + formvalues.parentPhoneNumber;
    studentObject.pincode = formvalues.pincode;
    studentObject.registrationNumber = formvalues.registrationNumber;
    studentObject.dateOfbirth = new Date(formvalues.userdob);
    studentObject.rollNo = formvalues.rollNo;
    students[elementsIndex] = studentObject;

    console.log("studentObject", studentObject);

    try {
      let inputUpdateStudent = {
        studentName: studentObject.name,
        studentRollNo: studentObject.rollNo,
        studentEmail: studentObject.email,
        studentRegistrationNumber: studentObject.registrationNumber,
        studentPhoneNumber: "+" + formvalues.phone_number,
        studentDoB: moment(studentObject.dateOfbirth).format("YYYY-MM-DD"),
        parentPhoneNumber: studentObject.parentPhoneNumber,
        parentName: studentObject.parentName,
        adminCode: formvalues.UniCode,
        address: studentObject.address,
        pincode: studentObject.pincode,
        bloodGroup: studentObject.bloodGroup,
      };
      const result = await API.graphql(
        graphqlOperation(updateStudentMutation, {
          input: inputUpdateStudent,
        })
      );
      console.log("successfull student update", result.data.updateStudent);
      if (result.data.updateStudent.error === null) {
        dispatch({
          type: "STUDENTS_DATA",
          payload: students,
        });
        try {
          let config = {
            method: "get",
            params: {
              universityCode: formvalues.UniCode,
            },
            url:
              "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/contacts/getcontacts",
            headers: {},
          };

          let contacts = await axios(config);
          console.log("Contacts", contacts.data);
          dispatch({
            type: "CONTACTS_DATA",
            payload: contacts.data,
          });
        } catch (e) {
          console.log("Error occured while fetching contacts after update", e);
        }
        return new Promise((resolve, reject) =>
          resolve(" Student updated successfully")
        );
      } else {
        return new Promise((resolve, reject) =>
          reject(result.data.updateStudent.error)
        );
      }
      // this.props.updateStudent(result.data.updateStudent);

      // this.props.addConfirmPopUp();
    } catch (e) {
      console.log("result error in student delete", e);
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }

    /*

address: "address"
bloodGroup: "C-"
name: "ArunGhata"
parentName: "Arun"
parentPhoneNumber: "1122334468"
phone_number: "1122334668"
pincode: "123456"
registrationNumber: "1234"
rollNo: "1234"
userdob: "2021-04-16"
username: "arung@innocurve.com"



address: "address"
bloodGroup: "A-"
classId: "13"
dateOfbirth: "1988-09-10T00:00:00.000Z"
division: "D"
email: "adityab@innocurve.com"
error: null
name: "Adiyta"
parentName: "1234"
parentPhoneNumber: "+1122334455"
phoneNumber: "+1122334455"
pincode: "123456"
registrationNumber: "1234"
rollNo: "1234"
semester: "8"
subYear: "4"


    */
  };
};

export const loadClassData = (formValues) => {
  return async function (dispatch) {
    try {
      console.log("formValues", formValues);
      dispatch({ type: "REMOVE_ALERT", payload: "" });
      const studentDataResult = await API.graphql(
        graphqlOperation(scheduleDataRespectiveClassId, {
          id: formValues.UniCode,
          classId: formValues.classId,
        })
      );
      console.log("studentDataResult", studentDataResult);
      if (studentDataResult.data.scheduleDataRespectiveClassId.length === 0) {
        return new Promise((resolev, reject) => {
          reject("No data Found for Class");
        });
      }
      if (
        studentDataResult.data.scheduleDataRespectiveClassId[0].error === null
      ) {
        return new Promise((resolve, reject) => {
          resolve(studentDataResult.data.scheduleDataRespectiveClassId);
        });
      } else {
        return new Promise((resolve, reject) => {
          reject(studentDataResult.data.scheduleDataRespectiveClassId[0].error);
        });
      }
    } catch (e) {
      console.log("error studentDataResult", e);
      return new Promise((resolve, reject) => {
        reject("No data Found for Class");
      });
    }
  };
};

// export const updateProfileData = (updateProfileObj, userData) => {
//   return async function (dispatch) {
//     try {
//       console.log("formValues222", updateProfileObj);
//       let newObject = {
//         UserAddress: updateProfileObj["​UserAddress"],
//         UserDOB: updateProfileObj["​UserDOB"],
//         UserParentName: updateProfileObj["​UserParentName"],
//         UserPhone: updateProfileObj["​UserPhone"],
//       };
//       let updatedProfileObj = {
//         ...userData,
//         ...updateProfileObj,
//         ...newObject,
//       };

//       let input2 = {
//         userCode: updatedProfileObj.UserCode,
//         universityCode: updatedProfileObj.UniCode,
//         name: updatedProfileObj.UserName,
//         email: updatedProfileObj.UserEmail,
//         user_dob:
//           moment(updatedProfileObj["​UserDOB"]).format("YYYY-MM-DD") || "",
//         phone_number: updatedProfileObj["​UserPhone"],
//         parentName: updatedProfileObj["​UserParentName"],
//         parentPhoneNumber: updatedProfileObj.UserParentContactNo,
//         pincode: updatedProfileObj.UserPinCode,
//         designation: updatedProfileObj.UserDesignation,
//         address: updatedProfileObj.UserAddress,
//         bloodGroup: updatedProfileObj.BloodGroup,
//       };

//       console.log("input2", input2);
//       const result = await API.graphql(
//         graphqlOperation(mutationUpdateProfile, { input: input2 }) //mutationUpdateProfile, { input: input2 })
//       );
//       console.log("result", result);

//       if (result.data.updateProfile.error === null) {
//         dispatch({
//           type: "LOGIN_SUCCESS",
//           payload: { ...userData, ...updateProfileObj, ...newObject },
//         });
//         return new Promise((res) => res("Profile Saved"));
//       } else {
//         return new Promise((res, rej) => rej(result.data.updateProfile.error));
//       }
//       // console.log("{ ...userData, ...formValues }", {
//       //   ...userData,
//       //   ...updateProfileObj,
//       //   ...newObject,
//       // });
//     } catch (e) {
//       console.log("error updating profile", e);
//       return new Promise((res, rej) => rej(JSON.stringify(e)));
//     }
//   };
// };

// export const createDepartment = (departmentData, departments) => {
//   return async function (dispatch) {
//     try {
//       console.log("createDepartment", departmentData, departments);
//       let createDepartment = {
//         universityCode: departmentData.universityCode,
//         departmentName: departmentData.departmentName,
//       };
//       let checkDepartment = departments.find((item) => {
//         return item.departmentName === departmentData.departmentName;
//       });

//       if (checkDepartment !== undefined) {
//         setTimeout(function () {
//           dispatch({ type: "REMOVE_ALERT", payload: "" });
//         }, 10000);
//         return new Promise((resolve, reject) => {
//           return reject("department name exists");
//         });
//       }
//       const result = await API.graphql(
//         graphqlOperation(createDepartmentData, { input: createDepartment })
//       );

//       if (result.data.createDepartment.error === null) {
//         let departmentData = result.data.createDepartment;
//         dispatch({
//           type: "ADD_DEPARTMENT",
//           payload: {
//             departmentCode: departmentData.departmentCode,
//             departmentName: departmentData.departmentName,
//           },
//         });
//         return new Promise((resolve) => resolve("departmentCreated"));
//       } else {
//         setTimeout(function () {
//           dispatch({ type: "REMOVE_ALERT", payload: "" });
//         }, 10000);
//         return new Promise((resolve, reject) =>
//           reject(result.data.createDepartment.error)
//         );
//       }
//     } catch (e) {
//       setTimeout(function () {
//         dispatch({ type: "REMOVE_ALERT", payload: "" });
//       }, 10000);
//       return new Promise((resolve, reject) => reject(JSON.stringify(e)));
//     }
//   };
// };
