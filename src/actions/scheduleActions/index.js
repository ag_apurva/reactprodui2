import { Functions } from "@material-ui/icons";
import axios from "axios";

import {
  DELETE_PROFESSOR_URL,
  UPDATE_PROFESSOR_URL,
  HELP_PUT_FAQ,
  DELETE_FAQ,
  UPDATE_FAQ,
  CREATE_PROFESSOR_URL,
  DELETE_SCHEDULE_URL,
  DELETE_CONTACT_URL,
  CREATE_CONTACT_URL,
  UPDATE_CONTACT_URL,
} from "../reducerActionsConsts";

export const checkConflict = (data) => {
  return async function (dispatch) {
    var dataInput = JSON.stringify(data);

    var config = {
      method: "post",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/checkforconflict",
      headers: {
        "Content-Type": "application/json",
      },
      data: dataInput,
    };
    try {
      let data = await axios(config);
      console.log("check conflict data11111111", data);
      if (data.data.success == "No Overlaps") {
        console.log("check conflict data2222222", data);
        // dispatch(
        //   firebase("Class Timing Changed", JSON.parse(data.data.changeDetails))
        // );
      }
      return data;
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

//this is action which loads the default values in professor screen.
export const loadProfessorData = (professorData) => {
  return async (dispatch) => {
    dispatch({
      type: "PROFESSOR_DATA_EDIT",
      payload: professorData,
    });
  };
};

export const deleteProfessor = (UniCode, UserEmail, allProfessors) => {
  return async (dispatch) => {
    try {
      let config = {
        method: "delete",
        params: {
          userEmail: UserEmail,
          universityCode: UniCode,
        },
        url: DELETE_PROFESSOR_URL,
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
      };

      try {
        let data = await axios(config);
        // console.log("delete data", data);
        if (data.data.hasOwnProperty("error")) {
          return new Promise((resolve, reject) => {
            reject(data.data.error);
          });
        } else {
          // now remove the element from the professor array with the email of deleted Professor
          const newProfessorList = allProfessors.filter((item) => {
            if (item.UserEmail === UserEmail) {
              return false;
            } else {
              return true;
            }
          });

          dispatch({
            type: "PROFESSORS_DATA",
            payload: newProfessorList,
          });

          return new Promise((resolve, reject) => {
            resolve(data.data.data);
          });
        }
      } catch (e) {
        return new Promise((resolve, reject) => {
          reject(JSON.stringify(e));
        });
      }
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

export const updateProfessor = (professorData) => {
  return async (dispatch) => {
    dispatch({
      type: "PROFESSOR_DATA_EDIT",
      payload: professorData,
    });
  };
};

export const createSchedule = (data) => {
  return async function (dispatch) {
    var dataInput = JSON.stringify(data);

    var config = {
      method: "post",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/schedule/createschedule",
      headers: {
        "Content-Type": "application/json",
      },
      data: dataInput,
    };
    try {
      let yy = await axios(config);
      console.log("create Schedule Data", yy);
      return new Promise((resolve, reject) => {
        resolve(yy);
      });
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

export const putFaq = (formData) => {
  return async function (dispatch) {
    let newFAQObject = {};
    newFAQObject["Category"] = formData["Category"];
    newFAQObject["Answer"] = formData["Answer"];
    newFAQObject["Question"] = formData["Question"];
    newFAQObject["UniCode"] = formData["UniCode"];

    let dataInput = JSON.stringify(newFAQObject);
    //dispatch({ type: "PUT_FAQ", payload: newFAQObject });
    let config = {
      method: "post",
      url: HELP_PUT_FAQ,
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      data: dataInput,
    };
    // return new Promise((resolve, reject) => {
    //   resolve("data");
    // });
    try {
      let data = await axios(config);
      //  console.log("data.data.data", data);
      newFAQObject["idUniFAQ"] = data.data.data; // since in case its successfull it returns the new id
      if (data.data.hasOwnProperty("error")) {
        //console.log("error", data.data.error);
        return new Promise((resolve, reject) => {
          reject(data.data.error);
        });
      } else {
        dispatch({
          type: "PUT_FAQ",
          payload: newFAQObject,
        });
        return new Promise((resolve, reject) => {
          resolve(data.data.data);
        });
      }
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

export const deleteFaq = (faqId, faqs, UniCode) => {
  return async function (dispatch) {
    const newFaqs = faqs.filter((item) => {
      return item.idUniFAQ != faqId;
    });
    //  console.log("faqId, faqs, UniCode", faqId, UniCode);
    try {
      let config = {
        method: "delete",
        url: DELETE_FAQ,
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        params: {
          faqId: faqId,
          universityCode: UniCode,
        },
      };

      let data = await axios(config);
      //   console.log("data", data);
      if (data.hasOwnProperty("error")) {
        return new Promise((resolve, reject) => reject(data.error));
      } else {
        dispatch({ type: "FAQ_DATA", payload: newFaqs });
        return new Promise((resolve, reject) => resolve());
      }
    } catch (e) {
      //   console.log("error", e);
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const updateFaq = (formData, faqId, UniCode, faqs) => {
  return async function (dispatch) {
    try {
      const newFaqsIndex = faqs.findIndex((item) => {
        return item.idUniFAQ === faqId;
      });

      faqs[newFaqsIndex]["Question"] = formData["question"];
      faqs[newFaqsIndex]["Answer"] = formData["answer"];
      let newFaQs = JSON.parse(JSON.stringify(faqs));
      let dataInput = JSON.stringify({
        Question: formData["question"],
        Answer: formData["answer"],
        UniCode: UniCode,
        faqId: faqId,
      });
      try {
        let config = {
          method: "put",
          url: UPDATE_FAQ,
          headers: {
            "Content-Type": "application/json",
            "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
          },
          data: dataInput,
        };

        let data = await axios(config);
        if (data.hasOwnProperty("error")) {
          return new Promise((resolve, reject) => reject(data.error));
        } else {
          dispatch({ type: "FAQ_DATA", payload: newFaQs });
          return new Promise((resolve, reject) => resolve());
        }
      } catch (e) {
        return new Promise((resolve, reject) => reject(JSON.stringify(e)));
      }
    } catch (e) {
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const loadFAQData = (data) => {
  return async function (dispatch) {
    // console.log("loadFAQData", data);
    dispatch({ type: "LOAD_FAQ_DATA", payload: data });
  };
};

export const editTeacher = (formData, professors, UniCode) => {
  return async (dispatch) => {
    let index = professors.findIndex((item) => {
      return item.UserEmail == formData.email;
    });
    let newProfObject = professors[index];

    newProfObject["UserName"] = formData["name"];
    newProfObject["UserPhone"] = "+" + formData["phone_number"]; //userdob
    newProfObject["UserDOB"] = formData["userdob"]; //userdob
    newProfObject["UserParentContactNo"] =
      "+" + formData["primaryContactNumber"];
    newProfObject["UserParentName"] = formData["primaryContactName"];
    if (newProfObject.hasOwnProperty("UserAddress")) {
      newProfObject.UserAddress = formData["address"];
    }
    if (newProfObject.hasOwnProperty("UserPinCode")) {
      newProfObject["UserPinCode"] = formData["pincode"].toString();
    }

    if (newProfObject.hasOwnProperty("​UserRegistrationNo")) {
      newProfObject.UserRegistrationNo = parseInt(formData.registrationNumber);
    }

    newProfObject["BloodGroup"] = formData["bloodGroup"];

    let dataInput = JSON.stringify(newProfObject);

    let config = {
      method: "put",
      url: UPDATE_PROFESSOR_URL,
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      data: dataInput,
    };
    try {
      let data = await axios(config);
      // console.log("data", data);

      if (data.data.hasOwnProperty("error")) {
        //console.log("error", data.data.error);
        return new Promise((resolve, reject) => {
          reject(data.data.error);
        });
      } else {
        //console.log("success", data.data.data);

        professors[index] = newProfObject;
        dispatch({
          type: "PROFESSORS_DATA",
          payload: JSON.parse(JSON.stringify(professors)),
        });

        try {
          let config = {
            method: "get",
            params: {
              universityCode: UniCode,
            },
            url:
              "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/contacts/getcontacts",
            headers: {},
          };

          let contacts = await axios(config);
          console.log("Contacts", contacts.data);
          dispatch({
            type: "CONTACTS_DATA",
            payload: contacts.data,
          });
        } catch (e) {
          console.log("Error occured while fetching contacts after update", e);
        }
        return new Promise((resolve, reject) => {
          resolve(data.data.data);
        });
      }
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

export const createProfessor = (formData, professors) => {
  return async (dispatch) => {
    let newProfObject = {};

    newProfObject["UserName"] = formData["name"];
    newProfObject["UserPhone"] = "+" + formData["phone_number"]; //userdob
    newProfObject["UserDOB"] = formData["userdob"]; //email
    newProfObject["UserEmail"] = formData["email"]; //email
    newProfObject["UserParentContactNo"] =
      "+" + formData["primaryContactNumber"];
    newProfObject["UserParentName"] = formData["primaryContactName"];

    newProfObject["UserAddress"] = formData["address"];
    newProfObject["UserPinCode"] = formData["pincode"];
    newProfObject["UserRegistrationNo"] =
      parseInt(formData.registrationNumber) || 0;
    newProfObject["BloodGroup"] = formData["bloodGroup"];
    newProfObject["UniCode"] = formData["UniCode"];

    let dataInput = JSON.stringify(newProfObject);

    let config = {
      method: "post",
      url: CREATE_PROFESSOR_URL,
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      data: dataInput,
    };
    try {
      let data = await axios(config);
      console.log("User UserCode", data.data.data);

      if (data.data.hasOwnProperty("error")) {
        return new Promise((resolve, reject) => {
          reject(data.data.error);
        });
      } else {
        newProfObject["UniCode"] = formData["UniCode"];
        newProfObject["UserCode"] = data.data.data;
        newProfObject["UserImage"] = "";
        professors.push(newProfObject);
        console.log("Professors", professors);
        dispatch({
          type: "PROFESSORS_DATA",
          payload: JSON.parse(JSON.stringify(professors)),
        });
        return new Promise((resolve, reject) => {
          resolve("data.data.data");
        });
      }
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject(JSON.stringify(e));
      });
    }
  };
};

export const firebase = (title, detailsObject) => {
  return async function (dispatch) {
    console.log("fireBase called", detailsObject);

    const {
      DataChanged,
      DateOld,
      DateNew,
      endTimeChanged,
      endTimeNew,
      endTimeOld,
      professorChanged,
      professorOld,
      professorNew,
      startTimeChanged,
      startTimeNew,
      startTimeOld,
      subjectChanged,
      subjectOld,
      subjectNew,
    } = detailsObject;
    let newTime = startTimeChanged || endTimeChanged;
    let changeBody = [
      "Class on",
      DateOld,
      "from",
      startTimeOld,
      endTimeOld,
      "changed to ",
      DataChanged ? `Class  on  ${DateNew}` : `Class  on  ${DateOld}`,
      newTime ? "from" : "",
      newTime ? (startTimeChanged ? startTimeNew : startTimeOld) : "",
      newTime ? (endTimeChanged ? endTimeNew : endTimeOld) : "",
      subjectChanged ? `to Subject ${subjectNew}` : "",
      professorChanged ? `of Professor ${professorNew}` : "",
    ];
    console.log("changeBody", changeBody);
    /*
DataChanged: 0
DateOld: "2021-06-08"
endTimeChanged: 0
endTimeOld: "08:41"
professorChanged: 0
professorOld: "Manish V"
startTimeChanged: 1
startTimeNew: "07:20"
startTimeOld: "07:26"
subjectChanged: 0
subjectOld: "Computer Science"



    */
    let data = JSON.stringify({
      to: "/topics/schedule",
      data: {
        title: title,
        body: changeBody.join(" "),
      },
    });

    let config = {
      method: "post",
      url: "https://fcm.googleapis.com/fcm/send",
      headers: {
        Authorization:
          "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      let data = await axios(config);
      console.log("data sent to fireBase", data);
    } catch (e) {
      console.log("error  sending to fireBase", e);
    }
  };
};

export const deleteSchedule = (ClassId, UniCode, DateNew, ClassSchMonId) => {
  return async function (dispatch) {
    let config = {
      method: "delete",
      params: {
        ClassId: ClassId,
        UniCode: UniCode,
        DateNew: DateNew,
        ClassSchMonId: ClassSchMonId,
      },
      url: DELETE_SCHEDULE_URL,
      headers: {
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
    };

    try {
      let data = await axios(config);
      console.log("delete schedule", data);
      return new Promise((resolve, reject) => {
        return resolve(data);
      });
    } catch (e) {
      console.log("error deleting schedule", e);
    }
  };
};

export const deleteContact = (contacts, contact) => {
  return async function (dispatch) {
    try {
      let config = {
        method: "delete",
        params: {
          idUniContact: contact.idUniContacts,
          universityCode: contact.UniCode,
        },
        url: DELETE_CONTACT_URL,
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
      };
      console.log("contact", contact);
      let data = await axios(config);
      console.log("data", data);
      if (data.data.hasOwnProperty("error")) {
        return new Promise((resolve, reject) => reject(data.data.error));
      } else {
        console.log("deleteContact", contacts);
        let newContacts = contacts.filter((item) => {
          return item.idUniContacts != contact.idUniContacts;
        });
        console.log("newContacts", newContacts);
        dispatch({
          type: "CONTACTS_DATA",
          payload: newContacts,
        });
        return new Promise((resolve, reject) => resolve(""));
      }
    } catch (e) {
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const updateContact = (contacts, contact) => {
  return async function (dispatch) {
    try {
      let dataInput = JSON.stringify(contact);
      let config = {
        method: "put",
        url: UPDATE_CONTACT_URL,
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        data: dataInput,
      };
      let data = await axios(config);
      console.log("update contact", data);
      if (data.data.hasOwnProperty("error")) {
        return new Promise((resolve, reject) => reject(data.data.error));
      } else {
        let contactIndex = contacts.findIndex((item) => {
          return item.idUniContacts == contact.idUniContact;
        });
        contacts[contactIndex] = { ...contacts[contactIndex], ...contact };
        dispatch({
          type: "CONTACTS_DATA",
          payload: JSON.parse(JSON.stringify(contacts)),
        });
        return new Promise((resolve, reject) => resolve(""));
      }
    } catch (e) {
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const createContact = (contacts, contact) => {
  return async function (dispatch) {
    try {
      let dataInput = JSON.stringify(contact);
      let config = {
        method: "post",
        url: CREATE_CONTACT_URL,
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        data: dataInput,
      };

      let data = await axios(config);
      console.log("data create", data);
      if (data.data.hasOwnProperty("error")) {
        return new Promise((resolve, reject) => reject(data.data.error));
      } else {
        console.log("data.data.data", data.data.data);
        contact["idUniContacts"] = data.data.data;
        contacts.push(contact);
        dispatch({
          type: "CONTACTS_DATA",
          payload: JSON.parse(JSON.stringify(contacts)),
        });
        return new Promise((resolve, reject) => resolve(""));
      }
    } catch (e) {
      return new Promise((resolve, reject) => reject(JSON.stringify(e)));
    }
  };
};

export const createLocation = (datainput, locations) => {
  return async function (dispatch) {
    try {
      let config = {
        method: "post",
        url:
          "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/locations/createlocations",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        data: JSON.stringify(datainput),
      };
      let data = await axios(config);
      console.log("locations before", locations);
      datainput["UniClassRoomId"] = locations.length + 1;
      locations.push(datainput);
      console.log("locations after", locations);
      console.log("Data create Location", data);
      dispatch({
        type: "LOCATIONS",
        payload: JSON.parse(JSON.stringify(locations)),
      });
      return new Promise((resolve, reject) => resolve(""));
    } catch (e) {
      console.log("Error create location", e);
      return new Promise((resolve, reject) => reject(""));
    }
  };
};

export const createSubject = (datainput, subjects) => {
  return async function (dispatch) {
    try {
      let config = {
        method: "post",
        url:
          "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/subjects/createsubjects",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        data: JSON.stringify(datainput),
      };
      let data = await axios(config);
      console.log("subjects before", subjects);
      datainput["SubjCode"] = data.data.data;
      datainput["SubjName"] = datainput["Subject"];
      subjects.push(datainput);
      console.log("subjects after", subjects);
      console.log("Data create Location", data);
      dispatch({
        type: "SUBJECTS",
        payload: JSON.parse(JSON.stringify(subjects)),
      });
      return new Promise((resolve, reject) => resolve(""));
    } catch (e) {
      console.log("Error create location", e);
      return new Promise((resolve, reject) => reject(""));
    }
  };
};

export const saveMyProfile = (dataInput, userData) => {
  return async function (dispatch) {
    let config = {
      method: "put",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/savemyprofile",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      data: JSON.stringify(dataInput),
    };
    dispatch({
      type: "LOGIN_SUCCESS",
      payload: { ...userData, ...dataInput },
    });
    /// console.log("{ ...userData,...dataInput }", { ...userData, ...dataInput });
    try {
      let data = await axios(config);
      console.log("data", data);
      return new Promise((resolve, reject) =>
        resolve("data updated successfully")
      );
    } catch (e) {
      console.log("error update profile", JSON.stringify(e));
      return new Promise((resolve, reject) => reject(e));
    }
  };
};
