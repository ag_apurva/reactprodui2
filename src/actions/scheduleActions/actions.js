import { Functions } from "@material-ui/icons";
import { Auth, API, graphqlOperation, Storage } from "aws-amplify";
import {
  departmentData,
  admissionData,
  classData,
  studentsData,
  scheduleDataUniversity,
} from "../../mutation";
import axios from "axios";

import { LOCATIONS, SUBJECTS } from "../reducerActionsConsts";

export const getSubjects = (code) => {
  return async function (dispatch) {
    let config = {
      method: "get",
      url: SUBJECTS,
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      params: {
        universityCode: code,
      },
    };

    try {
      let data = await axios(config);
      console.log("Data Subjects", data.data);
      dispatch({
        type: "SUBJECTS",
        payload: data.data,
      });
    } catch (e) {
      console.log("error retriving Subjects", e);
    }
  };
};

export const getLocations = (code) => {
  return async function (dispatch) {
    let config = {
      method: "get",
      url: LOCATIONS,
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      params: {
        universityCode: code,
      },
    };

    try {
      let data = await axios(config);
      console.log("Data Locations", data.data);
      dispatch({
        type: "LOCATIONS",
        payload: data.data,
      });
    } catch (e) {
      console.log("error retriving Locations", e);
    }
  };
};

export const getDepartments = (code) => {
  return async function (dispatch) {
    var config = {
      method: "get",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/getdepartments?universityCode=TR021",
      headers: {
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      params: {
        universityCode: code,
      },
    };
    try {
      let data = await axios(config);
      console.log("data departments", data);
      dispatch({
        type: "DEPARTMENTS_DATA",
        payload: data.data,
      });
    } catch (e) {
      dispatch({
        type: "DEPARTMENTS_DATA",
        payload: [],
      });
    }
  };
};

export const getClasses = (code) => {
  return async function (dispatch) {
    const classDataResult = await API.graphql(
      graphqlOperation(classData, { id: code })
    );
    dispatch({
      type: "CLASS_DATA",
      payload: classDataResult.data.classData,
    });
  };
};
