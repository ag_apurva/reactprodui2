import history from "../../history";
import { Auth, API, graphqlOperation, Storage } from "aws-amplify";
import {
  departmentData,
  admissionData,
  classData,
  studentsData,
  scheduleDataUniversity,
} from "../../mutation";
import axios from "axios";
import {
  ID_TOKEN_COGNITO,
  ACCESS_TOKEN_COGNITO,
  REFRESH_TOKEN_COGNITO,
  ERROR_OCCURED,
  FETCH_TEACHERS_DATA_URL,
  HELP_FAQ_URL,
} from "../reducerActionsConsts";
import {
  getLocations,
  getSubjects,
  getDepartments,
  getClasses,
} from "../scheduleActions/actions";
import { batchActions, asyncAdmissionsData } from "../../reducers/batch";
export const getData = async function (userEmail, userCode, idToken) {
  var config = {
    method: "get",
    params: {
      email: userEmail,
      code: userCode,
    },
    url: "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/userinfo",
    headers: {
      "x-api-key": idToken,
    },
  };

  try {
    return await axios(config);
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
};

export const getTeachersData = async function (uniCode, idToken) {
  var config = {
    method: "get",
    params: {
      universityCode: uniCode,
    },
    url: FETCH_TEACHERS_DATA_URL,
  };

  try {
    let data = await axios(config);

    return new Promise((resolve, reject) => {
      resolve(data);
    });
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
};

export const getDataAfterSignUp = async function (userEmail, userCode) {
  var config = {
    method: "post",
    params: {
      email: userEmail,
      code: userCode,
    },
    url:
      "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/userinfoaftersignup",
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    return await axios(config);
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
};

async function getDataAnotherUser(userEmail) {
  var config = {
    method: "get",
    params: {
      email: userEmail,
    },
    url:
      "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/anotheruserrequest",
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    return await axios(config);
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
}

async function getDataAnotherUserSubmit(formData) {
  var dataInput = JSON.stringify(formData);
  var config = {
    method: "post",
    url:
      "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/anotherusersubmit",
    headers: {
      "Content-Type": "application/json",
    },
    data: dataInput,
  };
  try {
    return await axios(config);
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
}

/*
 these are basically api actions.

 getData()  is the basicallyy the api which gets called to verify the university code and email,
 with the email id.

 getDataAnotherUser()  when we forgot univeristy codes or if we want to register a user more than once,
 this API is called.

 getDataAnotherUserSubmit()
 this api is called when a user which is already registered tried to re register itself for another university.

*/

//actions
export const loginAction = (formData) => {
  return async function (dispatch) {
    let errorCode;
    try {
      const user = await Auth.signIn(formData.username, formData.password, {
        UniCode: formData.code,
      });
      const { accessToken, idToken, refreshToken } = user.signInUserSession;
      console.log("accessToken", accessToken);
      console.log("idToken", idToken);
      dispatch({ type: ID_TOKEN_COGNITO, payload: idToken.jwtToken });
      dispatch({ type: ACCESS_TOKEN_COGNITO, payload: accessToken.jwtToken });
      dispatch({ type: REFRESH_TOKEN_COGNITO, payload: refreshToken.token });
      if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
        history.push({
          pathname: "/student",
          state: { email: formData.username, password: formData.password },
        });
      }
      try {
        var apiDetails = await getData(
          formData.username,
          formData.code,
          idToken.jwtToken
        );
        console.log("apiDetails", apiDetails);
      } catch (e) {
        console.log("error occured ", e);
        errorCode = e["response"].status;
        if (errorCode === 401) {
          await Auth.signOut();
          let error = { message: "Code is incorrect" };
          throw error;
        } else if (errorCode === 400) {
          await Auth.signOut();
          let error = { message: "Code is incorrect" };
          throw error;
        } else {
          throw e;
        }
      }
      localStorage.setItem("universityCode", formData.code);
      dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
      dispatch({ type: "IS_AUTHENTICATED", payload: true });
      dispatch(loadDepartmentsData(formData.code));
      dispatch(getLocations(formData.code));
      dispatch(getSubjects(formData.code));
      dispatch(getDepartments(formData.code));
      dispatch(getClasses(formData.code));
      let path =
        apiDetails.data.Uni_Code +
        "/" +
        apiDetails.data.UserCode +
        "/profileImage.jpg";
      console.log("this.props profilephoto path to download from s3", path);
      dispatch(downLoadImage(path));
      if (user.challengeName !== "NEW_PASSWORD_REQUIRED") {
        history.push("/welcome");
      }
    } catch (e) {
      dispatch({
        type: "LOGIN_FAILED",
        payload: e.message || e,
      });
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);
      return new Promise((res, rej) => rej(e.message));
    }
  };
};

export const load = (data) => ({ type: "LOAD", data });

//export const loadTaskDetails = (data) => { }({ type: "TASK_DETAILS", data });

export const loadStudentData = (data) => ({ type: "LOAD_STUDENT", data });

//this shold be renamed load data
export const loadDepartmentsData = (code) => {
  return async function (dispatch) {
    const departmentsDataResult = await API.graphql(
      graphqlOperation(departmentData, { id: code })
    );

    // const admissionDataResult = await API.graphql(
    //   graphqlOperation(admissionData, { id: code })
    // );
    // const classDataResult = await API.graphql(
    //   graphqlOperation(classData, { id: code })
    // );
    // dispatch({ type: "CLASS_DATA", payload: classDataResult.data.classData });
    const studentDataResult = await API.graphql(
      graphqlOperation(studentsData, { id: code })
    );

    const scheduleDataUniversityResult = await API.graphql(
      graphqlOperation(scheduleDataUniversity, { id: code })
    );

    try {
      try {
        let config = {
          method: "get",
          params: {
            universityCode: code,
            searchKeyword: "",
          },
          url:
            "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/help/getfaq",
          headers: {
            "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
          },
        };
        let faqData = await axios(config);
        console.log("faqData", faqData.data);
        dispatch({
          type: "FAQ_DATA",
          payload: faqData.data,
        });
      } catch (e) {
        console.log("error while getting faqData", e);
      }
    } catch (e) {}

    try {
      const teachersData = await getTeachersData(code, "");
      console.log("teachersData", teachersData.data);
      dispatch({
        type: "PROFESSORS_DATA",
        payload: teachersData.data,
      });
      dispatch(
        downloadImagesForStudentsAndProfessors(
          studentDataResult,
          teachersData.data,
          code
        )
      );
    } catch (e) {
      dispatch({
        type: "PROFESSORS_DATA",
        payload: [],
      });
    }

    try {
      let config = {
        method: "get",
        params: {
          universityCode: code,
        },
        url:
          "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/contacts/getcontacts",
        headers: {},
      };

      let contacts = await axios(config);
      console.log("Contacts", contacts.data);
      console.log("batchActions", batchActions);
      // dispatch(
      //   batchActions.admissionsData(admissionDataResult.data.admissionData)
      // );
      dispatch(asyncAdmissionsData(code));
      dispatch({
        type: "CONTACTS_DATA",
        payload: contacts.data,
      });
    } catch (e) {}

    dispatch({
      type: "STUDENTS_DATA",
      payload: studentDataResult.data.studentData,
    });

    dispatch({
      type: "SCHEDULE_DATA_UNIVERSITY",
      payload:
        scheduleDataUniversityResult.data.scheduleDataRespectiveUniversity,
    });
    // dispatch({
    //   type: "DEPARTMENTS_DATA",
    //   payload: departmentsDataResult.data.departmentData,
    // });
    // dispatch({
    //   type: "ADMISSION_DATA",
    //   payload: admissionDataResult.data.admissionData,
    // });
  };
};

export const downloadImagesForStudentsAndProfessors = (
  students,
  professors,
  universityCode
) => {
  return async (dispatch) => {
    console.log("students,professors", students.data.studentData);
    let professorsNew = JSON.parse(JSON.stringify(professors));
    let studentsNew = JSON.parse(JSON.stringify(students.data.studentData));

    for (let i = 0; i < studentsNew.length; i++) {
      studentsNew[i].UserImage = "";
      const UserCode = studentsNew[i].UserCode;
      let path = universityCode + "/" + UserCode + "/profileImage.jpg";
      // if (i == 0) {
      //   path = universityCode + "/" + "USR052" + "/profileImage.jpg";
      // }

      try {
        const downloadedImage = await Storage.get(path.toString(), {
          contentType: "image/jpeg",
          expires: 2400,
        });
        console.log("downloadedImage", downloadedImage);
        await checkImage(downloadedImage);
        studentsNew[i].UserImage = downloadedImage;
      } catch (e) {
        studentsNew[i].UserImage = "";
      }
    }

    dispatch({
      type: "STUDENTS_DATA",
      payload: studentsNew,
    });
    for (let i = 0; i < professorsNew.length; i++) {
      professorsNew[i].UserImage = "";
      const UserCode = professorsNew[i].UserCode;
      let path = universityCode + "/" + UserCode + "/profileImage.jpg";
      // if (i == 0) {
      //   path = universityCode + "/" + "USR052" + "/profileImage.jpg";
      // }

      try {
        const downloadedImage = await Storage.get(path.toString(), {
          contentType: "image/jpeg",
          expires: 2400,
        });
        console.log("downloadedImage", downloadedImage);
        await checkImage(downloadedImage);
        professorsNew[i].UserImage = downloadedImage;
      } catch (e) {
        professorsNew[i].UserImage = "";
      }
    }

    dispatch({
      type: "PROFESSORS_DATA",
      payload: professorsNew,
    });
  };
};

export const logOutAction = () => {
  return async function (dispatch) {
    try {
      const signoutData = await Auth.signOut();
      history.push("/");
      console.log("singout called", signoutData);
      dispatch({ type: "IS_AUTHENTICATED", payload: false });
      dispatch({ type: "LOGIN_SUCCESS", payload: null });
    } catch (e) {
      console.log("FAILED LOGOUT");
    }
  };
};

export const passwordsDoNotMatch = () => {
  return async function (dispatch) {
    dispatch({
      type: "REGISTER_FAILED",
      payload: "Passwords do not match",
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 10000);
  };
};

export const signUpAction = (formData) => {
  console.log("SignUp", formData);
  const {
    name,
    username,
    address,
    phone_number,
    college_name,
    password,
    designation,
    pincode,
    state,
    country,
  } = formData;

  return async function (dispatch) {
    try {
      const signUpCalled = await Auth.signUp({
        username,
        password,
        attributes: {
          "custom:Name": name,
          email: username,
          "custom:phoneNumber": phone_number,
          "custom:Address": address,
          "custom:college_name": college_name,
          "custom:designation": designation,
          "custom:pincode": pincode,
          "custom:state": state,
          "custom:country": country,
        },
      });
      console.log("SignUpCalled", signUpCalled);
      return new Promise((resolve) =>
        resolve("Perfect this user does not exist in cognito user pool")
      );
    } catch (e) {
      console.log("FAILED LOGOUT", e);
      const apiDetails = await getDataAnotherUser(formData.username);
      console.log("apiDetails", apiDetails);
      let dataObject = { apiDetails, formData };

      dispatch({
        type: "ANOTHER_USER",
        payload: dataObject,
      });

      if (e.message === "An account with the given email already exists.") {
        history.push("/registerAnotherUser");
      }
      dispatch({
        type: "LOGIN_FAILED",
        payload: e.message,
      });
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);
    }
  };
};

export const anotherUserSignUp = (formData) => {
  return async function (dispatch) {
    console.log("REGISTER FROM ANOTHER USER", formData);
    try {
      const apiDetails = await getDataAnotherUserSubmit(formData);
      console.log("Api details", apiDetails);
      history.push("/");
    } catch (e) {
      console.log("another user sign up failed", e);
    }
  };
};

export const ConfimCodeAction = (formData) => {
  console.log("ConfirmCode", formData.username, formData.code);
  const { username, code } = formData;
  return async function (dispatch) {
    try {
      console.log("ConfirmCode FINAL", username, code);
      const confirmCode = await Auth.confirmSignUp(username, code);
      console.log("Confim Code", confirmCode);
      //dispatch({ type: "LOGIN_SUCCESS", payload: null });
      //dispatch({ type: "IS_AUTHENTICATED", payload: true });
      history.push("/");
    } catch (e) {
      console.log("FAILED Confirm Codee", e, e.message);
      if (e.message === "Username/client id combination not found.") {
        e.message = "Username not found";
      }

      dispatch({
        type: "REGISTER_FAILED",
        payload: e.message,
      });
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);
    }
  };
};

// export const reloadAction = (code) => {
//   return async function (dispatch) {
//     try {
//       const session = await Auth.currentSession();
//       console.log("session", session);
//       const user = await Auth.currentAuthenticatedUser();
//       console.log("current user", user);
//       console.log("api call info data", user.attributes.email, code);
//       var apiDetails = await getData(user.attributes.email, code);
//       dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
//       dispatch({ type: "IS_AUTHENTICATED", payload: true });

//       // dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
//       return new Promise((res) => res("async data"));
//     } catch (e) {
//       console.log("RELOADING OF USER FAILED", e);
//       logOutAction();
//       dispatch({ type: "LOGIN_SUCCESS", payload: null });
//       dispatch({ type: "IS_AUTHENTICATED", payload: false });
//     }
//   };
// };

export const alertAction = (data) => {
  return async function (dispatch) {
    dispatch({ type: ERROR_OCCURED, payload: data });
  };
};

export const alertActionStudentCreation = (data) => {
  return async function (dispatch) {
    dispatch({ type: "LOGIN_FAILED", payload: data });
  };
};

export const removeAlert = () => {
  return async function (dispatch) {
    dispatch({ type: "REMOVE_ALERT", payload: "" });
  };
};

export const resendAuthCode = ({ email }) => {
  return async function (dispatch) {
    //dispatch({ type: "REMOVE_ALERT", payload: "" });
    let e;
    try {
      console.log("email2", email);
      const resend = await Auth.resendSignUp(email);
      console.log("resend successfully", resend);
      e = { message: "Verification code sent to your registered mail Id" };
    } catch (error) {
      console.log("error", error.message);
      e = { message: error.message };
    }
    if (e.message === "Username/client id combination not found.") {
      e.message = "UserName not found";
    }
    dispatch({
      type: "LOGIN_FAILED", // althougth this is basically same for auth code failure.
      payload: e.message,
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 20000);
  };
};

export const forgotPasswordAction = ({ email, code, new_password }) => {
  console.log("forgotPasswordObject", { email, code, new_password });
  return async function (dispatch) {
    let e;
    try {
      var password = await Auth.forgotPasswordSubmit(email, code, new_password);
      console.log("Password Reset Successful", password);
      e = { message: "Password Reset Successful" };
    } catch (error) {
      console.log("problem reseting password", error);
      e = { message: error.message };
    }

    if (e.message === "Username/client id combination not found.") {
      e.message = "Username not found";
    }
    dispatch({
      type: "LOGIN_FAILED", // althougth this is basically same for auth code failure.
      payload: e.message,
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 20000);
  };
};

export const forgotPasswordActionSecond = (forgotPasswordObject) => {
  console.log("forgotPasswordObject", forgotPasswordObject);
  let e;
  return async function (dispatch) {
    try {
      var code = await Auth.forgotPassword(forgotPasswordObject.email);
      console.log("code", code);
      e = { message: "Verification code sent to your registered mail id" };
    } catch (error) {
      console.log("password reset error", error.message);
      e = { message: error.message };
    }
    dispatch({
      type: "LOGIN_FAILED", // althougth this is basically same for auth code failure.
      payload: e.message,
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 20000);
  };
};

export const forgotUniversityCodes = (formData) => {
  console.log("forgotUniversityCodesObject", formData);
  return async function (dispatch) {
    try {
      await Auth.signIn(formData.email, formData.password);
      const apiDetails = await getDataAnotherUser(formData.email);
      console.log("apiDetails", apiDetails);
      let dataObject = { apiDetails, formData };

      dispatch({
        type: "ANOTHER_USER",
        payload: dataObject,
      });
    } catch (e) {
      console.log("LOGIN for forget university codes failed", e);
      dispatch({ type: "IS_AUTHENTICATED", payload: false });
      return new Promise((res, rej) => rej(e.message));
    }
    //history.push("/forgotUniversityCodes");
  };
};

export const registerUser = (formData) => {
  return async function (dispatch) {
    try {
      console.log("formData", formData);
      const user = await Auth.signIn(formData.email, formData.old_password);
      const registerdUser = await Auth.completeNewPassword(
        user,
        formData.password
      );
      console.log("user", registerdUser);
      dispatch({ type: "LOGIN_SUCCESS", payload: { name: "check" } });
      dispatch({ type: "IS_AUTHENTICATED", payload: true });
      history.push("/");
    } catch (e) {
      console.log("error in student registration", e);
    }
  };
};

export const downLoadImage = (path) => {
  console.log("downloadImage", path);
  return async function (dispatch) {
    try {
      console.log("downloadImage22", path);
      const downloadedImage = await Storage.get(path.toString(), {
        contentType: "image/jpeg",
        expires: 2400,
      });
      console.log("path downloaded", path, downloadedImage);
      dispatch({ type: "PROFILE_IMAGE", payload: downloadedImage });
    } catch (e) {
      console.log("error", e);
    }
  };
};

// export const reloadAction = (code) => {
//   return async function (dispatch) {
//     try {
//       const session = await Auth.currentSession();
//       console.log("session", session);
//       const user = await Auth.currentAuthenticatedUser();
//       console.log("current user", user);
//       console.log("api call info data", user.attributes.email, code);
//       var apiDetails = await getData(user.attributes.email, code);
//       let path =
//         apiDetails.data.Uni_Code +
//         "/" +
//         apiDetails.data.UserCode +
//         "/profileImage.jpg";
//       // console.log("this.props uploadprofilephoto", path);
//       // downLoadImage(path);
//       downloadImagefunction(path, dispatch);
//       dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
//       dispatch({ type: "IS_AUTHENTICATED", payload: true });

//       // dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
//       return new Promise((res) => res("async data"));
//     } catch (e) {
//       console.log("RELOADING OF USER FAILED", e);
//       return <Redirect to="/" />;
//       dispatch(logOutAction());
//       dispatch({ type: "LOGIN_SUCCESS", payload: null });
//       dispatch({ type: "IS_AUTHENTICATED", payload: false });
//     }
//   };
// };
/*
this is actions file, every single component which gets rendered calls this file,once certain event in that component occurs.
the corresponding action presnet over here occurs,whose job is too either switch pages or make call for another.
what job each action performs is mentioned in the name of the actions.

*/

const checkImage = async (url) => {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.send();
    request.onload = function () {
      if (request.status == 200) {
        //if(statusText == OK)
        resolve();
      } else {
        reject();
      }
    };
  });
};
