import history from "../history";
import { Auth, API } from "aws-amplify";

export const getData = async function (userEmail, userCode) {
  const apiName = "retrieveInformationAPI1";
  const path = "/check";

  const myInit = {
    // OPTIONAL
    headers: {
      "Content-Type": "/*",
      "x-api-key": "SWocWhZaei9p20QVVjzIV5Yjrkzh95pT83v8y6gO",
    }, // OPTIONAL
    response: true, // OPTIONAL (return the entire Axios response object instead of only response.data)
    queryStringParameters: {
      // OPTIONAL
      email: userEmail,
      code: userCode,
    },
  };

  return API.get(apiName, path, myInit);
};

async function getDataAnotherUser(userEmail) {
  const apiName = "retrieveInformationAPI1";
  const path = "/anotheruserrequest";

  const myInit = {
    // OPTIONAL
    headers: {
      "Content-Type": "/*",
    }, // OPTIONAL
    response: true, // OPTIONAL (return the entire Axios response object instead of only response.data)
    queryStringParameters: {
      // OPTIONAL
      email: userEmail,
    },
  };

  return API.get(apiName, path, myInit);
}

async function getDataAnotherUserSubmit(formData) {
  const apiName = "retrieveInformationAPI1";
  const path = "/anotherusersubmit";

  const myInit = {
    // OPTIONAL
    headers: {
      "Content-Type": "/*",
    }, // OPTIONAL
    response: true, // OPTIONAL (return the entire Axios response object instead of only response.data)
    queryStringParameters: {
      // OPTIONAL
      formData: JSON.stringify(formData),
    },
  };

  return API.get(apiName, path, myInit);
}

/*
 these are basically api actions.

 getData()  is the basicallyy the api which gets called to verify the university code and email,
 with the email id.

 getDataAnotherUser()  when we forgot univeristy codes or if we want to register a user more than once,
 this API is called.

 getDataAnotherUserSubmit()
 this api is called when a user which is already registered tried to re register itself for another university.

*/

//actions
export const loginAction = (formData) => {
  console.log("1", formData.username, formData.password, formData.code);
  return async function (dispatch) {
    let errorCode;
    try {
      const user = await Auth.signIn(formData.username, formData.password);
      //const userData = JSON.parse(JSON.stringify(user));
      console.log("challengeName", user.challengeName, user);
      if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
        console.log("vawefwerfwergfv");
        history.push({
          pathname: "/student",
          state: { email: formData.username, password: formData.password },
        });
      }
      console.log("LOGIN SUCCESSFULL", user);
      try {
        var apiDetails = await getData(formData.username, formData.code);
        console.log("apiDetails", apiDetails.data.UserRole);

        // if (apiDetails.data.UserRole === "Student") {
        //   history.push("/studentLogin");
        // }
      } catch (e) {
        console.log("error", e);
        errorCode = e["response"].status;
        if (errorCode === 401) {
          console.log("code do match");
          await Auth.signOut();
          let error = { message: "Code is incorrect" };
          throw error;
        }
      }
      console.log("authenticationFlowType", user.authenticationFlowType);
      console.log("apiDetails", apiDetails);
      console.log("errorCode", errorCode);
      //console.log("apiDetails code", formData.code, apiDetails.data);
      localStorage.setItem("universityCode", formData.code);
      // console.log("Count", apiDetails.data.Count);
      dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
      dispatch({ type: "IS_AUTHENTICATED", payload: true });
      if (user.challengeName !== "NEW_PASSWORD_REQUIRED") {
        console.log("welcome");
        history.push("/welcome");
      }
    } catch (e) {
      console.log("LOGIN FAILED", e);
      dispatch({ type: "IS_AUTHENTICATED", payload: false });
      dispatch({
        type: "LOGIN_FAILED",
        payload: e.message,
      });
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);
      console.log("loginfailedcheck");
      return new Promise((res, rej) => rej(e.message));
    }
  };
};

export const load = (data) => ({ type: "LOAD", data });
export const loadStudentData = (data) => ({ type: "LOAD_STUDENT", data });

export const logOutAction = () => {
  return async function (dispatch) {
    try {
      const signoutData = await Auth.signOut();
      history.push("/");
      console.log("singout called", signoutData);
      dispatch({ type: "IS_AUTHENTICATED", payload: false });
      dispatch({ type: "LOGIN_SUCCESS", payload: null });
    } catch (e) {
      console.log("FAILED LOGOUT");
    }
  };
};

export const passwordsDoNotMatch = () => {
  return async function (dispatch) {
    dispatch({
      type: "REGISTER_FAILED",
      payload: "Passwords do not match",
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 10000);
  };
};

export const SignUpAction = (formData) => {
  console.log("SignUp", formData);
  const {
    name,
    username,
    address,
    phone_number,
    college_name,
    password,
    alternate_email,
    pincode,
    state,
    country,
  } = formData;

  return async function (dispatch) {
    try {
      const signUpCalled = await Auth.signUp({
        username,
        password,
        attributes: {
          name,
          email: username,
          phone_number,
          address,
          "custom:college_name": college_name,
          "custom:alternate_email": alternate_email,
          "custom:pincode": pincode,
          "custom:state": state,
          "custom:country": country,
        },
      });
      console.log("SignUpCalled", signUpCalled);
      //history.push("/confirm");
    } catch (e) {
      console.log("FAILED LOGOUT", e);
      const apiDetails = await getDataAnotherUser(formData.username);
      console.log("apiDetails", apiDetails);
      let dataObject = { apiDetails, formData };

      dispatch({
        type: "ANOTHER_USER",
        payload: dataObject,
      });

      if (e.message === "An account with the given email already exists.") {
        history.push("/registerAnotherUser");
      }
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);
    }
  };
};

export const anotherUserSignUp = (formData) => {
  return async function (dispatch) {
    console.log("REGISTER FROM ANOTHER USER", formData);
    try {
      const apiDetails = await getDataAnotherUserSubmit(formData);
      console.log("Api details", apiDetails);
      history.push("/");
    } catch (e) {
      console.log("another user sign up failed", e);
    }
  };
};

export const ConfimCodeAction = (formData) => {
  console.log("ConfirmCode", formData.username, formData.code);
  const { username, code } = formData;
  return async function (dispatch) {
    try {
      console.log("ConfirmCode FINAL", username, code);
      const confirmCode = await Auth.confirmSignUp(username, code);
      console.log("Confim Code", confirmCode);
      //dispatch({ type: "LOGIN_SUCCESS", payload: null });
      dispatch({ type: "IS_AUTHENTICATED", payload: true });
      history.push("/welcome");
    } catch (e) {
      console.log("FAILED Confirm Codee", e, e.message);
      if (e.message === "Username/client id combination not found.") {
        e.message = "Username not found";
      }

      dispatch({
        type: "REGISTER_FAILED",
        payload: e.message,
      });
      setTimeout(function () {
        dispatch({ type: "REMOVE_ALERT", payload: "" });
      }, 5000);
    }
  };
};

export const reloadAction = (code) => {
  return async function (dispatch) {
    try {
      const session = await Auth.currentSession();
      console.log("session", session);
      const user = await Auth.currentAuthenticatedUser();
      console.log("current user", user);
      console.log("api call info data", user.attributes.email, code);
      var apiDetails = await getData(user.attributes.email, code);
      dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
      dispatch({ type: "IS_AUTHENTICATED", payload: true });

      // dispatch({ type: "LOGIN_SUCCESS", payload: apiDetails.data });
      return new Promise((res) => res("async data"));
    } catch (e) {
      console.log("RELOADING OF USER FAILED", e);
      logOutAction();
      dispatch({ type: "LOGIN_SUCCESS", payload: null });
      dispatch({ type: "IS_AUTHENTICATED", payload: false });
    }
  };
};

export const alertAction = (data) => {
  return async function (dispatch) {
    dispatch({ type: "LOGIN_FAILED", payload: "login message failed" });
  };
};

export const alertActionStudentCreation = (data) => {
  return async function (dispatch) {
    dispatch({ type: "LOGIN_FAILED", payload: data });
  };
};

export const removeAlert = () => {
  return async function (dispatch) {
    dispatch({ type: "REMOVE_ALERT", payload: "" });
  };
};

export const resendAuthCode = ({ email }) => {
  return async function (dispatch) {
    //dispatch({ type: "REMOVE_ALERT", payload: "" });
    let e;
    try {
      console.log("email2", email);
      const resend = await Auth.resendSignUp(email);
      console.log("resend successfully", resend);
      e = { message: "Verification code sent to your registered mail Id" };
    } catch (error) {
      console.log("error", error.message);
      e = { message: error.message };
    }
    if (e.message === "Username/client id combination not found.") {
      e.message = "UserName not found";
    }
    dispatch({
      type: "LOGIN_FAILED", // althougth this is basically same for auth code failure.
      payload: e.message,
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 20000);
  };
};

export const forgotPasswordAction = ({ email, code, new_password }) => {
  console.log("forgotPasswordObject", { email, code, new_password });
  return async function (dispatch) {
    let e;
    try {
      var password = await Auth.forgotPasswordSubmit(email, code, new_password);
      console.log("Password Reset Successful", password);
      e = { message: "Password Reset Successful" };
    } catch (error) {
      console.log("problem reseting password", error);
      e = { message: error.message };
    }

    if (e.message === "Username/client id combination not found.") {
      e.message = "Username not found";
    }
    dispatch({
      type: "LOGIN_FAILED", // althougth this is basically same for auth code failure.
      payload: e.message,
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 20000);
  };
};

export const forgotPasswordActionSecond = (forgotPasswordObject) => {
  console.log("forgotPasswordObject", forgotPasswordObject);
  let e;
  return async function (dispatch) {
    try {
      var code = await Auth.forgotPassword(forgotPasswordObject.email);
      console.log("code", code);
      e = { message: "Verification code sent to your registered mail id" };
    } catch (error) {
      console.log("password reset error", error.message);
      e = { message: error.message };
    }
    dispatch({
      type: "LOGIN_FAILED", // althougth this is basically same for auth code failure.
      payload: e.message,
    });
    setTimeout(function () {
      dispatch({ type: "REMOVE_ALERT", payload: "" });
    }, 20000);
  };
};

export const forgotUniversityCodes = (formData) => {
  console.log("forgotUniversityCodesObject", formData);
  return async function (dispatch) {
    try {
      await Auth.signIn(formData.email, formData.password);
      const apiDetails = await getDataAnotherUser(formData.email);
      console.log("apiDetails", apiDetails);
      let dataObject = { apiDetails, formData };

      dispatch({
        type: "ANOTHER_USER",
        payload: dataObject,
      });
    } catch (e) {
      console.log("LOGIN for forget university codes failed", e);
      dispatch({ type: "IS_AUTHENTICATED", payload: false });
      return new Promise((res, rej) => rej(e.message));
    }
    //history.push("/forgotUniversityCodes");
  };
};

export const registerUser = (formData) => {
  return async function (dispatch) {
    try {
      console.log("formData", formData);
      const user = await Auth.signIn(formData.email, formData.old_password);
      const registerdUser = await Auth.completeNewPassword(
        user,
        formData.password
      );
      console.log("user", registerdUser);
      dispatch({ type: "LOGIN_SUCCESS", payload: { name: "check" } });
      dispatch({ type: "IS_AUTHENTICATED", payload: true });
      history.push("/");
    } catch (e) {
      console.log("error in student registration", e);
    }
  };
};

/*
this is actions file, every single component which gets rendered calls this file,once certain event in that component occurs.
the corresponding action presnet over here occurs,whose job is too either switch pages or make call for another.
what job each action performs is mentioned in the name of the actions.

*/
