export const ID_TOKEN_COGNITO = "ID_TOKEN_COGNITO";
export const ACCESS_TOKEN_COGNITO = "ACCESS_TOKEN_COGNITO";
export const REFRESH_TOKEN_COGNITO = "REFRESH_TOKEN_COGNITO";
export const ERROR_OCCURED = "ERROR_OCCURED";
export const FETCH_TEACHERS_DATA_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/teachers/fetchteachers";
export const DELETE_PROFESSOR_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/teachers/deleteteacher";
export const UPDATE_PROFESSOR_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/teachers/updateteacher";
export const HELP_FAQ_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/help/getfaq";
export const HELP_PUT_FAQ =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/help/putfaq";
export const DELETE_FAQ =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/help/deletefaq";
export const UPDATE_FAQ =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/help/editfaq";
export const CREATE_PROFESSOR_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/teachers/createteacher";
export const DELETE_SCHEDULE_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/schedule/deleteresource";
export const DELETE_CONTACT_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/contacts/deletecontact";
export const CREATE_CONTACT_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/contacts/createcontact";
export const UPDATE_CONTACT_URL =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/contacts/updatecontact";
export const LOCATIONS =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/locations";
export const SUBJECTS =
  "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/subjects";
