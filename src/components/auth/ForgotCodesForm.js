import React from "react";
import { forgotUniversityCodes } from "../../actions/authActions";
//import ReactDOM from "react-dom";
import { connect } from "react-redux";
//import AlertComponent from "./AlertComponent";
import history from "../../history";
import { Field, reduxForm } from "redux-form";
import { SubmissionError } from "redux-form";
const validate = (formValues) => {
  let error = {};
  if (!formValues.email) {
    error.email = "Please provide an Email";
  }
  if (!formValues.password) {
    error.password = "Please provide a Password";
  }
  return error;
};

class ForgotCodesForm extends React.Component {
  state = { open: false, isSpinner: false };
  revertOpen = () => {
    this.setState({ open: !this.state.open });
  };
  renderError({ error, touched }) {
    //console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prependv"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  renderEmailInput = ({ input, placeholder, meta, type }) => {
    //   ddd console.log("formValues", formProps);

    return (
      <div className="form-group">
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  onSubmit = async (formValues) => {
    console.log("submit forgot university codes", formValues);
    this.setState({ isSpinner: true });
    try {
      await this.props.forgotUniversityCodes(formValues);
      console.log(this.props.val);
      this.props.check();
      this.setState({ isSpinner: false });
    } catch (e) {
      this.setState({ isSpinner: false });
      throw new SubmissionError({
        password: e,
      });
    }
  };
  render() {
    console.log(
      "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",
      this.props
    );
    return (
      <div onClick={() => history.push("/")}>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="header">
            Please input User Email to get University Codes.
          </div>
          <div className="content">
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
              <Field
                type="email"
                name="email"
                component={this.renderEmailInput}
                placeholder="Email"
              />
              <Field
                type="password"
                name="password"
                component={this.renderEmailInput}
                placeholder="Password"
              />
              <button className="ui button" type="submit">
                Submit
              </button>
            </form>
          </div>
          {this.state.isSpinner ? (
            <div className="ui active inverted dimmer">
              <div className="ui loader"></div>
            </div>
          ) : (
            <React.Fragment />
          )}
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: "forgetUniversityCodes",
  validate,
})(
  connect(null, {
    forgotUniversityCodes,
  })(ForgotCodesForm)
);

/*
This component gets called when we want to submit the form, 
when we forgot the univeristy codes to receive email.
*/

// export default connect(null, {
//   forgotUniversityCodes,
// })(formWrapped);
