import React from "react";
import { connect } from "react-redux";

class AlertComponent extends React.Component {
  render() {
    try {
      if (this.props.alertObject.isAlert) {
        return <p style={{ color: "red" }}>{this.props.alertObject.msg}</p>;
      } else {
        return <p></p>;
      }
    } catch (e) {
      return <p></p>;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    alertObject: state.alertUser,
  };
};
export default connect(mapStateToProps)(AlertComponent);

/*
when en error occurs in our system this component gets called.


*/
