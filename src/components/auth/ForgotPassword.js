import React from "react";
import {
  forgotPasswordAction,
  forgotPasswordActionSecond,
} from "../../actions";
import { connect } from "react-redux";
import AlertComponent from "./AlertComponent";
import { Field, reduxForm } from "redux-form";
function checkPassword(str) {
  var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  return re.test(str);
}
const validate = (formValues) => {
  let error = {};
  if (!formValues.email) {
    error.email = "Please provide an Email";
  }
  if (!formValues.code) {
    error.code = "Please provide a code";
  }
  if (formValues.code && formValues.code.length !== 6) {
    // console.log("formValues.pincode", formValues.pincode);
    error.code = "Please provide a 6 digit code";
  }
  if (!formValues.new_password) {
    error.new_password = "Please provide an New Password";
  }
  if (formValues.new_password && !checkPassword(formValues.new_password)) {
    error.new_password =
      "Password should have minimum of 7 character with at least one uppercase, one lowercase a number and one special symbol";
  }
  return error;
};

class ForgotPassword extends React.Component {
  state = { email: "", code: "", new_password: "" };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (formValues) => {
    console.log("submitfirst", formValues);
    let { email, new_password, code } = formValues;
    console.log("submit confirm called", email, new_password, code);
    this.props.forgotPasswordAction({
      email: email,
      code: code,
      new_password: new_password,
    });
  };

  onSubmitSecond = (e) => {
    console.log("submitsecond");
    e.preventDefault();
    console.log("submit confirm called", this.state.email);
    this.props.forgotPasswordActionSecond({
      email: this.state.email,
    });
  };

  renderError({ error, touched }) {
    //      console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          {error}
        </div>
      );
    }
  }

  renderInput = ({ input, placeholder, meta, type }) => {
    return (
      <div className="field">
        <div>
          <input type={type} placeholder={placeholder} {...input} />
          {this.renderError(meta)}
        </div>
      </div>
    );
  };

  render() {
    return (
      <React.Fragment>
        <form className="ui form" onSubmit={(e) => this.onSubmitSecond(e)}>
          <div className="field">
            <label>Email</label>
            <input
              required=""
              type="email"
              name="email"
              placeholder="Email"
              value={this.state.email}
              onChange={(e) => this.onChange(e)}
            />
          </div>
          <button className="ui button" type="submit">
            Submit
          </button>
        </form>

        <form
          className="ui form"
          onSubmit={this.props.handleSubmit(this.onSubmit)}
        >
          <Field
            type="email"
            name="email"
            component={this.renderInput}
            placeholder="Email"
          />
          <Field
            type="text"
            name="code"
            component={this.renderInput}
            placeholder="Confirmation Code"
          />
          <Field
            type="password"
            name="new_password"
            component={this.renderInput}
            placeholder="New Password"
          />
          <button className="ui button" type="submit">
            Submit
          </button>
        </form>
        <AlertComponent />
      </React.Fragment>
    );
  }
}

// export default connect(null, {
//   forgotPasswordAction,
//   forgotPasswordActionSecond,
// })(ForgotPassword);

const formWrapped = reduxForm({
  form: "forgetPassword",
  validate,
})(ForgotPassword);

export default connect(null, {
  forgotPasswordAction,
  forgotPasswordActionSecond,
})(formWrapped);
