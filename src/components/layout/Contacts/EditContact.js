import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { updateContact } from "../../../actions/scheduleActions";
import validator from "validator";
const validate = (formValues) => {
  let error = {};
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  if (formValues.phoneNumber1 && formValues.phoneNumber1.length !== 10) {
    error.phoneNumber1 = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phoneNumber2 && formValues.phoneNumber2.length !== 10) {
    error.phoneNumber2 = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phoneNumber1) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phoneNumber1)) {
      error.phoneNumber1 =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }
  if (formValues.phoneNumber2) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phoneNumber2)) {
      error.phoneNumber2 =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }
  return error;
};

const validationSchema = Yup.object({
  name: Yup.string().required("Please provide a Name").nullable(),
  designation: Yup.string().required("Please provide a Designation").nullable(),
});

const EditContact = function (props) {
  console.log("props", props.contactInfo);
  const [error, setError] = useState("");
  const initialValues = {
    name: props.contactInfo.Name,
    location: props.contactInfo.Location,
    email: props.contactInfo.EmailId,
    designation: props.contactInfo.Designation,
    phoneNumber1: props.contactInfo.PhoneNumber1
      ? props.contactInfo.PhoneNumber1.toString().slice(1, 11)
      : "",
    phoneNumber2: props.contactInfo.PhoneNumber2
      ? props.contactInfo.PhoneNumber2.toString().slice(1, 11)
      : "",
  };

  const onSubmit = async (formValues) => {
    let updateContactObject = {
      Name: formValues.name,
      EmailId: formValues.email,
      Location: formValues.location,
      Designation: formValues.designation,
      PhoneNumber1: formValues.phoneNumber1
        ? "+" + formValues.phoneNumber1
        : "",
      PhoneNumber2: formValues.phoneNumber2
        ? "+" + formValues.phoneNumber2
        : "",
      idUniContact: props.contactInfo.idUniContacts,
      UniCode: props.contactInfo.UniCode,
    };

    try {
      await props.updateContact(props.contactsData, updateContactObject);
      props.setContactProfileFlag(false);
      props.setSelectedContact(-1);
    } catch (e) {
      setError(e);
      setTimeout(() => {
        props.setContactProfileFlag(false);
        props.setSelectedContact(-1);
      }, 10000);
    }
  };

  return (
    <Formik
      className="ui segment"
      initialValues={initialValues}
      validate={validate}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      <Form className="ui form">
        <div className="field">
          <label>Name</label>
          <Field type="text" name="name" />
          <ErrorMessage name="name">
            {(errorMsg) => {
              return <p style={{ color: "red" }}>{errorMsg}</p>;
            }}
          </ErrorMessage>
        </div>
        <div className="field">
          <label>Email</label>
          <Field type="text" name="email" />
          <ErrorMessage name="email">
            {(errorMsg) => {
              return <p style={{ color: "red" }}>{errorMsg}</p>;
            }}
          </ErrorMessage>
        </div>
        <div className="field">
          <label>Location</label>
          <Field type="text" name="location" />
          <ErrorMessage name="location">
            {(errorMsg) => {
              return <p style={{ color: "red" }}>{errorMsg}</p>;
            }}
          </ErrorMessage>
        </div>
        <div className="field">
          <label>Designation</label>
          <Field type="text" name="designation" />
          <ErrorMessage name="designation">
            {(errorMsg) => {
              return <p style={{ color: "red" }}>{errorMsg}</p>;
            }}
          </ErrorMessage>
        </div>
        <div className="field">
          <label>Phone Number1</label>
          <Field type="text" name="phoneNumber1" />
          <ErrorMessage name="phoneNumber1">
            {(errorMsg) => {
              return <p style={{ color: "red" }}>{errorMsg}</p>;
            }}
          </ErrorMessage>
        </div>
        <div className="field">
          <label>Phone Number2</label>
          <Field type="text" name="phoneNumber2" />
          <ErrorMessage name="phoneNumber2">
            {(errorMsg) => {
              return <p style={{ color: "red" }}>{errorMsg}</p>;
            }}
          </ErrorMessage>
        </div>

        <button
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          type="submit"
        >
          Submit
        </button>
        {error ? <p style={{ color: "red" }}>{error}</p> : null}
      </Form>
    </Formik>
  );
};

const mapStateToProps = (state) => {
  return {
    UniCode: state.userData.UniCode,
    contactsData: state.contactsData,
  };
};

export default connect(mapStateToProps, { updateContact })(EditContact);
