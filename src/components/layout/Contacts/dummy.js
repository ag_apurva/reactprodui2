import React from "react";
import ReactDOM from "react-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const validationSchema = Yup.object({
  name: Yup.string().required("Required"),
  email: Yup.string()
    .email("Please provide a valid email")
    .required("Please provide an email"),
  comment: Yup.string().required("Please provide a comment"),
});

const FormikForm = function (props) {
  console.log("...props.initialValues", props);
  console.log("...props.initialValues", props.initialValues);
  const initialValues = {
    ...props.initialValues,
  };
  const onSubmit = (formValues) => {
    console.log("form Values", formValues);
  };

  return (
    <Formik
      className="ui segment"
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      <Form className="ui form">
        <div className="field">
          <label>Name</label>
          <Field type="text" name="name" />
          <ErrorMessage name="name" />
        </div>
        <div className="field">
          <label>Email</label>
          <Field type="text" name="email" />
          <ErrorMessage name="email" />
        </div>
        <div className="field">
          <label>comment</label>
          <Field type="text" name="comment" />
          <ErrorMessage name="comment" />
        </div>
        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
};

const App = () => {
  const initialValues = {
    name: "namerr",
    email: "v@v.com",
    comment: "comment2222",
  };
  return (
    <div>
      <FormikForm initialValues={initialValues} />
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
/*
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import { Field, reduxForm } from "redux-form";
import { Autocomplete } from "@material-ui/lab";
import TextField from "@material-ui/core/TextField";
import { SentimentVerySatisfiedOutlined } from "@material-ui/icons";
const validate = (formValues) => {
  // console.log("formValues", formValues);
  let error = {};
  if (!formValues.name) {
    error.name = "Please select a Name";
  }
  //   if (!formValues.location) {
  //     error.location = "Please select a Location";
  //   }
  //   if (!formValues.designation) {
  //     error.designation = "Please select a Designation";
  //   }
  //   if (!formValues.phoneNumber1) {
  //     error.phoneNumber1 = "Please provide a Phone Name";
  //   }
  //   if (formValues.phoneNumber1 && formValues.phoneNumber1.length !== 10) {
  //     error.phoneNumber1 = "* Please provide a 10 digit Phone Name";
  //   }

  //   if (!formValues.phoneNumber2) {
  //     error.phoneNumber2 = "Please provide a Phone Name";
  //   }
  //   if (formValues.phoneNumber2 && formValues.phoneNumber2.length !== 10) {
  //     error.phoneNumber2 = "* Please provide a 10 digit Phone Name";
  //   }

  return error;
};

const EditContactBasic = function (props) {
  const [value, setValue] = useState({ title: "title1", id: 2 });
  const renderError = function ({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  };

  const renderEmailInput = function ({
    input,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) {
    //console.log("argument", meta);
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {renderError(meta)}
      </div>
    );
  };
  const renderPhoneInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {renderError(meta)}
      </div>
    );
  };

  const renderAutoComplete = (argument) => {
    console.log("argument.meta", argument.meta);
    console.log("argument.defaultValue", argument.defaultValue);
    argument.meta.touched = true;
    argument.meta.dirty = true;
    argument.meta.active = false;
    argument.meta.visited = true;
    return (
      <Autocomplete
        {...argument.input}
        {...argument}
        onChange={(e, newValue) => {
          console.log("e,value", e, newValue);
          if (newValue) {
            setValue(newValue);
            argument.input.onChange(newValue.id);
          }
        }}
        name={argument.name}
        id="combo-box-demo"
        options={data}
        getOptionLabel={(option) => {
          const dt = data.filter((item) => item.title === option.title);
          //console.log("option111", dt[0].id.toString());
          return dt[0].id.toString();
        }}
        getOptionSelected={(option, value) => {
          console.log("option333", option, value, option.title == value.title);
          return option.title == value.title;
        }}
        renderOption={(option) => {
          console.log("option222", option);
          return option.title;
        }}
        style={{ width: 300 }}
        value={value}
        //value={handleChange}
        renderInput={(params) => (
          <TextField {...params} label="Combo box" variant="outlined" />
        )}
      />
    );
  };

  const data = [
    { title: "title1", id: 1 },
    { title: "title2", id: 2 },
    { title: "title3", id: 3 },
    { title: "title4", id: 4 },
    { title: "title5", id: 5 },
    { title: "title6", id: 6 },
  ];

  useEffect(() => {
    console.log("useEffect");
    // props.initialize({
    //   location: props.contactInfo.Location,
    //   designation: props.contactInfo.Designation || "",
    //   phoneNumber1: props.contactInfo.PhoneNumber1,
    //   phoneNumber2: props.contactInfo.PhoneNumber2,
    //   name: props.contactInfo.Name,
    // });
  }, []);
  const onSubmit = (formValues) => {
    console.log("formValues", formValues);
    console.log("value", value);
  };
  return (
    <div>
      <form
        initialValues={{
          location: props.contactInfo.Location,
          designation: props.contactInfo.Designation || "",
          phoneNumber1: props.contactInfo.PhoneNumber1,
          phoneNumber2: props.contactInfo.PhoneNumber2,
          name: props.contactInfo.Name,
        }}
        onSubmit={props.handleSubmit(onSubmit)}
      >
        <Field
          type="text"
          disabled={false}
          label="Name"
          name="nameCheck"
          value={value}
          component={renderAutoComplete}
        />
        <Field
          disabled={false}
          label="Name"
          name="name"
          component={renderEmailInput}
        />
        <Field
          disabled={false}
          label="Phone Number 1"
          name="phoneNumber1"
          component={renderPhoneInput}
        />
        <Field
          disabled={false}
          label="Phone Number 2"
          name="phoneNumber2"
          component={renderPhoneInput}
        />
        <Field
          disabled={false}
          label="Location"
          name="location"
          type="text"
          component={renderEmailInput}
        />
        <Field
          disabled={false}
          label="Designation"
          name="designation"
          type="text"
          component={renderEmailInput}
        />
        <div className="form-control"></div>
        <div className="form-group">
          <div className="form-control"></div>
        </div>
        <div className="form-group"></div>
        <button type="submit">Submit</button>
        {/*{this.state.isSpinner ? (
                  <div className="ui active inverted dimmer">
                    <div className="ui loader"></div>
                  </div>
                ) : (
                  <React.Fragment />
                )}
                 {this.state.conflictContact ? (
                      <div>
                        <p style={{ color: "red" }}>{this.state.error}</p>
                      </div>
                    ) : (
                      <div></div>
                    )} 
      </form>
    </div>
  );
};

const EditContactForm = reduxForm({
  form: "Edit Contact",
  validate,
  enableReinitialize: true,
})(EditContactBasic);

const mapStateToProps = (state) => {
  return {
    UniCode: state.userData.UniCode,
  };
};

export default connect(mapStateToProps, {})(EditContactForm);

ChangeId: 1
ContactCode: "USR059"
Created Date: null
Designation: null
EmailId: "check@gmail.com"
Location: "location"
Name: "name1"
PhoneNumber1: "3344556677"
PhoneNumber2: "1122334455"
UniCode: "TR021"
*/

/*
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import { Field, reduxForm } from "redux-form";
import { createContact } from "../../../actions/scheduleActions";
const validate = (formValues) => {
  let error = {};
  if (!formValues.name) {
    error.name = "Please select a Name";
  }
  if (!formValues.location) {
    error.location = "Please select a Location";
  }
  if (!formValues.designation) {
    error.designation = "Please select a Designation";
  }
  if (!formValues.phoneNumber1) {
    error.phoneNumber1 = "Please provide a Phone Name";
  }
  if (formValues.phoneNumber1 && formValues.phoneNumber1.length !== 10) {
    error.phoneNumber1 = "* Please provide a 10 digit Phone Name";
  }

  if (!formValues.phoneNumber2) {
    error.phoneNumber2 = "Please provide a Phone Name";
  }
  if (formValues.phoneNumber2 && formValues.phoneNumber2.length !== 10) {
    error.phoneNumber2 = "* Please provide a 10 digit Phone Name";
  }

  return error;
};

const CreateContactBasic = function (props) {
  const [error, setError] = useState("");
  const renderError = function ({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  };
  const renderEmailInput = function ({
    input,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {renderError(meta)}
      </div>
    );
  };
  const renderPhoneInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {renderError(meta)}
      </div>
    );
  };

  const onSubmit = async (formValues) => {
    console.log("formValues", formValues);
    let updateContactObject = {
      Name: formValues.name,
      Location: formValues.location,
      Designation: formValues.designation,
      PhoneNumber1: formValues.phoneNumber1,
      PhoneNumber2: formValues.phoneNumber2,
      UniCode: props.UniCode,
    };
    console.log("updateContactObject", updateContactObject);
    try {
      await props.createContact(props.contactsData, updateContactObject);
      props.setCreateContactPopUpFlag(false);
      props.setSelectedContact(-1);
    } catch (e) {
      console.log("errro creare", e);
      setError(e);
      setTimeout(() => {
        props.setCreateContactPopUpFlag(false);
        props.setSelectedContact(-1);
      }, 10000);
    }
  };
  return (
    <Modal
      open={props.createContactPopUpFlag}
      onClose={() => props.setCreateContactPopUpFlag(false)}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      aria-hidden="true"
    >
      <Draggable>
        <div
          tabIndex="-1"
          style={{
            paddingLeft: 200,
            paddingTop: 200,
            width: 250,
          }}
        >
          <div className="ui standard modal visible active">
            <div className="header">Contact Creation</div>
            <div className="content">
              <form onSubmit={props.handleSubmit(onSubmit)}>
                <Field
                  disabled={false}
                  label="Name"
                  name="name"
                  component={renderEmailInput}
                />

                <Field
                  disabled={false}
                  label="Phone Number 1"
                  name="phoneNumber1"
                  component={renderPhoneInput}
                />
                <Field
                  disabled={false}
                  label="Phone Number 2"
                  name="phoneNumber2"
                  component={renderPhoneInput}
                />
                <Field
                  disabled={false}
                  label="Location"
                  name="location"
                  component={renderEmailInput}
                />
                <Field
                  disabled={false}
                  label="Designation"
                  name="designation"
                  component={renderEmailInput}
                />
                <div className="form-control"></div>
                <div className="form-group">
                  <div className="form-control"></div>
                </div>
                <div className="form-group"></div>
                <button type="submit">Submit</button>
                
                {error ? (
                  <div>
                    <p style={{ color: "red" }}>{error}</p>
                  </div>
                ) : (
                  <div></div>
                )}
              </form>
            </div>
          </div>
        </div>
      </Draggable>
    </Modal>
  );
};

const CreateContactForm = reduxForm({
  form: "Create Contact",
  validate,
})(CreateContactBasic);

const mapStateToProps = (state) => {
  return {
    contactsData: state.contactsData,
    UniCode: state.userData.UniCode,
  };
};

export default connect(mapStateToProps, { createContact })(CreateContactForm);



*/
