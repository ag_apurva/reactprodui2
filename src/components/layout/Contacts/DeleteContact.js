import React, { useState, useEffect } from "react";
import { deleteContact } from "../../../actions/scheduleActions";
import { connect } from "react-redux";
/*
              contactProfileFlag={contactProfileFlag}
              setContactProfileFlag={setContactProfileFlag}
              setSelectedContact={setSelectedContact}
              contactInfo={item}
*/

const DeleteContact = function (props) {
  const [error, setError] = useState("");
  const onSubmit = async () => {
    console.log("Submit Delete Contact");
    try {
      await props.deleteContact(props.contactsData, props.contactInfo);
      props.setContactProfileFlag(false);
      props.setSelectedContact(-1);
    } catch (e) {
      setError(e);
      setTimeout(() => {
        props.setContactProfileFlag(false);
        props.setSelectedContact(-1);
      }, 10000);
    }
  };
  return (
    <div>
      <div className="content">
        <p>Are you sure you want to delete Contact {props.UserName} ? </p>
        <button
          onClick={() => onSubmit()}
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
        >
          Delete Contact
        </button>
        {error ? (
          <div>
            <p style={{ color: "red" }}>{error}</p>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    contactsData: state.contactsData,
  };
};

export default connect(mapStateToProps, { deleteContact })(DeleteContact);
