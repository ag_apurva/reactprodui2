import React, { useState, useEffect } from "react";
import MyProfile from "../popups/MyProfile2";
import { connect } from "react-redux";
import CreateContact from "./CreateContact";
import ContactsProfile from "./ContactsProfile";
import { Modal } from "semantic-ui-react";

const Contacts = function (props) {
  const [myProfile, setMyProfile] = useState(false);
  const [searchName, setSearchName] = useState("");
  const [contactsList, setContactsList] = useState([]);
  const [createContactPopUpFlag, setCreateContactPopUpFlag] = useState(false);
  const [contactProfileFlag, setContactProfileFlag] = useState(false);
  const [selectedContact, setSelectedContact] = useState(-1);
  useEffect(() => {
    setContactsList(props.contactsData);
  }, [props.contactsData]);

  const renderContactsList = () => {
    return contactsList.map((item, index) => {
      const Name = item.Name || " ";
      const Designation = item.Designation || " ";
      const Category = item.Category || " ";
      const Location = item.Location || " ";
      const PhoneNumber1 = item.PhoneNumber1 || " ";
      const PhoneNumber2 = item.PhoneNumber2 || " ";
      if (Name.toLowerCase().includes(searchName.toLowerCase())) {
        return (
          <tr key={index}>
            {contactProfileFlag && selectedContact === index ? (
              <ContactsProfile
                contactProfileFlag={contactProfileFlag}
                setContactProfileFlag={setContactProfileFlag}
                setSelectedContact={setSelectedContact}
                contactInfo={item}
              />
            ) : (
              <React.Fragment />
            )}
            <td>{Name}</td>
            <td>{Designation}</td>
            <td>{Location}</td>
            <td>{Category}</td>
            <td>{PhoneNumber1}</td>
            <td>{PhoneNumber2}</td>
            <td>
              <button
                onClick={() => {
                  setContactProfileFlag(true);
                  setSelectedContact(index);
                }}
              >
                <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
              </button>
            </td>
          </tr>
        );
      } else {
        return null;
      }
    });
  };

  return (
    <div className="main-content bg-light">
      <Modal
        open={myProfile}
        onClose={() => setMyProfile(false)}
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
      >
        <MyProfile
          myProfile={props.userData}
          removeMyProfile={() => setMyProfile(false)}
        />
      </Modal>
      <header>
        <h4>
          <label htmlFor="nav-toggel">
            <span>
              <i className="fa fa-bars" aria-hidden="true"></i>
            </span>
          </label>
          <span className="name">Contacts</span>
        </h4>

        <div className="search-wrapper">
          <span>
            <i className="fa fa-search" aria-hidden="true"></i>
          </span>
          <input
            type="search"
            placeholder="Search here"
            onChange={(e) => setSearchName(e.target.value)}
          />
        </div>

        <div className="user-wrapper" onClick={() => setMyProfile(true)}>
          <img src={props.profileImageUrl} width="50px" height="50px" alt="" />
          <div>
            <h6>{props.userData.name}</h6>
            <small>Admin</small>
          </div>
        </div>
      </header>

      <main>
        <div className="filter-wrapper"></div>

        <div className="table-content m-3">
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">​Designation</th>
                <th scope="col">​Location</th>
                <th scope="col">Category</th>
                <th scope="col">​PhoneNumber1</th>
                <th scope="col">​PhoneNumber2</th>
                <th scope="col">Edit/Delete</th>
              </tr>
            </thead>
            <tbody>{renderContactsList()}</tbody>
          </table>
        </div>
        <button
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          onClick={() => setCreateContactPopUpFlag(true)}
        >
          Add Contact
        </button>
        {createContactPopUpFlag ? (
          <div>
            <CreateContact
              createContactPopUpFlag={createContactPopUpFlag}
              setCreateContactPopUpFlag={setCreateContactPopUpFlag}
              setContactProfileFlag={setContactProfileFlag}
              setSelectedContact={setSelectedContact}
            />
          </div>
        ) : (
          <div></div>
        )}
      </main>
    </div>
  );
};

const mapStateToProps = (state) => {
  //console.log("Contacts", state);
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
    contactsData: state.contactsData,
  };
};
export default connect(mapStateToProps, null)(Contacts);

/*

ChangeId: 1
​ContactCode: "USR059"
​"Created Date": null
​Designation: null
​EmailId: "check@gmail.com"
​Location: "location"
​Name: "name"
​PhoneNumber1: "3344556677"
​PhoneNumber2: "1122334455"
​UniCode: "TR021"
​"Updated Date": null
​idUniContacts: 1
 
 
*/
