import React, { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import DeleteContact from "./DeleteContact";
import EditContact from "./EditContact";

const ContactsProfile = function (props) {
  const [deleteProfile, setDeleteProfile] = useState("selectedTab");
  const [editProfile, setEditProfile] = useState();

  return (
    <Modal
      open={props.contactProfileFlag}
      onClose={() => {
        props.setContactProfileFlag(false);
        props.setSelectedContact(-1);
      }}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      aria-hidden="true"
    >
      <Draggable>
        <div
          tabIndex="-1"
          style={{
            paddingLeft: 200,
            paddingTop: 200,
            width: 250,
          }}
        >
          <div className="ui standard modal visible active">
            <div className="header">Contact Profile</div>
            <div className="content">
              <div className="ui secondary pointing menu">
                <button
                  className={
                    deleteProfile === "selectedTab" ? "item active" : "item"
                  }
                  onClick={() => {
                    setDeleteProfile("selectedTab");
                    setEditProfile("");
                  }}
                >
                  Delete Contact
                </button>
                <button
                  className={
                    editProfile === "selectedTab" ? "item active" : "item"
                  }
                  onClick={() => {
                    setEditProfile("selectedTab");
                    setDeleteProfile("");
                  }}
                >
                  Edit Contact Profile
                </button>
              </div>
              <div className="ui segment">
                {deleteProfile === "selectedTab" ? (
                  <div>
                    <DeleteContact
                      setContactProfileFlag={props.setContactProfileFlag}
                      setSelectedContact={props.setSelectedContact}
                      contactInfo={props.contactInfo}
                    />
                  </div>
                ) : (
                  <React.Fragment />
                )}
                {editProfile === "selectedTab" ? (
                  <div>
                    <EditContact
                      setContactProfileFlag={props.setContactProfileFlag}
                      setSelectedContact={props.setSelectedContact}
                      contactInfo={props.contactInfo}
                    />
                  </div>
                ) : (
                  <React.Fragment />
                )}
              </div>
            </div>
          </div>
        </div>
      </Draggable>
    </Modal>
  );
};

export default ContactsProfile;
