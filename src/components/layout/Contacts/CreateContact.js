import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import { createContact } from "../../../actions/scheduleActions";
import validator from "validator";
import * as Yup from "yup";
import { Grid, Button, FormField, Header } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";

const validate = (formValues) => {
  let error = {};
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  if (formValues.phoneNumber1 && formValues.phoneNumber1.length !== 10) {
    error.phoneNumber1 = "* Please provide a 10 digit Phone Name";
  }

  if (!formValues.category) {
    error.category = "* Please provide a Category";
  }

  if (formValues.phoneNumber2 && formValues.phoneNumber2.length !== 10) {
    error.phoneNumber2 = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phoneNumber1) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phoneNumber1)) {
      error.phoneNumber1 =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }

  if (formValues.phoneNumber2) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phoneNumber2)) {
      error.phoneNumber2 =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }
  return error;
};

const validationSchema = Yup.object({
  name: Yup.string().required("Please provide a Name").nullable(),
  //location: Yup.string().required("Please provide a Location").nullable(),
  designation: Yup.string().required("Please provide a Designation").nullable(),
});

const CreateContact = function (props) {
  const [error, setError] = useState("");

  const initialValues = {
    name: "",
    email: "",
    location: "",
    category: "",
    designation: "",
    phoneNumber1: "",
    phoneNumber2: "",
  };
  const onSubmit = async (formValues) => {
    let createContactObject = {
      Name: formValues.name,
      Location: formValues.location,
      EmailId: formValues.email,
      Category: formValues.category,
      Designation: formValues.designation,
      PhoneNumber1: formValues.phoneNumber1
        ? "+" + formValues.phoneNumber1
        : "",
      PhoneNumber2: formValues.phoneNumber2
        ? "+" + formValues.phoneNumber2
        : "",
      UniCode: props.UniCode,
    };
    console.log("updateContactObject", createContactObject);
    try {
      await props.createContact(props.contactsData, createContactObject);
      props.setCreateContactPopUpFlag(false);
      props.setSelectedContact(-1);
    } catch (e) {
      console.log("errro creare", e);
      setError(e);
      setTimeout(() => {
        props.setCreateContactPopUpFlag(false);
        props.setSelectedContact(-1);
      }, 10000);
    }
  };
  return (
    <Modal
      open={props.createContactPopUpFlag}
      onClose={() => props.setCreateContactPopUpFlag(false)}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      aria-hidden="true"
    >
      <Draggable>
        <div
          tabIndex="-1"
          style={{
            paddingLeft: 200,
            paddingTop: 200,
            width: 250,
          }}
        >
          <div className="ui standard modal visible active">
            <div className="header">Contact Creation</div>
            <div className="content">
              <Formik
                initialValues={initialValues}
                validate={validate}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
              >
                <Form className="ui form">
                  <div className="field">
                    <label>Name</label>
                    <Field type="text" name="name" />
                    <ErrorMessage name="name">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  <div className="field">
                    <label>Email</label>
                    <Field type="text" name="email" />
                    <ErrorMessage name="email">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  <div className="field">
                    <label>Location</label>
                    <Field type="text" name="location" />
                    <ErrorMessage name="location">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  <div className="field">
                    <label>Designation</label>
                    <Field type="text" name="designation" />
                    <ErrorMessage name="designation">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  <div className="field">
                    <label>Category</label>
                    <Field type="text" name="category">
                      {(props) => {
                        return (
                          <select {...props.field}>
                            <option value="">None</option>
                            <option value="Student">Student</option>
                            <option value="Professor">Professor</option>
                            <option value="Alumni">Alumni</option>
                            <option value="Management">Management</option>
                          </select>
                        );
                      }}
                    </Field>
                    <ErrorMessage name="category">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  <div className="field">
                    <label>Phone Number1</label>
                    <Field type="text" name="phoneNumber1" />
                    <ErrorMessage name="phoneNumber1">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  <div className="field">
                    <label>Phone Number2</label>
                    <Field type="text" name="phoneNumber2" />
                    <ErrorMessage name="phoneNumber2">
                      {(errorMsg) => {
                        return <p style={{ color: "red" }}>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>

                  <button
                    className="btn btn-primary mb-2"
                    style={{ backgroundColor: "#4cadad" }}
                    type="submit"
                  >
                    Submit
                  </button>
                  {error ? <p style={{ color: "red" }}>{error}</p> : null}
                </Form>
              </Formik>
            </div>
          </div>
        </div>
      </Draggable>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    contactsData: state.contactsData,
    UniCode: state.userData.UniCode,
  };
};

export default connect(mapStateToProps, { createContact })(CreateContact);
