import { Modal } from "semantic-ui-react";

const ImagePopUp = function (props) {
  return (
    <Modal open={props.imagePopUp} onClose={() => props.setImagePopUp(false)}>
      <Modal.Header>Image</Modal.Header>
      <Modal.Content style={{ width: "300" }}>
        <img src={props.UserImage} width="500px" height="500px" alt="" />
      </Modal.Content>
    </Modal>
  );
};

export default ImagePopUp;
