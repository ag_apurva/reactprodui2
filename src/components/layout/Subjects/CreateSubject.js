import * as React from "react";
import { useState, useEffect } from "react";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { connect } from "react-redux";
import { Formik, Field, ErrorMessage } from "formik";
import { createSubject } from "../../../actions/scheduleActions";

export const CreateSubject = (props) => {
  // const { createSubjectPopUpFlag, setCreateSubjectPopUpFlag } = props;
  const [subjects, setSubject] = useState([]);

  useEffect(() => {
    console.log("props.subjects", props.subjects);
    setSubject(props.subjects);
  }, [props.subjects]);

  const validate = (formValues) => {
    const errors = {};
    if (!formValues.subject) {
      errors.subject = "* Please provide a Subject";
    }

    if (formValues.subject) {
      const subjectCheck = subjects.find((subject) => {
        return (
          subject.toString().toLowerCase() === formValues.subject.toLowerCase()
        );
      });

      if (subjectCheck) {
        errors.subject = "* Subject Already Exists";
      }
    }
    return errors;
  };

  const onSubmit = async function (formValues) {
    try {
      let data = await props.createSubject(
        {
          UniCode: props.UniCode,
          Subject: formValues.subject,
        },
        subjects
      );

      props.setCreateSubjectPopUpFlag(false);
    } catch (e) {
      console.log("error creating subject", e);
    }
  };
  return (
    <Modal
      dimmer={"inverted"}
      open={props.createSubjectPopUpFlag}
      onClose={() => props.setCreateSubjectPopUpFlag(false)}
    >
      <Modal.Header>Create Subject</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{ subject: "" }}
          onSubmit={onSubmit}
          inverted={"dimmer"}
          validate={validate}
        >
          {(formik) => {
            return (
              <Form>
                <FormField>
                  <label htmlFor="subjectId">Subject</label>
                  <Field id="subjectId" name="subject" />
                  <ErrorMessage name="subject" />
                </FormField>
                <Button type="submit">Submit</Button>
              </Form>
            );
          }}
        </Formik>
      </Modal.Content>
      <Modal.Actions></Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  console.log("state", state.subjects);
  return {
    subjects: state.subjects,
    UniCode: state.userData.UniCode,
  };
};
export default connect(mapStateToProps, { createSubject })(CreateSubject);
