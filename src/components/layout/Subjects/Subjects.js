import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import CreateSubject from "./CreateSubject";
import { Modal } from "semantic-ui-react";
import MyProfile from "../popups/MyProfile2";
const Subjects = function (props) {
  const [subjects, setSubjects] = useState([]);
  const [myProfile, setMyProfile] = useState(false);
  const [searchName, setSearchName] = useState("");
  const [createSubjectPopUpFlag, setCreateSubjectPopUpFlag] = useState(false);
  const [subjectProfileFlag, setSubjectProfileFlag] = useState(false);
  const [selectedSubject, setSelectedSubject] = useState(-1);

  useEffect(() => {
    setSubjects(props.subjects);
  }, [props.subjects]);

  const renderSubjects = () => {
    return subjects.map((item, index) => {
      const SubjName = item.SubjName || " ";

      return (
        <tr key={index}>
          <td>{SubjName}</td>
          {/* <td>
            <button
              onClick={() => {
                setSubjectProfileFlag(true);
                setSelectedSubject(index);
              }}
            >
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>
          </td> */}
        </tr>
      );
    });
  };
  const removeMyProfile = function () {
    setMyProfile(false);
  };
  return (
    <div className="main-content bg-light">
      <Modal
        open={myProfile}
        onClose={() => setMyProfile(false)}
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
      >
        <MyProfile
          myProfile={props.userData}
          removeMyProfile={() => removeMyProfile()}
        />
      </Modal>
      <header>
        <h4>
          <label htmlFor="nav-toggel">
            <span>
              <i className="fa fa-bars" aria-hidden="true"></i>
            </span>
          </label>
          <span className="name">Subjects</span>
        </h4>

        <div className="search-wrapper">
          <span>
            <i className="fa fa-search" aria-hidden="true"></i>
          </span>
          <input
            type="search"
            placeholder="Search here"
            onChange={(e) => setSearchName(e.target.value)}
          />
        </div>

        <div className="user-wrapper" onClick={() => setMyProfile(true)}>
          <img src={props.profileImageUrl} width="50px" height="50px" alt="" />
          <div>
            <h6>{props.userData.name}</h6>
            <small>Admin</small>
          </div>
        </div>
      </header>

      <main>
        <div className="filter-wrapper"></div>

        <div className="table-content m-3">
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Subjects</th>
                {/* <th scope="col">Edit/Delete</th> */}
              </tr>
            </thead>
            <tbody>{renderSubjects()}</tbody>
          </table>
        </div>
        <button
          color="teal"
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          onClick={() => setCreateSubjectPopUpFlag(true)}
        >
          Add Subject
        </button>
        {createSubjectPopUpFlag ? (
          <CreateSubject
            createSubjectPopUpFlag={createSubjectPopUpFlag}
            setCreateSubjectPopUpFlag={setCreateSubjectPopUpFlag}
          />
        ) : (
          <div></div>
        )}
      </main>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    subjects: state.subjects,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, null)(Subjects);

/*
ClassRoomName
ClassRoomDetail
ClassRoomType
*/
