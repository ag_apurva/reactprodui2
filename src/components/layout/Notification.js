import React from "react";
import { connect } from "react-redux";
import { Modal } from "semantic-ui-react";
import MyProfile from "./popups/MyProfile2";

class Notification extends React.Component {
  state = {
    myProfile: false,
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };

  render() {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          style={{ overflow: "auto", maxHeight: 600 }}
          dimmer={"inverted"}
        >
          <MyProfile myProfile={data} removeMyProfile={this.removeMyProfile} />
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">Notification</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img
              src={this.props.profileImageUrl}
              width="50px"
              height="50px"
              alt=""
            />
            <div>
              <h6>{name}</h6>
              <small>Admin</small>
            </div>
          </div>
        </header>

        <main>
          <div className="filter-wrapper"></div>

          <div className="table-content m-3">
            <p>Notification</p>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, null)(Notification);
