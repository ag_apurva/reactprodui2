import React, { useEffect, useState } from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField, List } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { Parser } from "json2csv";
import { Form } from "formik-semantic-ui-react";
import MyProfile from "./popups/MyProfile2";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { createDepartment } from "../../actions/appSyncActions";
import { getDepartments } from "../../actions/scheduleActions/actions";
import csvtemplate from "./csv/DepartmentsTemplate.csv";
import FileDownload from "js-file-download";
import CSVReader from "react-csv-reader";
import { CSVDownloader } from "react-papaparse";
import CsvDownloader from "react-csv-downloader";
const AddDepartments = function (props) {
  const [error, setError] = useState("");
  const departmentsData = useSelector((state) => state.departments);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const departments = departmentsData.map((item) => item.departmentName);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    let error = {};
    if (!formValues.departmentName) {
      error.departmentName = "* Please provide an Department Name";
    }
    if (formValues.departmentName) {
      if (departments.includes(formValues.departmentName)) {
        error.departmentName = "* Department Exists";
      }
    }
    if (!formValues.departmentHead) {
      error.departmentHead = "* Please provide an Department Head";
    }
    return error;
  };
  const onSubmit = async (formValues) => {
    try {
      await dispatch(
        createDepartment(
          {
            departmentName: formValues.departmentName,
            departmentHead: formValues.departmentHead,
            universityCode: UniCode,
          },
          departmentsData
        )
      );
      props.removeDepartmentPopUpBox();
    } catch (e) {
      setError(e);
      setTimeout(() => {
        props.removeDepartmentPopUpBox();
      }, 20000);
    }
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      <Modal.Header>Add Department</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{ departmentName: "", departmentHead: "" }}
          validate={validate}
          onSubmit={onSubmit}
        >
          <Form>
            <FormField>
              <label htmlFor="deptId">Department Name</label>
              <Field id="deptId" name="departmentName" />
              <ErrorMessage name="departmentName" />
            </FormField>
            <FormField>
              <label htmlFor="depthead">Department Head</label>
              <Field id="depthead" name="departmentHead" />
              <ErrorMessage name="departmentHead" />
            </FormField>
            <Button type="submit">Submit</Button>
          </Form>
        </Formik>
      </Modal.Content>
      <Modal.Actions>
        {error ? <p>{error}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};

const UploadDepartmentData = function (props) {
  const [downloadTemplate, setDownloadTemplate] = useState(true);
  const [uploadData, setUploadData] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [csvData, setCSVData] = useState("");
  const [errors, setErrors] = useState([]);
  const [errorsCSV, setCSVErrors] = useState("");
  const [errorProcessed, setErrorsProcessed] = useState(false);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const dispatch = useDispatch();
  console.log("csvtemplate", csvtemplate);
  const dataJSON = {};
  const errorUpload = function () {
    console.log("ErrorUploadonError={this.handleDarkSideForce}");
  };
  const handleChange = async (data) => {
    console.log("data", data);
    const file = JSON.parse(JSON.stringify(data).replace(/\n|\r/g, ""));
    // console.log("file", file);
    //console.log("data1", data);
    const dataMap = [];
    data.forEach((items, index) => {
      //console.log("item", Object.keys(items));
      let obj = {};
      Object.keys(items).forEach((item) => {
        //console.log("tt", item, items[item]);
        obj[
          JSON.parse(JSON.stringify(item.replace(/[\n\r]+/g, "")))
        ] = JSON.parse(JSON.stringify(items[item].replace(/[\n\r]+/g, "")));
      });
      dataMap.push(obj);
    });
    console.log("dataMap", dataMap);
    console.log("errors", errors);
    if (file.size > 1048576 * 10) {
      console.log("file.size");
      alert("Please image size of less than 1 MB");
    }

    var config = {
      method: "post",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/uploadcsv",
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(dataMap),
      params: {
        universityCode: UniCode,
        category: "Department",
      },
    };
    // try {
    //   let data = await axios(config);
    //   console.log("data check", data);
    //   if (data.data.data.length > 0) {
    //     setErrorsProcessed(true);

    //     const errorsOutput = [...data.data.data];
    //     // console.log("errorsOutput1", errorsOutput);
    //     errorsOutput.map((item, index) => {
    //       item["Id"] = index + 1;
    //     });
    //     console.log("errorsOutput2", errorsOutput);
    //     setErrors(errorsOutput);
    //     const json2csvParser = new Parser();
    //     const csv = json2csvParser.parse(errorsOutput);
    //     setCSVErrors(csv);
    //     dispatch(getDepartments(UniCode));
    //   } else {
    //     setErrorsProcessed(false);
    //     props.onClose();
    //     dispatch(getDepartments(UniCode));
    //   }
    // } catch (e) {
    //   console.log("error uploading file", e);
    // }
  };
  const config = {
    quotes: false, //or array of booleans
    quoteChar: '"',
    escapeChar: '"',
    delimiters: ",",
    header: true,
    newline: "\n\r",
    skipEmptyLines: false, //other option is 'greedy', meaning skip delimiters, quotes, and whitespace.
  };

  useEffect(() => {
    const downloadCSV = async function () {
      try {
        let response = await axios.get(csvtemplate, {
          baseURL: window.location.origin,
          responseType: "blob",
        });
        setCSVData(response.data);
      } catch (e) {
        console.log("error", e);
      }
    };

    downloadCSV();
  }, []);

  const columns = [
    {
      id: "Name",
      displayName: "Name",
    },
    {
      id: "departmentHead",
      displayName: "DepartmentHead",
    },
  ];
  //console.log("CSVFile", CSVFile);
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      {errorProcessed ? (
        <Modal
          open={errorProcessed}
          onClose={() => setErrorsProcessed(false)}
          dimmer={"inverted"}
        >
          <Modal.Header>Data that could not be processed</Modal.Header>
          <Modal.Content>
            <List as="ul">
              {errors.map((item, index) => (
                <List.Item as="li" key={index}>
                  {item.Name}
                </List.Item>
              ))}
            </List>
          </Modal.Content>
          <Modal.Actions>
            <a
              href={
                "data:attachment/csv;charset=utf-8," +
                encodeURIComponent(errorsCSV)
              }
              download="DepartmentsErrors.csv"
              role="button"
            >
              <Button>Download Erros CSV</Button>
            </a>
          </Modal.Actions>
        </Modal>
      ) : (
        <React.Fragment />
      )}
      <Modal.Header>
        <Button
          onClick={() => {
            setUploadData(false);
            setDownloadTemplate(true);
          }}
        >
          DownLoadTemplete
        </Button>
        <Button
          onClick={() => {
            setUploadData(true);
            setDownloadTemplate(false);
          }}
        >
          Upload csv
        </Button>
      </Modal.Header>
      <Modal.Content>
        {downloadTemplate ? (
          // <button
          //   onClick={() => {
          //     FileDownload(csvData, "DownloadTemplate.csv");
          //   }}
          // >
          //   DownLoadTemplete
          // </button>
          // <CSVDownloader data={csvData} filename="DepartmentTemplate.csv">
          //   Download Template
          // </CSVDownloader>
          <CsvDownloader
            filename="DepartmentTemplate"
            extension=".csv"
            onError={errorUpload}
            columns={columns}
            text="DepartmentTemplate"
          />
        ) : (
          <React.Fragment />
        )}
        {uploadData ? (
          <div>
            {spinner ? (
              <div className="ui active inverted dimmer">
                <div className="ui loader"></div>
              </div>
            ) : (
              <React.Fragment />
            )}
            <CSVReader
              onFileLoaded={(data, fileInfo) => handleChange(data, fileInfo)}
              parserOptions={config}
            />
            Upload File (less than 10MB)
          </div>
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
    </Modal>
  );
};

class CreateDepartments extends React.Component {
  state = {
    myProfile: false,
    addDepartments: false,
    departments: [],
    uploadData: false,
  };
  componentDidMount = () => {
    this.setState({ departments: this.props.departments });
  };
  componentDidUpdate = (previousProps) => {
    if (previousProps.departments !== this.props.departments) {
      this.setState({ departments: this.props.departments });
    }
  };
  removeDepartmentPopUpBox = () => {
    this.setState({ addDepartments: false });
  };
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };
  renderDepartments() {
    var departments = this.state.departments;
    return departments.map((item, index) => {
      let { departmentCode, departmentName, UniDeptHead } = item;
      UniDeptHead = UniDeptHead ? UniDeptHead : "";
      return (
        <tr key={index}>
          <td>{departmentCode}</td>
          <td>{departmentName}</td>
          <td>{UniDeptHead}</td>
        </tr>
      );
    });
  }
  render() {
    if (this.props.isAuthenticated) {
      let data = this.props.userData.UserName;
      let name = data["UserName"];
      return (
        <div className="main-content bg-light">
          <Modal
            open={this.state.myProfile}
            onClose={() => this.setState({ myProfile: false })}
            style={{ overflow: "auto", maxHeight: 600 }}
            dimmer={"inverted"}
          >
            <MyProfile
              myProfile={data}
              removeMyProfile={this.removeMyProfile}
            />
          </Modal>
          <UploadDepartmentData
            open={this.state.uploadData}
            onClose={() => this.setState({ uploadData: false })}
          />

          <header>
            <h4>
              <label htmlFor="nav-toggel">
                <span>
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </span>
              </label>
              <span className="name">Create Departments</span>
            </h4>

            <div className="user-wrapper" onClick={this.myProfile}>
              <img
                src={this.props.profileImageUrl}
                width="50px"
                height="50px"
                alt=""
              />
              <div>
                <h6>{name}</h6>
                <small>Admin</small>
              </div>
            </div>
          </header>
          <main>
            {this.state.addDepartments ? (
              <AddDepartments
                open={this.state.addDepartments}
                onClose={() => this.setState({ addDepartments: false })}
                removeDepartmentPopUpBox={this.removeDepartmentPopUpBox}
              />
            ) : (
              <React.Fragment />
            )}

            <div className="table-content m-3">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Department Code</th>
                    <th scope="col">Department Name</th>
                    <th scope="col">Department Head</th>
                  </tr>
                </thead>
                <tbody>{this.renderDepartments()}</tbody>
              </table>
            </div>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ addDepartments: true })}
            >
              Add Department
            </button>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ uploadData: true })}
            >
              Upload Data
            </button>
            {/* <Link to="./csv/DepartmentsTemplate.csv" target="_blank" download>
              Download
            </Link> */}
          </main>
        </div>
      );
    } else {
      return <Redirect push to="/" />;
    }
  }
}

// const CreateDepartments = function (props) {
//   const [myProfile, setMyProfile] = useState(false);
//   const [addDepartments, setAddDepartments] = useState(false);
//   const [departments, setDepartments] = useState([]);
//   const [uploadData, setUploadData] = useState(false);

//   useEffect(() => {
//     setAddDepartments(departments);
//   }, [departments]);

//   const removeDepartmentPopUpBox = () => {
//     setAddDepartments(false);
//   };
//   const myProfileFunction = () => {
//     setMyProfile(true);
//   };
//   const removeMyProfile = () => {
//     setMyProfile(false);
//   };
//   const renderDepartments = () => {
//     var departments = this.state.departments;
//     return departments.map((item, index) => {
//       let { departmentCode, departmentName, UniDeptHead } = item;
//       UniDeptHead = UniDeptHead ? UniDeptHead : "";
//       return (
//         <tr key={index}>
//           <td>{departmentCode}</td>
//           <td>{departmentName}</td>
//           <td>{UniDeptHead}</td>
//         </tr>
//       );
//     });
//   };

//   return (
//     <div className="main-content bg-light">
//       <Modal
//         open={myProfile}
//         onClose={() => setMyProfile(false)}
//         style={{ overflow: "auto", maxHeight: 600 }}
//         dimmer={"inverted"}
//       >
//         <MyProfile
//           myProfile={props.userData}
//           removeMyProfile={removeMyProfile}
//         />
//       </Modal>
//       <UploadDepartmentData
//         open={uploadData}
//         onClose={() => setUploadData(false)}
//       />

//       <header>
//         <h4>
//           <label htmlFor="nav-toggel">
//             <span>
//               <i className="fa fa-bars" aria-hidden="true"></i>
//             </span>
//           </label>
//           <span className="name">Create Departments</span>
//         </h4>

//         <div className="user-wrapper" onClick={myProfileFunction}>
//           <img src={props.profileImageUrl} width="50px" height="50px" alt="" />
//           <div>
//             <h6>{props.userData.UserName}</h6>
//             <small>Admin</small>
//           </div>
//         </div>
//       </header>
//       <main>
//         {addDepartments ? (
//           <AddDepartments
//             open={addDepartments}
//             onClose={() => setAddDepartments(false)}
//             removeDepartmentPopUpBox={removeDepartmentPopUpBox}
//           />
//         ) : (
//           <React.Fragment />
//         )}

//         <div className="table-content m-3">
//           <table className="table table-hover">
//             <thead>
//               <tr>
//                 <th scope="col">Department Code</th>
//                 <th scope="col">Department Name</th>
//                 <th scope="col">Department Head</th>
//               </tr>
//             </thead>
//             <tbody>{renderDepartments()}</tbody>
//           </table>
//         </div>
//         <button
//           className="btn btn-primary mb-2"
//           style={{ backgroundColor: "#4cadad" }}
//           onClick={() => setAddDepartments(true)}
//         >
//           Add Department
//         </button>
//         <button
//           className="btn btn-primary mb-2"
//           style={{ backgroundColor: "#4cadad" }}
//           onClick={() => setUploadData(true)}
//         >
//           Upload Data
//         </button>
//       </main>
//     </div>
//   );
// };

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    departments: state.departments,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, null)(CreateDepartments);
