import React from "react";
import { connect } from "react-redux";
import MyProfile from "../popups/MyProfile2";
import { Modal } from "semantic-ui-react";

import "../../css/student.css";
import "../../css/schedule.css";
import Calendar from "react-calendar";
import moment from "moment";
//import CreateSchedule from "./createSchedule";
import { Field, reduxForm } from "redux-form";
import AlertComponent from "../../auth/AlertComponent";
import { loadClassData } from "../../../actions/appSyncActions";
import "react-datetime/css/react-datetime.css";
import { alertAction, removeAlert } from "../../../actions/authActions";
import CreateSchedule from "./createSchedule";
const { TaskMain } = require("./Task.js");
const { ProfessorSubmitTask } = require("./ProfessorForm.js");
const { LocationSchedule } = require("./LocationForm.js");

const validate = (formValues) => {
  let error = {};
  if (!formValues.admissionYear) {
    error.admissionYear = "* Please provide a Admission Year";
  }
  if (!formValues.departmentName) {
    error.departmentName = "* Please provide a Department Name";
  }
  if (!formValues.semester) {
    error.semester = "* Please provide a Semester";
  }
  if (!formValues.division) {
    error.division = "* Please provide a Division";
  }
  return error;
};

class Schedule extends React.Component {
  state = {
    createScheduleFlag: false,
    profNames: [],
    locations: [],
    defaultFilter: "Class", //whenSchedulePageIsOpenedDefualtFilter is the one who decides which form to Show
    people: [{ item: "Class" }, { item: "Professor" }, { item: "Location" }],
    taskHidden: true, //to show sidebar for task
    isSpinner: false, //to show spinner when data is loaded from DB.
    myProfile: false, //to show pop up of filter page
    value: new Date(), //default date for calender
    selectedDate: new Date(),
    departmentName: "", // this is set on onChangeCheck
    semester: "", //  this is set on onChangeCheck
    division: "", //  this is set in onSubmit function
    processedScheduleData: {},
    processedDateData: {}, //contains the date related schedule
    admissions: [],
    admissionYear: "",
    departments: [],
    partialDepartments: [],
    departmentCode: "",
    classesDivision: [],
    partialDivision: [],
    admissionCode: "",
    admissionsDept: [],
    classesSemester: [],
    partialSemester: [],
    disableSemester: true,
    disableDivision: true,
    disableDepartment: true,
    taskDetails: "", // this values gets filled every time there a class is picked
    sttartTime: new Date(),
    reCallOnSubmit: false,
    finalFormValues: {},
  };

  // when we click on top left part of the page this functions gets called
  // for popup editProfile and uploaded photo to open up.
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  updateScheduleDataSingleDate = (date, data) => {
    let processedScheduleData = this.state.processedScheduleData;

    processedScheduleData[date] = data;
    console.log("schedulprocessedScheduleDataeData", processedScheduleData);
    this.setState({
      processedScheduleData: processedScheduleData,
      processedDateData:
        processedScheduleData[moment(new Date(date)).format("YYYY-MM-DD")] ||
        {},
    });
  };

  updateScheduleDataDouble = (date1, date2, data1, data2) => {
    let processedScheduleData = this.state.processedScheduleData;
    console.log("processedScheduleData", processedScheduleData);
    processedScheduleData[date1] = data1;
    processedScheduleData[date2] = data2;
    this.setState({
      processedScheduleData: processedScheduleData,
      processedDateData:
        processedScheduleData[moment(new Date(date1)).format("YYYY-MM-DD")] ||
        {},
    });
  };
  // when we want admins profile screen to go away we click outside which basically calls this functiou
  // when we submit the form with chnaged value this function is called to let admin pop up go away
  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };

  //when we have Class as DefautFilter we first selectAdmissionYear.
  onChangeCheck = (e) => {
    //after selecting admissionYear is selected this one gets called
    if (e.target.name === "admissionYear") {
      let admissionsDepartmentFiltered = this.state.admissionsDept;
      let tempDepartmentCodes = [];
      //corresponding to admissionYear we find out the deaprtmentCode
      admissionsDepartmentFiltered = admissionsDepartmentFiltered.filter(
        (item) => {
          console.log("item", item, e.target.value);
          if (item.admissionYear == e.target.value) {
            tempDepartmentCodes.push(item.departmentCode);
            return true;
          } else {
            return false;
          }
        }
      );
      //for the selected departmentCode we select their names from partialDepartments
      //department->Code to departmentName
      console.log("tempDepartmentCodes", tempDepartmentCodes);
      console.log("this.state.departments", this.state.departments);
      let partialDepartments = this.state.departments;
      partialDepartments = partialDepartments.filter((item) => {
        if (tempDepartmentCodes.includes(item.departmentCode)) {
          return true;
        } else {
          return false;
        }
      });
      console.log("partialDepartments", partialDepartments);
      this.setState({
        partialDepartments: partialDepartments,
        admissionYear: e.target.value, // sets the state with selected admissionYear
        disableDepartment: false, //enable departmentFilter
      });
    }

    //after selecting departmentName is selected this one gets called
    if (e.target.name === "departmentName") {
      //we find the departmentCode corresponding to which department exists
      //departmentName to department->Code
      let departmentCode = this.state.departments.find((item) => {
        return item.departmentName === e.target.value;
      });

      //finding out the admission code corresponsing to department and admission Year.
      let admissionCode = this.state.admissionsDept.filter((item) => {
        if (
          item.departmentCode == departmentCode.departmentCode &&
          item.admissionYear == this.state.admissionYear
        ) {
          return true;
        } else {
          return false;
        }
      });

      // finding out the classes corresponding to admission code and making sure that semester is unique.
      let semesters = [];
      let classes = this.state.classesSemester.filter((item) => {
        if (
          !semesters.includes(item.semester) &&
          item.admissionCode === admissionCode[0].admissionCode
        ) {
          semesters.push(item.semester);
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        disableSemester: false,
        departmentCode: departmentCode.departmentCode,
        departmentName: e.target.value,
        admissionCode: admissionCode[0].admissionCode,
        partialSemester: classes,
      });
    }

    //after selecting Semester is selected this one gets called
    if (e.target.name === "semester") {
      let divisions = [];

      let classesDivision = this.state.classesDivision.filter((item) => {
        if (
          //the division may have same value of division corresponding to different semester
          //to show unique value for division we fill division array and have unique values in it.
          !divisions.includes(item.division) &&
          item.admissionCode == this.state.admissionCode &&
          item.semester == e.target.value
        ) {
          divisions.push(item.division);
          return true;
        } else {
          return false;
        }
      });
      // DEBUG
      //console.log("classesDivision", classesDivision);
      this.setState({
        disableDivision: false, //enable division Field
        semester: e.target.value, //sets the state with semester values
        partialDivision: classesDivision, //sets partialDivision to show the division of this semster
      });
    }
  };

  //when we select filter other than Class ,Professor or Locations
  //when we have information about a professor or location to be displayed in schedule page
  //we call this function with pocessed data to show the schedule page.
  renderScheduleData = (processedScheduleData) => {
    this.setState({
      processedScheduleData: processedScheduleData,
      processedDateData:
        processedScheduleData[moment(new Date()).format("YYYY-MM-DD")] || {},
    });
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="filter">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((person) => (
              <option key={person.item} value={person.item}>
                {person.item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  //most important function in the Schedule component
  //this same function is used in ProfessorForm
  processScheduleData = (unfilteredScheduleData) => {
    var dataMapped = {};
    unfilteredScheduleData.forEach((item) => {
      var itemDate = moment(item.Date).format("YYYY-MM-DD");
      if (!dataMapped.hasOwnProperty(itemDate)) {
        dataMapped[itemDate] = [];
      }
      item["StartTime"] = moment(item.newStartTime);
      item["EndTime"] = moment(item.newEndTime);
      dataMapped[itemDate].push(item);
    });
    // all the keys in dataMapped are the dates in yyyy-mm-dd format and contains the starttime and end time
    //of various classes

    var dataMappedMain = {};
    Object.keys(dataMapped).forEach((item) => {
      dataMappedMain[item] = {};
      for (let i = 4; i <= 23; i++) {
        dataMappedMain[item][i] = [];
      }
      //corresponding to each date we set the values of classes from 6 to 21
      dataMapped[item].forEach((data) => {
        let startTime = moment(data.StartTime);
        let endTime = moment(data.EndTime);
        let minutes = moment.duration(endTime.diff(startTime)).asMinutes();
        //we find out how long the classes will go on and set the hieght of the class in as minutes
        //dataMappedMain has values of each date corresponding to hours.
        //each hous has top values telling how much after beginnning of hour the schedule will lie.
        dataMappedMain[item][startTime.hour()].push({
          ...data,
          top: startTime.minutes(),
          height: minutes,
        });
      });
    });

    /* corresponding to each data there is an hour and corresponding to every hour there is an array.
    {
    '2021-04-01':{
        8: [],
        9: []
      }
    }*/

    Object.keys(dataMappedMain).forEach((item) => {
      //we iterate through each day
      Object.keys(dataMappedMain[item]).forEach((data) => {
        //for each data hour where we have no class set the value of height and top to zero.
        if (dataMappedMain[item][data].length === 0) {
          dataMappedMain[item][data].push({ height: 0, top: 0 });
        }
      });
    });
    console.log("dataMappedMain", dataMappedMain);
    return dataMappedMain;
  };

  componentDidMount = () => {
    let partialAdmissionsYear = [];
    console.log("reCallOnSubmit", this.state.reCallOnSubmit);
    console.log("admissions", this.props.admissions);
    //filtering the admission array to have unique admission year also.
    let tempAdmissionsYear = this.props.admissions.filter((item) => {
      if (!partialAdmissionsYear.includes(item.admissionYear)) {
        partialAdmissionsYear.push(item.admissionYear);
        return true;
      } else {
        return false;
      }
    });
    this.setState({
      locations: this.props.scheduleData.Locations,
      profNames: this.props.scheduleData.ProfNames,
      processedScheduleData: [],
      admissions: tempAdmissionsYear,
      admissionsDept: this.props.admissions,
      departments: this.props.departments,
      classesSemester: this.props.classes, //semester
      classesDivision: this.props.classes, // division
    });
  };

  componentDidUpdate = (previousProps) => {
    console.log("this.props.reCallScheduleAp", this.props.reCallScheduleApi);
    if (previousProps.admissions !== this.props.admissions) {
      let partialAdmissionsYear = [];
      let tempAdmissionsYear = this.props.admissions.filter((item) => {
        if (!partialAdmissionsYear.includes(item.admissionYear)) {
          partialAdmissionsYear.push(item.admissionYear);
          return true;
        } else {
          return false;
        }
      });
      this.setState({
        admissions: tempAdmissionsYear,
        admissionsDept: this.props.admissions,
      });
    }
    if (previousProps.departments !== this.props.departments) {
      this.setState({
        departments: this.props.departments,
      });
    }
    if (previousProps.classes !== this.props.classes) {
      this.setState({
        classesSemester: this.props.classes, //semester
        classesDivision: this.props.classes, // division
      });
    }
    if (previousProps.scheduleData !== this.props.scheduleData) {
      this.setState({
        locations: this.props.scheduleData.Locations,
        profNames: this.props.scheduleData.ProfNames,
      });
    }
    if (this.props.reCallScheduleApi && this.state.reCallOnSubmit) {
      this.setState({
        reCallOnSubmit: false,
      });
      console.log(
        "we have reached the case where we hvae to call onSubmit again",
        this.props.reCallScheduleApi,
        this.state.reCallOnSubmit
      );
      this.onSubmit(this.state.finalFormValues);
    }
  };

  //when we click on a schedule of a class on a day
  //this function gets called
  //taskDetails is whats gets passed to task Component
  //taskHidden is set to true and then set to false so that task gets reloaded to intital state
  onScheduleClick = (values) => {
    this.setState({ taskHidden: true });
    setTimeout(() => {
      this.setState({ taskHidden: false, taskDetails: values });
    }, 0);
  };

  renderClass = (item, top, height, values) => {
    let subject = "",
      startTime = "",
      endTime = "",
      minutes = "",
      location = "",
      profName = "";
    if (height !== 0) {
      subject = values.SubjName;
      startTime = moment(values.newStartTime).format("h:mm");
      endTime = moment(values.newEndTime).format("h:mm");
      minutes = moment
        .duration(moment(values.newEndTime).diff(moment(values.newStartTime)))
        .asMinutes();
      location = values.Location;
      profName = values.ProfName;
    }
    return (
      <div
        onClick={() => this.onScheduleClick(values)}
        //className="hidden"
        style={{ display: height ? "" : "none" }}
        className="row timeline-row"
      >
        <div className="col-md-12 timeline-pad" style={{ top: top + "px" }}>
          <div
            className="timeline-inner"
            id={item % 2 ? "cat-2" : "cat-1"}
            style={{ height: height - 1 + "px" }}
          >
            <div className="t-block">
              <p>
                <strong>{subject}</strong>
              </p>
              <p>
                {startTime} - {endTime} ({minutes} minutes)
              </p>
            </div>
            <div className="t-block">
              <p>
                <small>{location}</small>
              </p>
              <p>{profName}</p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  renderLi = (item) => {
    return (
      <li>
        <span></span>
        {item.data.map((values) =>
          this.renderClass(item.item, values.top, values.height, values)
        )}
        <div className="time">
          <span>{item.item}</span>
        </div>
      </li>
    );
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  //when we click on calender Date on schedule page this functions gets called to render the schedule of
  // the particular date.
  onChange = (e) => {
    let value = moment(new Date(e)).format("YYYY-MM-DD");
    this.setState({
      taskHidden: true,
      selectedDate: value,
      processedDateData: this.state.processedScheduleData[value] || {},
    });
  };

  renderList = () => {
    let dateList = this.state.processedDateData;
    let newList = [];
    Object.keys(dateList).forEach((item) => {
      newList.push({ item, data: dateList[item] });
    });

    // this is how the list component is built out.
    // let list = [
    //   { item: 8, data: [{ top: 0, height: 0 }] },
    //   { item: 9, data: [{ top: 30, height: 60 }] },
    //   { item: 10, data: [{ top: 30, height: 150 }] },
    //   { item: 11, data: [{ top: 30, height: 0 }] },
    //   { item: 12, data: [{ top: 30, height: 0 }] },
    //   { item: 13, data: [{ top: 30, height: 60 }] },
    //   { item: 14, data: [{ top: 0, height: 0 }] },
    //   { item: 15, data: [{ top: 30, height: 60 }] },
    //   { item: 16, data: [{ top: 30, height: 60 }] },
    // ];
    return (
      <ul>
        {newList.map((item) => {
          return this.renderLi(item);
        })}
      </ul>
    );
  };

  onSubmit = async (formValues) => {
    //console.log("formValues", formValues);
    this.props.removeAlert();
    this.setState({
      isSpinner: true,
      finalFormValues: formValues,
      reCallOnSubmit: true,
    });
    let classId = this.state.classesDivision.filter((item) => {
      return (
        item.division === formValues.division &&
        item.semester === formValues.semester &&
        item.admissionCode === this.state.admissionCode
      );
    });

    try {
      if (classId.length === 0) {
        console.log("error");
        await new Promise((res, reject) => reject("Class was not found"));
      }

      let data = await this.props.loadClassData({
        UniCode: this.props.UniCode,
        classId: classId[0].idClass,
      });
      let filteredScheduleData = this.processScheduleData(data);

      this.setState({
        taskHidden: true,
        division: formValues.division, // this value is being set last after the form is submiited
        isSpinner: false,
        processedScheduleData: filteredScheduleData,
        selectedDate: moment(new Date()).format("YYYY-MM-DD"),
        processedDateData:
          filteredScheduleData[moment(new Date()).format("YYYY-MM-DD")] || {},
      });
    } catch (e) {
      console.log("eSCHEDULE EERROORR", e);
      //this.props.alertAction(e);
      setTimeout(() => {
        this.props.removeAlert();
      }, 5000);
      this.setState({
        isSpinner: false,
        processedScheduleData: [],
        selectedDate: moment(new Date()).format("YYYY-MM-DD"),
        processedDateData: {},
      });
    }

    //
  };
  // on the schedule page when we increase the date to next days this function gets called
  // we calculate the new date //541
  // we take out the schedule of incremented date we update porcessed data of state with the new scheule
  // of the new date
  incrementDate = () => {
    let value = this.state.selectedDate;
    let currentMomentDate = new moment(new Date(value));
    let incrementedDate = currentMomentDate.add(1, "days");

    this.setState({
      taskHidden: true,
      selectedDate: moment(incrementedDate).format("YYYY-MM-DD"),
      processedDateData:
        this.state.processedScheduleData[
          moment(incrementedDate).format("YYYY-MM-DD")
        ] || {},
    });
  };

  //works in same way as incremented date
  decrementDate = () => {
    let value = this.state.selectedDate;
    let currentMomentDate = new moment(new Date(value));
    let decrementedDate = currentMomentDate.subtract(1, "days");

    this.setState({
      taskHidden: true,
      selectedDate: moment(decrementedDate).format("YYYY-MM-DD"),
      processedDateData:
        this.state.processedScheduleData[
          moment(decrementedDate).format("YYYY-MM-DD")
        ] || {},
    });
  };
  //when we want to see schedule according to location or prof Name
  // we select the dropdown,when a value is selected this function is called.
  //defualtFilter changes from class to Professor or Location
  //people in state basically holds all the value from dropdown.
  optionCheck = (event) => {
    this.setState({
      defaultFilter: event.target.value,
      taskHidden: true, // if we dont set this value changing from professor to location or class wont remove task bar
      processedScheduleData: {},
      processedDateData: {},
    });
  };
  createSchedulePopUp = () => {
    this.setState({
      createScheduleFlag: !this.state.createScheduleFlag,
    });
  };

  render = () => {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          style={{ overflow: "auto", maxHeight: 600 }}
          dimmer={"inverted"}
        >
          <MyProfile myProfile={data} removeMyProfile={this.removeMyProfile} />
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">Schedule</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img
              src={this.props.profileImageUrl}
              width="50px"
              height="50px"
              alt=""
            />
            <div>
              <h6>{name}</h6>
              <small>Admin</small>
            </div>
          </div>
        </header>

        <main>
          <div>
            <div className="filter-wrapper">
              <label htmlFor="dropDownSelect">Schedule Filter</label>
              <div>
                <select onChange={this.optionCheck}>
                  {this.state.people.map((person) => (
                    <option key={person.item} value={person.item}>
                      {person.item}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <button
                  className="btn btn-primary mb-2"
                  onClick={this.createSchedulePopUp}
                  style={{ backgroundColor: "#4cadad" }}
                >
                  Create Schedule
                </button>
              </div>
              {this.state.createScheduleFlag ? (
                <CreateSchedule
                  createScheduleFlag={this.state.createScheduleFlag}
                  createSchedulePopUp={this.createSchedulePopUp}
                />
              ) : (
                <React.Fragment />
              )}
            </div>
            {this.state.defaultFilter === "Class" ? (
              <form
                className="filter-wrapper"
                onSubmit={this.props.handleSubmit(this.onSubmit)}
              >
                <Field
                  disabled={false}
                  label="Admission Year"
                  name="admissionYear"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.admissions.map(function (item) {
                    return { item: item.admissionYear };
                  })}
                />
                <Field
                  disabled={this.state.disableDepartment}
                  label="Department Name"
                  name="departmentName"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.partialDepartments.map(function (item) {
                    return { item: item.departmentName };
                  })}
                />
                <Field
                  disabled={this.state.disableSemester}
                  label="Semester"
                  name="semester"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.partialSemester.map(function (item) {
                    return { item: item.semester };
                  })}
                />
                <Field
                  disabled={this.state.disableDivision}
                  label="Division"
                  name="division"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.partialDivision.map(function (item) {
                    return { item: item.division };
                  })}
                />
                <div className="filter fil-btn">
                  <label></label>
                  <button
                    type="submit"
                    className="btn btn-primary mb-2"
                    style={{ backgroundColor: "#4cadad" }}
                  >
                    GO
                  </button>
                  <AlertComponent />
                </div>
                {this.state.isSpinner ? (
                  <div className="ui active inverted dimmer">
                    <div className="ui loader"></div>
                  </div>
                ) : (
                  <React.Fragment />
                )}
              </form>
            ) : (
              <React.Fragment />
            )}

            {this.state.defaultFilter === "Professor" ? (
              <ProfessorSubmitTask
                renderScheduleData={this.renderScheduleData}
              />
            ) : (
              <React.Fragment />
            )}

            {this.state.defaultFilter === "Location" ? (
              <LocationSchedule renderScheduleData={this.renderScheduleData} />
            ) : (
              <React.Fragment />
            )}
          </div>

          <div className="schedule-content p-3">
            <div className="row mt-4">
              <div className="col-lg-3 ">
                <div className="row">
                  <div className="col-12">
                    <div className="calendar" style={{ borderRadius: "5px" }}>
                      <div className="datepicker">
                        <div className="cal-inner-wrapper">
                          <Calendar
                            value={this.state.value}
                            onChange={this.onChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 mt-5">
                    <div className="category-wrapper">
                      <div className="category">
                        <h4>Category</h4>
                      </div>
                      <div
                        className="search-wrapper mt-4"
                        style={{ paddingLeft: "15px", borderRadius: "10px" }}
                      >
                        <input type="search" placeholder="Search" />
                      </div>

                      <div className="category-list">
                        <ul>
                          <li>
                            <label className="container">
                              Select All
                              <input type="checkbox" />
                              <span className="checkmark"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Batch
                              <input type="checkbox" />
                              <span className="checkmark " id="cat-1"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              {" "}
                              Course
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-2"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              ClassName
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-3"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Division
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-4"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Teaching Staff
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-5"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Type of ClassName
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-6"></span>
                            </label>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className={this.state.taskHidden ? "col-lg-9" : "col-lg-6"}>
                <div
                  className="timeline bg-white"
                  style={{ borderRadius: "5px" }}
                >
                  <div className="timeline-head">
                    <div className="timeline-heading">
                      <h4>
                        {moment(this.state.selectedDate).format("MMMM Do YYYY")}
                      </h4>
                    </div>
                    <div className="date-inc">
                      <span>
                        <button onClick={this.decrementDate}>
                          <img src="../image/1.png" alt="" />
                        </button>
                      </span>
                      <span>
                        <button onClick={this.incrementDate}>
                          <img src="../image/2.png" alt="" />
                        </button>
                      </span>
                    </div>
                  </div>
                  {this.renderList()}
                </div>
              </div>
              {this.state.taskHidden ? (
                <React.Fragment />
              ) : (
                <TaskMain
                  updateScheduleDataSingleDate={
                    this.updateScheduleDataSingleDate
                  }
                  updateScheduleDataDouble={this.updateScheduleDataDouble}
                  taskHidden={this.state.taskHidden}
                  admissionYear={this.state.admissionYear}
                  departmentName={this.state.departmentName}
                  division={this.state.division}
                  semester={this.state.semester}
                  taskDetails={this.state.taskDetails}
                  processScheduleData={this.processScheduleData}
                />
              )}
            </div>
          </div>
        </main>
      </div>
    );
  };
}

const formWrapped = reduxForm({
  form: "Display Schedule",
  validate,
})(Schedule);

const mapStateToProps = (state) => {
  return {
    scheduleData: state.scheduleDataInformation,
    userData: state.userData,
    classes: state.classes,
    admissions: state.batch.admissionsData,
    departments: state.departments,
    UniCode: state.userData.UniCode,
    profileImageUrl: state.uploadedImage.data,
    reCallScheduleApi: state.reCallScheduleApi,
  };
};

export default connect(mapStateToProps, {
  loadClassData,
  alertAction,
  removeAlert,
})(formWrapped);

/*
explaination  mapStateToProps 


scheduleData: state.scheduleDataInformation,
//schedule data is loaded from initial logic ,more explain in ProfssorFrom component in this direcotry
userData: state.userData,
// details of the logged in user MyProfile needs these values to passed if we want to change them.
classes: state.classes,
//all the class related data that is loaded up at the time of admin login
admissions: state.admissions,
//all the admission related data that is loaded up at the time of admin login
departments: state.departments,
//all the departments related data that is loaded up at the time of admin login
UniCode: state.userData.UniCode,
//this is university component that gets the value of current university Code
profileImageUrl: state.uploadedImage.data,
//the URL of downloaded image the images also gets downloaded when the user logs in.



*/
