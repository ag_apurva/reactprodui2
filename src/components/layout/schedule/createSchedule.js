import React from "react";
//import Modal from "@material-ui/core/Modal";
//import Draggable from "react-draggable";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import TextField from "@material-ui/core/TextField";
import DateTime from "react-datetime";
import moment from "moment";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { createSchedule } from "../../../actions/scheduleActions";
import { Modal } from "semantic-ui-react";
const validate = (formValues) => {
  // console.log("formValues", formValues);
  let error = {};
  //admissionYear,departmentName,semester,division
  if (!formValues.admissionYear) {
    error.admissionYear = "Please select a Admission Year";
  }
  if (!formValues.departmentName) {
    error.departmentName = "Please select a Department Name";
  }
  if (!formValues.semester) {
    error.semester = "Please select a Semester";
  }
  if (!formValues.division) {
    error.division = "Please select a Division";
  }
  if (!formValues.location) {
    error.location = "Please select a Location";
  }
  if (!formValues.subject) {
    error.subject = "Please select a Subject";
  }
  if (!formValues.professorName) {
    error.professorName = "Please select a ProfessorName";
  }
  if (!formValues.startDate) {
    error.startDate = "Please select a Start Date";
  }
  if (!formValues.endDate) {
    error.endDate = "Please select a End Date";
  }
  if (!formValues.startTime) {
    error.startTime = "Please select a Start Time";
  }
  if (!formValues.endTime) {
    error.endTime = "Please select a End Time";
  }

  if (
    formValues.startTime &&
    formValues.endTime &&
    moment(formValues.startTime) > moment(formValues.endTime)
  ) {
    error.endTime = "Please select endTime greater than startTime";
  }
  return error;
};

const reCallScheduleApi = function () {
  return async function (dispatch) {
    dispatch({ type: "SCHEDULE_CREATED", payload: true });
    dispatch({ type: "SCHEDULE_CREATED_DONE", payload: false });
  };
};

class CreateSchedule extends React.Component {
  state = {
    conflictSchedule: false,
    error: "",
    isSpinner: false,
    currentDate: new Date(),
    startDate: new Date(),
    endDate: new Date(),
    startTime: moment(new Date()),
    endTime: moment(new Date()),
    locationDefault: "",
    profDefault: "",
    subjectDefault: "",
    scheduleDataInfo: {},
    locations: [],
    profNames: [],
    subjectNames: [],
    value: "hello",
    disableDepartment: false,
    disableSemester: false,
    disableDivision: false,
    admissions: [],
    admissionsDept: [],
    departments: [],
    classesSemester: [],
    partialDepartments: [],
    partialSemester: [],
    partialDivision: [],
  };

  onSubmit = async (formValues) => {
    this.setState({ isSpinner: true });
    let classId = this.state.classesDivision.filter((item) => {
      return (
        item.division === formValues.division &&
        item.semester === formValues.semester &&
        item.admissionCode === this.state.admissionCode
      );
    });

    try {
      if (classId.length === 0) {
        console.log("error");
        await new Promise((res, reject) => reject("Class was not found"));
      }

      let professorEmail;
      if (formValues.professorName !== null) {
        let data = this.state.profNames.filter((item) => {
          console.log("item", item);
          return (
            item.UserName.toLowerCase() ===
            formValues.professorName.toLowerCase()
          );
        });
        professorEmail = data[0].UserEmail;
      }
      console.log("professorEmail", professorEmail);
      let formData = {
        classId: classId[0].idClass,
        startDate: moment(formValues.startDate).format("YYYY-MM-DD"),
        endDate: moment(formValues.endDate).format("YYYY-MM-DD"),
        endTime: moment(formValues.endTime).format("HH:mm"),
        startTime: moment(formValues.startTime).format("HH:mm"),
        startTimeBoundaryCheck: moment(formValues.startTime)
          .add(1, "minute")
          .format("HH:mm"),
        endTimeBoundaryCheck: moment(formValues.endTime)
          .subtract(1, "minute")
          .format("HH:mm"),
        subject: formValues.subject,
        location: formValues.location,
        professorName: formValues.professorName,
        professorEmail,
        UniCode: this.props.UniCode,
      };
      // let sstart = moment(formValues.startDate);
      // let eendDate = moment(formValues.endDate);

      console.log("formData", formData);
      let data = await this.props.createSchedule(formData);

      if (data.data.hasOwnProperty("error")) {
        console.log("data", data.data.error);
        this.setState({
          conflictSchedule: true,
          error: data.data.error,
        });
      } else {
        this.props.reCallScheduleApi();
        this.props.createSchedulePopUp();
      }
      //this.props.createSchedulePopUp()
    } catch (e) {
      console.log("eSCHEDULE EERROORR", e);
    }

    this.setState({
      isSpinner: false,
    });
    /*

classId: "10"
endDate: "2021-05-15"
endTime: "14:05"
location: "CL 21 Main Bldg"
professorName: "Adiyta"
startDate: "2021-05-15"
startTime: "12:55"
subject: "Chemistry"
__proto__: Object

      */
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  filterOptions = createFilterOptions({
    matchFrom: "start",
    stringify: (option) => option.title,
  });

  componentDidMount = () => {
    let partialAdmissionsYear = [];

    //filtering the admission array to have unique admission year also.
    let tempAdmissionsYear = this.props.admissions.filter((item) => {
      if (!partialAdmissionsYear.includes(item.admissionYear)) {
        partialAdmissionsYear.push(item.admissionYear);
        return true;
      } else {
        return false;
      }
    });
    this.setState({
      scheduleDataInfo: this.props.scheduleData,
      locations: this.props.locations,
      profNames: this.props.professors,
      subjectNames: this.props.subjects,
      admissions: tempAdmissionsYear,
      admissionsDept: this.props.admissions,
      departments: this.props.departments,
      classesSemester: this.props.classes, //semester
      classesDivision: this.props.classes, // division
    });
  };

  //when we have Class as DefautFilter we first selectAdmissionYear.
  onChangeCheck = (e) => {
    //after selecting admissionYear is selected this one gets called
    if (e.target.name === "admissionYear") {
      let admissionsDepartmentFiltered = this.state.admissionsDept;
      let tempDepartmentCodes = [];
      //corresponding to admissionYear we find out the deaprtmentCode
      admissionsDepartmentFiltered = admissionsDepartmentFiltered.filter(
        (item) => {
          if (item.admissionYear == e.target.value) {
            tempDepartmentCodes.push(item.departmentCode);
            return true;
          } else {
            return false;
          }
        }
      );
      //for the selected departmentCode we select their names from partialDepartments
      //department->Code to departmentName
      let partialDepartments = this.state.departments;
      partialDepartments = partialDepartments.filter((item) => {
        if (tempDepartmentCodes.includes(item.departmentCode)) {
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        partialDepartments: partialDepartments,
        admissionYear: e.target.value, // sets the state with selected admissionYear
        disableDepartment: false, //enable departmentFilter
      });
    }

    //after selecting departmentName is selected this one gets called
    if (e.target.name === "departmentName") {
      //we find the departmentCode corresponding to which department exists
      //departmentName to department->Code
      let departmentCode = this.state.departments.find((item) => {
        return item.departmentName == e.target.value;
      });

      //finding out the admission code corresponsing to department and admission Year.
      let admissionCode = this.state.admissionsDept.filter((item) => {
        if (
          item.departmentCode == departmentCode.departmentCode &&
          item.admissionYear == this.state.admissionYear
        ) {
          return true;
        } else {
          return false;
        }
      });

      // finding out the classes corresponding to admission code and making sure that semester is unique.
      let semesters = [];
      let classes = this.state.classesSemester.filter((item) => {
        if (
          !semesters.includes(item.semester) &&
          item.admissionCode == admissionCode[0].admissionCode
        ) {
          semesters.push(item.semester);
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        disableSemester: false,
        departmentCode: departmentCode.departmentCode,
        departmentName: e.target.value,
        admissionCode: admissionCode[0].admissionCode,
        partialSemester: classes,
      });
    }

    //after selecting Semester is selected this one gets called
    if (e.target.name === "semester") {
      let divisions = [];

      let classesDivision = this.state.classesDivision.filter((item) => {
        if (
          //the division may have same value of division corresponding to different semester
          //to show unique value for division we fill division array and have unique values in it.
          !divisions.includes(item.division) &&
          item.admissionCode === this.state.admissionCode &&
          item.semester === e.target.value
        ) {
          divisions.push(item.division);
          return true;
        } else {
          return false;
        }
      });
      // DEBUG
      //console.log("classesDivision", classesDivision);
      this.setState({
        disableDivision: false, //enable division Field
        semester: e.target.value, //sets the state with semester values
        partialDivision: classesDivision, //sets partialDivision to show the division of this semster
      });
    }
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select className="form-control" disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((person) => (
              <option key={person.item} value={person.item}>
                {person.item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  changeDate = (value, input, variableName) => {
    input.onChange(value._d);
    this.setState({ [variableName]: new Date(value._d) });
  };

  dateComponent = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta: { touched, error },
    type,
    label,
    disabled,
    variableName,
    dateFormat,
    timeFormatBool,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          defaultValue={defaultValue}
          label={label}
          disabled={disabled}
          {...input}
          dateFormat={dateFormat}
          timeFormat={timeFormatBool}
          name={name}
          value={this.state[variableName]}
          onChange={(event) => this.changeDate(event, input, variableName)}
        />
        {touched && error && <p style={{ color: "red" }}>{error}</p>}
      </div>
    );
  };

  onChange = (event, input, value, variableName) => {
    input.onChange(value);
    this.setState({ [variableName]: value });
  };

  renderTextFieldLocation = ({
    label,
    name,
    input,
    variableName,
    meta: { touched, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <React.Fragment>
      <Autocomplete
        id={name}
        name={name}
        {...input}
        className="filter"
        disabled={disabled}
        value={this.state[variableName]}
        options={values}
        style={{ width: 300 }}
        getOptionLabel={(option) => {
          return option;
        }}
        {...custom}
        onChange={(event, value) =>
          this.onChange(event, input, value, variableName)
        }
        renderInput={(params) => <TextField {...params} label={label} />}
      />
      {touched && error && <p style={{ color: "red" }}>{error}</p>}
    </React.Fragment>
  );

  timeComponentEndTime = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          inputProps={{ disabled: disabled }}
          {...input}
          dateFormat={false}
          name={name}
          value={this.state.endTime}
          onChange={(event) => this.changeEndTime(event, input)}
        />
      </div>
    );
  };

  timeComponentStartTime = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          inputProps={{ disabled: disabled }}
          {...input}
          dateFormat={false}
          name={name}
          value={this.state.startTime}
          onChange={(event) => this.changeStartTime(event, input)}
        />
      </div>
    );
  };

  // when we change the time of the class this functions is called sttartTime to change
  // the start time in state and inputStart is material-ui function toi change the time in time
  // component
  changeStartTime = (value, inputStart) => {
    inputStart.onChange(value._d);
    this.setState({ startTime: new Date(value._d) });
  };

  // when we change the time of the class this functions is called sttartTime to change
  // the start time in state and inputStart is material-ui function toi change the time in time
  // component
  changeEndTime = (value, inputEnd) => {
    inputEnd.onChange(value._d);
    this.setState({ endTime: new Date(value._d) });
  };

  // render = () => {
  //   return (
  //     <Modal
  //       open={this.props.createScheduleFlag}
  //       onClose={() => this.props.createSchedulePopUp()}
  //       aria-labelledby="simple-modal-title"
  //       aria-describedby="simple-modal-description"
  //       aria-hidden="true"
  //     >
  //       <Draggable>
  //         <div
  //           tabIndex="-1"
  //           style={{
  //             paddingLeft: 400,
  //             paddingTop: 100,
  //           }}
  //         >
  //           <div className="ui standard modal visible active">
  //             <div className="header">Schedule Creation</div>
  //             <div
  //               className="content"
  //               style={{
  //                 height: 600,
  //                 overflow: "auto",
  //                 "text-align": "justify",
  //               }}
  //             >
  //               <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
  //                 <Field
  //                   disabled={false}
  //                   label="Admission Year"
  //                   name="admissionYear"
  //                   onChange={this.onChangeCheck}
  //                   component={this.renderInput}
  //                   people={this.state.admissions.map(function (item) {
  //                     return { item: item.admissionYear };
  //                   })}
  //                 />

  //                 <Field
  //                   disabled={this.state.disableDepartment}
  //                   label="Department Name"
  //                   name="departmentName"
  //                   onChange={this.onChangeCheck}
  //                   component={this.renderInput}
  //                   people={this.state.partialDepartments.map(function (item) {
  //                     return { item: item.departmentName };
  //                   })}
  //                 />
  //                 <Field
  //                   disabled={this.state.disableSemester}
  //                   label="Semester"
  //                   name="semester"
  //                   onChange={this.onChangeCheck}
  //                   component={this.renderInput}
  //                   people={this.state.partialSemester.map(function (item) {
  //                     return { item: item.semester };
  //                   })}
  //                 />
  //                 <Field
  //                   disabled={this.state.disableDivision}
  //                   label="Division"
  //                   name="division"
  //                   onChange={this.onChangeCheck}
  //                   component={this.renderInput}
  //                   people={this.state.partialDivision.map(function (item) {
  //                     return { item: item.division };
  //                   })}
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   name="location"
  //                   values={this.state.locations}
  //                   component={this.renderTextFieldLocation}
  //                   label="Location"
  //                   type="text"
  //                   variableName="locationDefault"
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   name="subject"
  //                   values={this.state.subjectNames}
  //                   component={this.renderTextFieldLocation}
  //                   label="Subject"
  //                   type="text"
  //                   variableName="subjectDefault"
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   name="professorName"
  //                   values={
  //                     this.state.profNames
  //                       ? this.state.profNames.map((item) => item.UserName)
  //                       : []
  //                   }
  //                   component={this.renderTextFieldLocation}
  //                   label="ProfessorName"
  //                   type="text"
  //                   variableName="profDefault"
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   label="Start Date"
  //                   defaultValue={this.state.currentDate}
  //                   value={this.state.currentDate}
  //                   name="startDate"
  //                   component={this.dateComponent}
  //                   placeholder="Date"
  //                   variableName="startDate"
  //                   dateFormat="YYYY-MM-DD"
  //                   timeFormatBool={false}
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   label="End Date"
  //                   defaultValue={this.state.currentDate}
  //                   value={this.state.currentDate}
  //                   name="endDate"
  //                   component={this.dateComponent}
  //                   placeholder="End Date"
  //                   variableName="endDate"
  //                   dateFormat="YYYY-MM-DD"
  //                   timeFormatBool={false}
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   defaultValue={this.state.startTime}
  //                   value={this.state.startTime}
  //                   label="Start Time"
  //                   name="startTime"
  //                   component={this.timeComponentStartTime}
  //                   placeholder="Start Time"
  //                 />
  //                 <Field
  //                   disabled={false}
  //                   defaultValue={this.state.endTime}
  //                   value={this.state.endTime}
  //                   label="End Time"
  //                   name="endTime"
  //                   component={this.timeComponentEndTime}
  //                   placeholder="End Time"
  //                 />
  //                 <div className="form-control"></div>
  //                 <div className="form-group">
  //                   <div className="form-control"></div>
  //                 </div>
  //                 <div className="form-group"></div>
  //                 <button
  //                   className="btn btn-primary mb-2"
  //                   style={{ backgroundColor: "#4cadad" }}
  //                   type="submit"
  //                 >
  //                   Submit
  //                 </button>
  //                 {this.state.isSpinner ? (
  //                   <div className="ui active inverted dimmer">
  //                     <div className="ui loader"></div>
  //                   </div>
  //                 ) : (
  //                   <React.Fragment />
  //                 )}
  //                 {this.state.conflictSchedule ? (
  //                   <div>
  //                     <p style={{ color: "red" }}>{this.state.error}</p>
  //                   </div>
  //                 ) : (
  //                   <div></div>
  //                 )}
  //               </form>
  //             </div>
  //           </div>
  //         </div>
  //       </Draggable>
  //     </Modal>
  //   );
  // };

  render = () => {
    return (
      <Modal
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
        open={this.props.createScheduleFlag}
        onClose={() => this.props.createSchedulePopUp()}
      >
        <Modal.Header>Schedule Creation</Modal.Header>
        <Modal.Content>
          <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
            <Field
              disabled={false}
              label="Admission Year"
              name="admissionYear"
              onChange={this.onChangeCheck}
              component={this.renderInput}
              people={this.state.admissions.map(function (item) {
                return { item: item.admissionYear };
              })}
            />

            <Field
              disabled={this.state.disableDepartment}
              label="Department Name"
              name="departmentName"
              onChange={this.onChangeCheck}
              component={this.renderInput}
              people={this.state.partialDepartments.map(function (item) {
                return { item: item.departmentName };
              })}
            />
            <Field
              disabled={this.state.disableSemester}
              label="Semester"
              name="semester"
              onChange={this.onChangeCheck}
              component={this.renderInput}
              people={this.state.partialSemester.map(function (item) {
                return { item: item.semester };
              })}
            />
            <Field
              disabled={this.state.disableDivision}
              label="Division"
              name="division"
              onChange={this.onChangeCheck}
              component={this.renderInput}
              people={this.state.partialDivision.map(function (item) {
                return { item: item.division };
              })}
            />
            <Field
              disabled={false}
              name="location"
              values={
                this.state.locations
                  ? this.state.locations.map((location) => {
                      return location.ClassRoomName;
                    })
                  : []
              }
              component={this.renderTextFieldLocation}
              label="Location"
              type="text"
              variableName="locationDefault"
            />
            <Field
              disabled={false}
              name="subject"
              values={
                this.state.subjectNames
                  ? this.state.subjectNames.map((subject) => {
                      return subject.SubjName;
                    })
                  : []
              }
              component={this.renderTextFieldLocation}
              label="Subject"
              type="text"
              variableName="subjectDefault"
            />
            <Field
              disabled={false}
              name="professorName"
              values={
                this.state.profNames
                  ? this.state.profNames.map((item) => item.UserName)
                  : []
              }
              component={this.renderTextFieldLocation}
              label="ProfessorName"
              type="text"
              variableName="profDefault"
            />
            <Field
              disabled={false}
              label="Start Date"
              defaultValue={this.state.currentDate}
              value={this.state.currentDate}
              name="startDate"
              component={this.dateComponent}
              placeholder="Date"
              variableName="startDate"
              dateFormat="YYYY-MM-DD"
              timeFormatBool={false}
            />
            <Field
              disabled={false}
              label="End Date"
              defaultValue={this.state.currentDate}
              value={this.state.currentDate}
              name="endDate"
              component={this.dateComponent}
              placeholder="End Date"
              variableName="endDate"
              dateFormat="YYYY-MM-DD"
              timeFormatBool={false}
            />
            <Field
              disabled={false}
              defaultValue={this.state.startTime}
              value={this.state.startTime}
              label="Start Time"
              name="startTime"
              component={this.timeComponentStartTime}
              placeholder="Start Time"
            />
            <Field
              disabled={false}
              defaultValue={this.state.endTime}
              value={this.state.endTime}
              label="End Time"
              name="endTime"
              component={this.timeComponentEndTime}
              placeholder="End Time"
            />
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              type="submit"
            >
              Submit
            </button>
            {this.state.isSpinner ? (
              <div className="ui active inverted dimmer">
                <div className="ui loader"></div>
              </div>
            ) : (
              <React.Fragment />
            )}
          </form>
        </Modal.Content>
        <Modal.Actions>
          {this.state.conflictSchedule ? (
            <div>
              <p style={{ color: "red" }}>{this.state.error}</p>
            </div>
          ) : (
            <div></div>
          )}
        </Modal.Actions>
      </Modal>
    );
  };
}

const formWrapped = reduxForm({
  form: "Create Schedule",
  validate,
})(CreateSchedule);

const mapStateToProps = (state) => {
  return {
    scheduleData: state.scheduleDataInformation,
    userData: state.userData,
    classes: state.classes,
    admissions: state.admissions,
    departments: state.departments,
    UniCode: state.userData.UniCode,
    professors: state.professors,
    locations: state.locations,
    subjects: state.subjects,
  };
};

export default connect(mapStateToProps, { createSchedule, reCallScheduleApi })(
  formWrapped
);
