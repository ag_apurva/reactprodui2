import React from "react";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";

class PopUp extends React.Component {
  render = () => {
    return (
      <Modal
        open={!this.props.popUp}
        onClose={() => this.props.removePopUp()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        aria-hidden="true"
      >
        <Draggable>
          <div
            tabIndex="-1"
            style={{
              paddingLeft: 200,
              paddingTop: 200,
              width: 250,
            }}
          >
            <div className="ui standard modal visible active">
              <div className="ui secondary pointing menu">POINTING</div>
            </div>
          </div>
        </Draggable>
      </Modal>
    );
  };
}

export default PopUp;
