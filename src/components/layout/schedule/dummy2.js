/*
this component is working but is very poorly designed and needs refractoring.
*/

import React, { useState } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { useSelector, useDispatch } from "react-redux";
import DateTime from "react-datetime";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "react-datetime/css/react-datetime.css";
import { SubmissionError } from "redux-form";
//import PopUp from "./PopUp";
import moment from "moment";
import {
  checkConflict,
  deleteSchedule,
} from "../../../actions/scheduleActions";

const Task = (props) => {
  const scheduleData = useState((state) => state.scheduleData);
  console.log("scheduleData", scheduleData);

  const {
    Location,
    ProfName,
    SubjName,
    Date,
    newStartTime,
    newEndTime,
  } = props.taskDetails;
  const initialValues = {
    location: Location,
    professor: ProfName,
    subject: SubjName,
    date: Date,
    startTime: newStartTime,
    endTime: newEndTime,
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validate={validate}
      >
        <Form>
          <div className="time">
            <label>Location</label>
            <Field type="text" name="Location" />
            <ErrorMessage name="Location" />
          </div>
          <div className="time">
            <label>Professor</label>
            <Field type="text" name="professor" />
            <ErrorMessage name="professor" />
          </div>
          <div className="time">
            <label>Subject</label>
            <Field type="text" name="subject" />
            <ErrorMessage name="subject" />
          </div>
          {/* <div className="time">
            <label>Date</label>
            <Field type="text" name="date" />
            <ErrorMessage name="date" />
          </div>
          <div className="time">
            <label>StartTime</label>
            <Field type="text" name="startTime" />
            <ErrorMessage name="startTime" />
          </div>
          <div className="time">
            <label>EndTime</label>
            <Field type="text" name="endTime" />
            <ErrorMessage name="endTime" />
          </div> */}
          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </div>
  );
};

const mapStateToPropsTask = (state) => {
  return {
    scheduleData: state.scheduleDataInformation || {},
    UniCode: state.userData.UniCode,
  };
};
export const TaskMain = connect(mapStateToPropsTask, {
  checkConflict,
  deleteSchedule,
})(Task);

/*


      let ClassId = event.queryStringParameters.ClassId;
      let UniCode = event.queryStringParameters.UniCode;
      let DateNew = event.queryStringParameters.DateNew;
      let ClassSchMonId = event.queryStringParameters.ClassSchMonId;

*/
