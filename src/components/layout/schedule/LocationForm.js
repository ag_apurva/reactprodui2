import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Field, reduxForm } from "redux-form";
import AlertComponent from "../../auth/AlertComponent";
import "react-datetime/css/react-datetime.css";
import axios from "axios";
const validateLocation = (formValues) => {
  let error = {};
  if (!formValues.LocationName) {
    error.LocationName = "* Please select a Class Room";
  }
  return error;
};

class LocationSubmit extends React.Component {
  state = {
    scheduleData: {},
    isSpinner: false,
  };

  componentDidMount = () => {
    this.setState({
      scheduleData: this.props.scheduleData,
    });
  };
  processScheduleData = (unfilteredScheduleData) => {
    var dataMapped = {};
    unfilteredScheduleData.forEach((item) => {
      var itemDate = moment(item.Date).format("YYYY-MM-DD");
      if (!dataMapped.hasOwnProperty(itemDate)) {
        dataMapped[itemDate] = [];
      }
      item["StartTime"] = moment(item.newStartTime);
      item["EndTime"] = moment(item.newEndTime);
      dataMapped[itemDate].push(item);
    });
    // all the keys in dataMapped are the dates in yyyy-mm-dd format.

    var dataMappedMain = {};
    Object.keys(dataMapped).forEach((item) => {
      dataMappedMain[item] = {};
      for (let i = 6; i < 22; i++) {
        dataMappedMain[item][i] = [];
      }
      dataMapped[item].forEach((data) => {
        let startTime = moment(data.StartTime);
        let endTime = moment(data.EndTime);
        let minutes = moment.duration(endTime.diff(startTime)).asMinutes();
        dataMappedMain[item][startTime.hour()].push({
          ...data,
          top: startTime.minutes(),
          height: minutes,
        });
      });
    });

    Object.keys(dataMappedMain).forEach((item) => {
      Object.keys(dataMappedMain[item]).forEach((data) => {
        if (dataMappedMain[item][data].length === 0) {
          dataMappedMain[item][data].push({ height: 0, top: 0 });
        }
      });
    });
    return dataMappedMain;
  };
  componentDidUpdate = (previousProps) => {
    if (previousProps.scheduleData !== this.props.scheduleData) {
      this.setState({
        scheduleData: this.props.scheduleData,
      });
    }
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="filter">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((item) => (
              <option key={item} value={item}>
                {item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  onSubmit = async (formValues) => {
    this.setState({
      isSpinner: true,
    });

    var config = {
      method: "get",
      params: {
        email: formValues.LocationName,
        universityCode: this.props.UniCode,
      },
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/getschedule",
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      let data = await axios(config);
      //console.log("data", data);
      const processedData = this.processScheduleData(data.data);
      //console.log("processedData", processedData);
      this.props.renderScheduleData(processedData);
    } catch (e) {
      console.log("e", e);
    }
    this.setState({
      isSpinner: false,
    });
  };

  render = () => {
    return (
      <form
        className="filter-wrapper"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field
          disabled={false}
          label="Location Name"
          name="LocationName"
          onChange={this.onChangeCheck}
          component={this.renderInput}
          people={
            this.state.scheduleData.Locations
              ? this.state.scheduleData.Locations
              : []
          }
        />
        <div className="filter fil-btn">
          <label></label>
          <button
            type="submit"
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
          >
            Submit
          </button>
          <AlertComponent />
        </div>
        {this.state.isSpinner ? (
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        ) : (
          <React.Fragment />
        )}
      </form>
    );
  };
}

const formSubmitTask = reduxForm({
  form: "Display Location",
  validate: validateLocation,
})(LocationSubmit);

const mapStateToPropsLocation = (state) => {
  return {
    scheduleData: state.scheduleDataInformation || {},
    UniCode: state.userData.UniCode,
  };
};

export const LocationSchedule = connect(
  mapStateToPropsLocation,
  {}
)(formSubmitTask);

/*
state.scheduleDataInformation gets loaded when the user logs in with values //authAction loadDepartments data
Professor Names
Locations
Subject

We pass the locations name to render function basically locations Name is 
returned from db as Location Names

we select the values of location  from drop down.

*/
