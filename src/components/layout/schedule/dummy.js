/*
this component is working but is very poorly designed and needs refractoring.
*/

import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import DateTime from "react-datetime";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "react-datetime/css/react-datetime.css";
import { SubmissionError } from "redux-form";
//import PopUp from "./PopUp";
import moment from "moment";
import {
  checkConflict,
  deleteSchedule,
} from "../../../actions/scheduleActions";

const validateTask = (formValues) => {
  let error = {};
  if (
    formValues.sttartTime &&
    formValues.endTime &&
    moment(formValues.sttartTime) > moment(formValues.endTime)
  ) {
    error.endTime = "Please select endTime greater than startTime";
  }
  return error;
};

class Task extends React.Component {
  onSubmit = async (formValues) => {
    let professorEmail, professorEmailOld;
    if (formValues.professorName !== null) {
      let data = this.props.scheduleData.ProfNames.filter((item) => {
        return (
          item.split(",")[0].toLowerCase() ===
          formValues.professorName.toLowerCase()
        );
      });
      if (data.length > 0) {
        professorEmail = data[0].split(",")[1];
      }
    }

    {
      let data = this.props.scheduleData.ProfNames.filter((item) => {
        return (
          item.split(",")[0].toLowerCase() ==
          this.props.taskDetails.ProfName.toLowerCase()
        );
      });
      console.log("data", data);
      if (data.length > 0) {
        professorEmailOld = data[0].split(",")[1];
      }
    }
    let dateOld = moment(this.props.taskDetails.Date).format("YYYY-MM-DD"),
      dateNew;
    let startTimeToCheck, endTimeToCheck;

    if (formValues.hasOwnProperty("sttartTime")) {
      startTimeToCheck = moment(formValues["sttartTime"]);
    } else {
      startTimeToCheck = moment(this.props.taskDetails.newStartTime);
    }
    if (formValues.hasOwnProperty("endTime")) {
      endTimeToCheck = moment(formValues["endTime"]);
    } else {
      endTimeToCheck = moment(this.props.taskDetails.newEndTime);
    }

    if (moment(startTimeToCheck) > moment(endTimeToCheck)) {
      throw new SubmissionError({
        endTime: "Please select endTime greater than startTime",
      });
    }
    let startTimeOld = moment(this.props.taskDetails.newStartTime).format(
        "HH:mm"
      ),
      startTimeNew;
    let startTimeBoundaryCheck = moment(this.props.taskDetails.newStartTime)
      .add(1, "minute")
      .format("HH:mm");
    let endTimeOld = moment(this.props.taskDetails.newEndTime).format("HH:mm"),
      endTimeNew;
    let endTimeBoundartCheck = moment(this.props.taskDetails.newEndTime)
      .subtract(1, "minute")
      .format("HH:mm");

    let subjectOld = this.props.taskDetails.SubjName,
      subjectNew;
    let ProfNameOld = this.props.taskDetails.ProfName,
      ProfNameNew;
    let LocationOld = this.props.taskDetails.Location,
      LocationNew;
    if (formValues.location === null) {
      LocationNew = LocationOld;
    } else {
      LocationNew = formValues.location;
    }

    if (formValues.SubjectName === null) {
      subjectNew = subjectOld;
    } else {
      subjectNew = formValues.SubjectName;
    }

    if (formValues.professorName === null) {
      ProfNameNew = ProfNameOld;
    } else {
      ProfNameNew = formValues.professorName;
    }

    if (!formValues.hasOwnProperty("date")) {
      dateNew = dateOld;
    } else {
      dateNew = moment(formValues.date).format("YYYY-MM-DD");
    }

    if (!formValues.hasOwnProperty("sttartTime")) {
      startTimeNew = startTimeOld;
    } else {
      startTimeNew = moment(formValues.sttartTime).format("HH:mm");
      startTimeBoundaryCheck = moment(formValues.sttartTime)
        .add(1, "minute")
        .format("HH:mm");
    }

    if (!formValues.hasOwnProperty("endTime")) {
      endTimeNew = endTimeOld;
    } else {
      endTimeNew = moment(formValues.endTime).format("HH:mm");
      endTimeBoundartCheck = moment(formValues.endTime)
        .subtract(1, "minute")
        .format("HH:mm");
    }
    console.log("professorEmailNew", professorEmail);
    console.log("professorEmailOld", professorEmailOld);
    let anotherForm = true;

    this.setState({
      formData: {
        dateOld,
        dateNew,
        startTimeOld,
        startTimeNew,
        endTimeOld,
        endTimeNew,
        LocationOld,
        LocationNew,
        subjectOld,
        subjectNew,
        ProfNameOld,
        ProfNameNew,
        ClassId: this.props.taskDetails.ClassId,
        ClassSchMonId: this.props.taskDetails.ClassSchMonId,
        professorEmail,
        professorEmailOld,
        UniCode: this.props.UniCode,
        startTimeBoundaryCheck,
        endTimeBoundartCheck,
      },
    });

    if (subjectOld.toLowerCase() !== subjectNew.toLowerCase()) {
      if (ProfNameOld.toLowerCase() === ProfNameNew.toLowerCase()) {
        // console.log("ASK FOR Professsor");
        this.setState({
          popUp: true,
          askForProfessor: true,
        });
        anotherForm = false;
      }
    }
    if (anotherForm) {
      try {
        const data = await this.props.checkConflict({
          dateOld,
          dateNew,
          startTimeOld,
          startTimeNew,
          endTimeOld,
          endTimeNew,
          LocationOld,
          LocationNew,
          subjectOld,
          subjectNew,
          ProfNameOld,
          ProfNameNew,
          ClassId: this.props.taskDetails.ClassId,
          ClassSchMonId: this.props.taskDetails.ClassSchMonId,
          professorEmail,
          professorEmailOld,
          UniCode: this.props.UniCode,
          startTimeBoundaryCheck,
          endTimeBoundartCheck,
        });

        if (data.data.hasOwnProperty("error")) {
          //  console.log("data", data.data.error);
          this.setState({
            popUp: true,
            askForProfessor: false,
            conflictSchedule: true,
            error: data.data.error,
          });
        } else {
          // console.log("data", data.data);
          let processedData = this.props.processScheduleData(
            JSON.parse(data.data.data)
          );
          //console.log("processedData", processedData);
          if (dateNew !== dateOld) {
            this.props.updateScheduleDataDouble(
              dateOld,
              dateNew,
              processedData[dateOld],
              processedData[dateNew]
            );
          } else {
            this.props.updateScheduleDataSingleDate(
              dateNew,
              processedData[dateNew]
            );
          }

          this.setState({
            popUp: false,
            askForProfessor: false,
            conflictSchedule: false,
          });
        }
      } catch (e) {
        console.log("error", e);
      }
    }
  };

  state = {
    formData: {},
    conflictSchedule: false,
    askForProfessor: false,
    popUp: false,
    openPopUp: false,
    profNames: [],
    locations: [],
    subjectNames: [],
    disabled: true,
    scheduleDataInfo: [],
    locationDefault: "",
    profDefault: "",
    subjectDefault: "",
    sttartTime: "",
    endTime: "",
  };

  componentDidMount = () => {
    this.setState({
      scheduleDataInfo: this.props.scheduleData,
      locations: this.props.scheduleData.Locations,
      profNames: this.props.scheduleData.ProfNames,
      subjectNames: this.props.scheduleData.Subject,
      locationDefault: this.props.taskDetails.Location,
      profDefault: this.props.taskDetails.ProfName,
      subjectDefault: this.props.taskDetails.SubjName,
      currentDate: new Date(this.props.taskDetails.Date),
      sttartTime: new Date(this.props.taskDetails.newStartTime),
      endTime: new Date(this.props.taskDetails.newEndTime),
    });
  };

  // when location is changed we set the new locations values here
  onCHanngeLocation = (event, value) => {
    this.setState({ locationDefault: value });
  };

  // when Professor is changed we set the new Professors values here
  onCHanngeProf = (event, value) => {
    this.setState({ profDefault: value });
  };

  // when subject is changed we set the new subjects values here
  onCHanngeSubject = (event, value) => {
    this.setState({ subjectDefault: value });
  };

  renderTextFieldProfessor = ({
    label,
    name,
    input,
    meta: { touched, invalid, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <Autocomplete
      id={name}
      name={name}
      {...input}
      className="filter"
      disabled={disabled}
      onChange={this.onCHanngeProf}
      value={this.state.profDefault}
      options={values.map((item) => item.split(",")[0])}
      getOptionSelected={(option, value) => {
        //console.log("option value", option, value);
        if (option.split(",")[0] === value) {
          //console.log("TRUE TRUE");
        }
        return option.split(",")[0].toLowerCase() === value.toLowerCase();
      }}
      getOptionLabel={(option) => {
        //console.log("option", option);
        return option;
      }}
      {...custom}
      onInputChange={input.onChange}
      style={{ width: 150 }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );

  renderTextFieldLocation = ({
    label,
    name,
    input,
    meta: { touched, invalid, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <Autocomplete
      id={name}
      name={name}
      {...input}
      className="filter"
      disabled={disabled}
      onChange={this.onCHanngeLocation}
      value={this.state.locationDefault}
      options={values}
      getOptionSelected={(option, value) => {
        return option === value;
      }}
      getOptionLabel={(option) => {
        return option;
      }}
      {...custom}
      onInputChange={input.onChange}
      style={{ width: 150 }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );

  renderTextFieldSubject = ({
    label,
    name,
    input,
    meta: { touched, invalid, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <Autocomplete
      id={name}
      name={name}
      {...input}
      className="filter"
      disabled={disabled}
      onChange={this.onCHanngeSubject}
      value={this.state.subjectDefault}
      options={values}
      getOptionSelected={(option, value) => {
        return option.toLowerCase() === value.toLowerCase();
      }}
      getOptionLabel={(option) => {
        return option;
      }}
      {...custom}
      onInputChange={input.onChange}
      style={{ width: 150 }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );

  onClick = () => {
    this.setState({ disabled: false });
  };

  renderError({ error, touched }) {
    if (error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  // when we change the time of the class this functions is called sttartTime to change
  // the start time in state and inputStart is material-ui function toi change the time in time
  // component
  changeStartTime = (value, inputStart) => {
    inputStart.onChange(value._d);
    this.setState({ sttartTime: new Date(value._d) });
  };

  // when we change the time of the class this functions is called sttartTime to change
  // the start time in state and inputStart is material-ui function toi change the time in time
  // component
  changeEndTime = (value, inputEnd) => {
    inputEnd.onChange(value._d);
    this.setState({ endTime: new Date(value._d) });
  };

  //same as about acts in case of dates.
  changeDate = (value, inputEnd) => {
    inputEnd.onChange(value._d);
    this.setState({ currentDate: new Date(value._d) });
  };

  dateComponent = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          inputProps={{ disabled: disabled }}
          disabled={disabled}
          {...input}
          dateFormat="YYYY-MM-DD"
          timeFormat={false}
          name={name}
          value={this.state.currentDate}
          onChange={(event) => this.changeDate(event, input)}
        />
      </div>
    );
  };

  timeComponentEndTime = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          inputProps={{ disabled: disabled }}
          {...input}
          dateFormat={false}
          name={name}
          value={this.state.endTime}
          onChange={(event) => this.changeEndTime(event, input)}
        />
        {this.renderError(meta)}
      </div>
    );
  };

  timeComponentStartTime = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          inputProps={{ disabled: disabled }}
          {...input}
          dateFormat={false}
          name={name}
          value={this.state.sttartTime}
          onChange={(event) => this.changeStartTime(event, input)}
        />
      </div>
    );
  };

  commonSubmit = async () => {
    console.log("No the do not want to chnage the professor and commonSubmit");

    try {
      console.log("this.state.formData", this.state.formData);
      const data = await this.props.checkConflict(this.state.formData);
      let { dateNew, dateOld } = this.state.formData;
      if (data.data.hasOwnProperty("error")) {
        console.log("data", data.data.error);
        this.setState({
          popUp: true,
          askForProfessor: false,
          conflictSchedule: true,
          error: data.data.error,
        });
      } else {
        console.log("data", data.data);
        let processedData = this.props.processScheduleData(
          JSON.parse(data.data.data)
        );
        console.log("processedData", processedData);
        if (dateNew !== dateOld) {
          this.props.updateScheduleDataDouble(
            dateOld,
            dateNew,
            processedData[dateOld],
            processedData[dateNew]
          );
        } else {
          this.props.updateScheduleDataSingleDate(
            dateNew,
            processedData[dateNew]
          );
        }

        this.setState({
          popUp: false,
          askForProfessor: false,
          conflictSchedule: false,
        });
      }
    } catch (e) {
      console.log("error", e);
    }

    // this.setState({ popUp: true, askForProfessor: false });
  };

  onSubmitYes = (formvalues) => {
    this.setState({ popUp: false });
  };

  removePopUp = () => {
    this.setState({ popUp: true });
  };

  onDeleteSchedule = async () => {
    console.log("this.props.taskDetails", this.props.taskDetails.ClassSchMonId);
    let ClassId = this.props.taskDetails.ClassId;
    let UniCode = this.props.UniCode;
    let DateNew = moment(this.props.taskDetails.Date).format("YYYY-MM-DD");
    let ClassSchMonId = this.props.taskDetails.ClassSchMonId;
    try {
      let data = await this.props.deleteSchedule(
        ClassId,
        UniCode,
        DateNew,
        ClassSchMonId
      );
      console.log("data deletedSchedule", data.data.data);
      // JSON.parse(data.data.data).forEach((item) => console.log("item", item));
      let processedData = this.props.processScheduleData(
        JSON.parse(data.data.data)
      );
      this.props.updateScheduleDataSingleDate(DateNew, processedData[DateNew]);
    } catch (e) {
      console.log("error in delete Schedule", e);
    }
    console.log(
      "ClassId,UniCode,DateNew,ClassSchMonId",
      ClassId,
      UniCode,
      DateNew,
      ClassSchMonId
    );
  };

  render = () => {
    if (!this.props.taskHidden) {
      return (
        <div
          style={{ display: this.props.taskHidden ? "none" : "" }}
          className="col-lg-3 task-wrap bg-white"
        >
          <div className="task-heading pt-2">
            <h4>Task Details</h4>
            <span>
              <button onClick={this.onClick}>Edit/Delete</button>
            </span>
          </div>
          <div>
            <Modal
              open={this.state.popUp}
              onClose={() => this.setState({ popUp: false })}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              aria-hidden="true"
            >
              <Draggable>
                <div
                  tabIndex="-1"
                  style={{
                    paddingLeft: 200,
                    paddingTop: 200,
                    width: 250,
                  }}
                >
                  <div className="ui standard modal visible active">
                    <div className="ui secondary pointing menu">
                      {this.state.askForProfessor ? (
                        <div>
                          <div className="header">
                            Subject is Changed do you want to change Professor?
                          </div>
                          <div className="content">
                            <span>
                              <button onClick={this.onSubmitYes}>Yes</button>
                            </span>
                            <span></span>
                            <span>
                              <button onClick={this.commonSubmit}>No</button>
                            </span>
                          </div>
                        </div>
                      ) : (
                        <div></div>
                      )}
                      {this.state.conflictSchedule ? (
                        <div>
                          <p style={{ color: "red" }}>{this.state.error}</p>
                        </div>
                      ) : (
                        <div></div>
                      )}
                    </div>
                  </div>
                </div>
              </Draggable>
            </Modal>
          </div>
          <div className="row task-card">
            <div className="task-inner">
              <div className="task-head" style={{ background: "#ffecc9" }}>
                <small>Class Details</small>
              </div>
            </div>

            <div className="col-12 ">
              <div className="time">
                <div>
                  <p>
                    <b>Admission Year </b> {this.props.admissionYear}
                  </p>
                </div>
                <div>
                  <p>
                    <b>Semester </b> {this.props.semester}
                  </p>
                </div>
              </div>
              <div className="time">
                <div>
                  <p>
                    <b>Department </b>
                    {this.props.departmentName ? this.props.departmentName : ""}
                  </p>
                </div>
                <div>
                  <p>
                    <b>Division </b> {this.props.division}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row task-card">
            <div className="task-inner">
              <div className="task-head" style={{ background: "#ffecc9" }}>
                <small>Timing Details</small>
              </div>
            </div>

            <div className="col-12 ">
              <div className="time">
                <button
                  onClick={() => this.onDeleteSchedule()}
                  disabled={this.state.disabled}
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  Delete Me
                </button>
              </div>

              <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                <div className="time">
                  <div>
                    <Field
                      defaultValue={
                        this.props.taskDetails.Location
                          ? this.props.taskDetails.Location
                          : ""
                      }
                      disabled={this.state.disabled}
                      name="location"
                      values={this.state.locations}
                      component={this.renderTextFieldLocation}
                      label="Location"
                      type="text"
                    />
                  </div>
                </div>

                <div className="time">
                  <div>
                    <Field
                      defaultValue={
                        this.props.taskDetails.SubjName
                          ? this.props.taskDetails.SubjName
                          : ""
                      }
                      disabled={this.state.disabled}
                      name="SubjectName"
                      values={this.state.subjectNames}
                      component={this.renderTextFieldSubject}
                      label="Subject Name"
                      type="text"
                    />
                  </div>
                </div>

                <div className="time">
                  <div>
                    <Field
                      defaultValue={
                        this.props.taskDetails.ProfName
                          ? this.props.taskDetails.ProfName
                          : ""
                      }
                      disabled={this.state.disabled}
                      name="professorName"
                      values={this.state.profNames}
                      component={this.renderTextFieldProfessor}
                      label="Professor"
                      type="text"
                    />
                  </div>
                </div>
                <div className="time">
                  <div>{}</div>
                </div>

                <div className="time">
                  <div>
                    <Field
                      disabled={this.state.disabled}
                      label="Date"
                      defaultValue={this.state.currentDate}
                      value={this.state.currentDate}
                      name="date"
                      component={this.dateComponent}
                      placeholder="Date"
                    />
                  </div>
                </div>

                <div className="time">
                  <div>
                    <Field
                      disabled={this.state.disabled}
                      defaultValue={this.state.sttartTime}
                      value={this.state.sttartTime}
                      label="Start Time"
                      name="sttartTime"
                      component={this.timeComponentStartTime}
                      placeholder="Start Time"
                    />
                  </div>
                </div>

                <div className="time">
                  <div>
                    <Field
                      disabled={this.state.disabled}
                      defaultValue={this.state.endTime}
                      value={this.state.endTime}
                      label="End Time"
                      name="endTime"
                      component={this.timeComponentEndTime}
                      placeholder="End Time"
                    />
                  </div>
                </div>

                <div className="time">
                  <div>
                    <input
                      disabled={this.state.disabled}
                      type="submit"
                      className="btn btn-primary mb-2"
                      style={{ backgroundColor: "#4cadad" }}
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      );
    } else {
      return <React.Component />;
    }
  };
}

const formWrappedTask = reduxForm({
  form: "Edit Task",
  validate: validateTask,
})(Task);

const mapStateToPropsTask = (state) => {
  return {
    scheduleData: state.scheduleDataInformation || {},
    UniCode: state.userData.UniCode,
  };
};
export const TaskMain = connect(mapStateToPropsTask, {
  checkConflict,
  deleteSchedule,
})(formWrappedTask);

/*


      let ClassId = event.queryStringParameters.ClassId;
      let UniCode = event.queryStringParameters.UniCode;
      let DateNew = event.queryStringParameters.DateNew;
      let ClassSchMonId = event.queryStringParameters.ClassSchMonId;

*/
