import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Field, reduxForm } from "redux-form";
import AlertComponent from "../../auth/AlertComponent";
import "react-datetime/css/react-datetime.css";
import axios from "axios";

const validate = (formValues) => {
  let error = {};
  if (!formValues.ProfEmail) {
    // here we have little glitch since scheduleData returns profName-profEmails the value in options
    // is prof Email not the prof name so formValues.ProfEmail so its is actually profEmail not the name
    error.ProfEmail = "* Please select a Professor Name";
  }
  return error;
};

class ProfessorSubmit extends React.Component {
  state = {
    scheduleData: {},
    isSpinner: false,
  };

  componentDidMount = () => {
    //console.log("this.state.scheduleData", this.state.scheduleData);
    this.setState({
      scheduleData: this.props.scheduleData,
    });
  };
  processScheduleData = (unfilteredScheduleData) => {
    var dataMapped = {};
    unfilteredScheduleData.forEach((item) => {
      var itemDate = moment(item.Date).format("YYYY-MM-DD");
      if (!dataMapped.hasOwnProperty(itemDate)) {
        dataMapped[itemDate] = [];
      }
      item["StartTime"] = moment(item.newStartTime);
      item["EndTime"] = moment(item.newEndTime);
      dataMapped[itemDate].push(item);
    });
    // all the keys in dataMapped are the dates in yyyy-mm-dd format.

    var dataMappedMain = {};
    Object.keys(dataMapped).forEach((item) => {
      dataMappedMain[item] = {};
      for (let i = 4; i < 24; i++) {
        dataMappedMain[item][i] = [];
      }
      dataMapped[item].forEach((data) => {
        let startTime = moment(data.StartTime);
        let endTime = moment(data.EndTime);
        let minutes = moment.duration(endTime.diff(startTime)).asMinutes();
        dataMappedMain[item][startTime.hour()].push({
          ...data,
          top: startTime.minutes(),
          height: minutes,
        });
      });
    });

    Object.keys(dataMappedMain).forEach((item) => {
      Object.keys(dataMappedMain[item]).forEach((data) => {
        if (dataMappedMain[item][data].length === 0) {
          dataMappedMain[item][data].push({ height: 0, top: 0 });
        }
      });
    });
    return dataMappedMain;
  };

  componentDidUpdate = (previousProps) => {
    if (previousProps.scheduleData !== this.props.scheduleData) {
      this.setState({
        scheduleData: this.props.scheduleData,
      });
    }
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="filter">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((item) => (
              <option key={item.split(",")[0]} value={item.split(",")[1]}>
                {item.split(",")[0]}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  onSubmit = async (formValues) => {
    this.setState({
      isSpinner: true,
    });

    var config = {
      method: "get",
      params: {
        email: formValues.ProfEmail,
        universityCode: this.props.UniCode,
      },
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/getschedule",
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      let data = await axios(config);
      //console.log("data", data);
      const processedData = this.processScheduleData(data.data);
      //console.log("processedData", processedData);
      this.props.renderScheduleData(processedData);
    } catch (e) {
      console.log("e", e);
    }
    this.setState({
      isSpinner: false,
    });
  };

  render = () => {
    return (
      <form
        className="filter-wrapper"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field
          disabled={false}
          label="Professor Name"
          name="ProfEmail"
          onChange={this.onChangeCheck}
          component={this.renderInput}
          people={
            this.state.scheduleData.ProfNames
              ? this.state.scheduleData.ProfNames
              : []
          }
        />
        <div className="filter fil-btn">
          <label></label>
          <button
            type="submit"
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
          >
            Submit
          </button>
          <AlertComponent />
        </div>
        {this.state.isSpinner ? (
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        ) : (
          <React.Fragment />
        )}
      </form>
    );
  };
}

const formSubmitTask = reduxForm({
  form: "Display Professor",
  validate,
})(ProfessorSubmit);

const mapStateToPropsProfessor = (state) => {
  return {
    scheduleData: state.scheduleDataInformation || {},
    UniCode: state.userData.UniCode,
  };
};

export const ProfessorSubmitTask = connect(
  mapStateToPropsProfessor,
  {}
)(formSubmitTask);

/*
state.scheduleDataInformation gets loaded when the user logs in with values //authAction loadDepartments data
Professor Names
Locations
Subject

            {people.map((item) => (
              <option key={item.split(",")[0]} value={item.split(",")[1]}>
                {item.split(",")[0]}
              </option>
            ))}

We pass the professor nane to render function basically professor Name is 
returned from db as "ProfessorName-ProfessorEmail"  Adiyta-"adiytab@inncourve.com"

we select the values of profName split(,)[0] from drop down 

*/
