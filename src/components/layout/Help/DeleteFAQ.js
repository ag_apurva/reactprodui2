import React from "react";
import { connect } from "react-redux";
import { deleteFaq } from "../../../actions/scheduleActions";

class DeleteFAQ extends React.Component {
  state = {
    error: "",
  };
  onSubmit = async () => {
    try {
      await this.props.deleteFaq(
        this.props.id,
        this.props.helpfaqs,
        this.props.UniCode
      );
      this.props.removePopUpEditDelete();
    } catch (e) {
      this.setState({
        error: e,
      });
      setTimeout(() => {
        this.setState({ error: "" });
        this.props.removePopUpEditDelete();
      }, 10000);
    }
  };
  render = () => {
    return (
      <div>
        <div className="content">
          <p>Are you sure you want to delete FAQ {this.props.id}? </p>

          <button
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
            onClick={() => this.onSubmit()}
          >
            Delete
          </button>
          {this.state.error ? (
            <div>
              <p style={{ color: "red" }}>{this.state.error}</p>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    helpfaqs: state.faqData,
    UniCode: state.userData.UniCode,
  };
};

const DeleteFAQMain = connect(mapStateToProps, { deleteFaq })(DeleteFAQ);
export default DeleteFAQMain;
