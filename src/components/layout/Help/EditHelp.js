import React from "react";
import { connect } from "react-redux";

import { Field, reduxForm } from "redux-form";
import { updateFaq, loadFAQData } from "../../../actions/scheduleActions";
const validate = (formValues) => {
  const error = {};
  if (!formValues.question) {
    error.question = "Please write an Question";
  }
  if (!formValues.answer) {
    error.answer = "Please write an Answer";
  }

  return error;
};

class EditFAQ extends React.Component {
  state = {
    error: "",
  };
  componentDidMount = () => {
    this.props.loadFAQData(this.props.item);
  };
  onSubmit = async (formValues) => {
    // console.log(
    //   "formValues",
    //   formValues,
    //   this.props.id,
    //   this.props.UniCode,
    //   this.props.helpfaqs
    // );
    try {
      await this.props.updateFaq(
        formValues,
        this.props.id,
        this.props.UniCode,
        this.props.helpfaqs
      );
      this.props.removePopUpEditDelete();
    } catch (e) {
      this.setState({
        error: e,
      });
      setTimeout(() => {
        this.setState({ error: "" });
        this.props.removePopUpEditDelete();
      }, 10000);
    }
  };
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  render = () => {
    return (
      <div>
        <div className="content">
          <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
            <Field
              label="Category"
              type="text"
              name="category"
              component={this.renderEmailInput}
              disabled={true}
            />
            <Field
              type="text"
              label="Question"
              name="question"
              component={this.renderEmailInput}
              disabled={false}
            />
            <Field
              type="text"
              label="Answer"
              name="answer"
              component={this.renderEmailInput}
              disabled={false}
            />
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              type="submit"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  };
}

const EditFAQFormWrapped = reduxForm({
  form: "Edit FAQ",
  validate,
  enableReinitialize: true,
})(EditFAQ);

const mapStateToProps = (state) => {
  if (state.editFaq) {
    return {
      helpfaqs: state.editFaq.faqData,
      initialValues: {
        category: state.editFaq.Category,
        question: state.editFaq.Question,
        answer: state.editFaq.Answer,
      },
      UniCode: state.userData.UniCode,
      helpfaqs: state.faqData,
    };
  } else {
    return {
      helpfaqs: state.faqData,
      UniCode: state.userData.UniCode,
      helpfaqs: state.faqData,
    };
  }
};

const EditFAQMain = connect(mapStateToProps, { updateFaq, loadFAQData })(
  EditFAQFormWrapped
);
export default EditFAQMain;
