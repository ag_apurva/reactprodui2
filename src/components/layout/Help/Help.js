import React from "react";
import MyProfile from "../popups/MyProfile2";
//import { Modal } from "semantic-ui-react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import { putFaq } from "../../../actions/scheduleActions";
import { Field, reduxForm } from "redux-form";
import EditFAQ from "./EditHelp";
import DeleteFAQ from "./DeleteFAQ";
import { Button } from "semantic-ui-react";

const validate = (formValues) => {
  //console.log("formValues", formValues);
  const error = {};

  if (!formValues.category && !formValues.Category1) {
    error.Category1 = "Please Provides an Category";
  }
  if (formValues.category && !formValues.Category2) {
    error.Category2 = "Please Provides an Category";
  }
  if (!formValues.Answer) {
    error.Answer = "Please Provides an Answer";
  }
  if (!formValues.Question) {
    error.Question = "Please Provides an Question";
  }
  return error;
};

class CreateFAQ extends React.Component {
  state = {
    disabled: false,
    uniqueCategory: [],
    helpfaqs: [],
    error: "",
  };

  onSubmit = async (formData) => {
    // console.log("formData", formData);
    if (formData.category) {
      formData["Category"] = formData["Category2"];
    } else {
      formData["Category"] = formData["Category1"];
    }
    try {
      formData["UniCode"] = this.props.UniCode;
      let data = await this.props.putFaq(formData);
      //  console.log("data", data);
      this.props.removePopUp();
    } catch (e) {
      // console.log("error", e);
      this.setState({
        error: e,
      });
      setTimeout(() => {
        this.setState({
          error: "",
        });
        this.props.removePopUp();
      }, 5000);
    }
  };

  componentDidMount = () => {
    const uniqueCategory = [];
    const category = this.props.helpfaqs;
    category.forEach((item) => {
      if (!uniqueCategory.includes(item.Category)) {
        uniqueCategory.push(item.Category);
      }
    });
    this.setState({
      uniqueCategory: uniqueCategory,
      helpfaqs: this.props.helpfaqs,
    });
  };
  componentDidUpdate = (previousProps) => {
    if (previousProps.helpfaqs != this.props.helpfaqs) {
      const uniqueCategory = [];
      const category = this.props.helpfaqs;
      category.forEach((item) => {
        if (!uniqueCategory.includes(item.Category)) {
          uniqueCategory.push(item.Category);
        }
      });
      this.setState({
        uniqueCategory: uniqueCategory,
        helpfaqs: this.props.helpfaqs,
      });
    }
  };
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  renderEmailInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    disabled,
    onChange,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="filter">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((person) => (
              <option key={person.item} value={person.item}>
                {person.item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  // onChange = () => {
  //   console.log("change", this.state.disabled);
  //   this.setState({
  //     disabled: !this.state.disabled,
  //   });
  //   console.log("change", this.state.disabled);
  // };

  render = () => {
    //console.log("Create Help");
    return (
      <Modal
        open={this.props.addFaq}
        onClose={() => this.props.removePopUp()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        aria-hidden="true"
      >
        <Draggable>
          <div
            tabIndex="-1"
            style={{
              paddingLeft: 200,
              paddingTop: 200,
              width: 250,
            }}
          >
            <div className="ui standard modal visible active">
              <div className="header">Schedule Creation</div>
              <div className="content">
                <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                  <div>
                    <label htmlFor="category">Create Category</label>
                    <div>
                      <Field
                        name="category"
                        id="category"
                        component="input"
                        type="checkbox"
                        onChange={() =>
                          this.setState({ disabled: !this.state.disabled })
                        }
                      />
                    </div>
                  </div>
                  <Field
                    disabled={this.state.disabled}
                    label="Category"
                    name="Category1"
                    component={this.renderInput}
                    people={this.state.uniqueCategory.map(function (item) {
                      return { item };
                    })}
                  />
                  <Field
                    disabled={!this.state.disabled}
                    label="Category"
                    type="text"
                    name="Category2"
                    component={this.renderEmailInput}
                    placeholder="Professor Name"
                  />
                  <Field
                    label="Question"
                    type="text"
                    name="Question"
                    component={this.renderEmailInput}
                    placeholder="Question"
                    disabled={false}
                  />
                  <Field
                    label="Answer"
                    type="text"
                    name="Answer"
                    component={this.renderEmailInput}
                    placeholder="Answer"
                    disabled={false}
                  />

                  <div className="row">
                    <div className="col-12">
                      <button
                        // disabled={!this.props.valid}
                        type="submit"
                        className="btn btn-primary mb-2"
                        style={{ backgroundColor: "#4cadad" }}
                      >
                        Save
                      </button>
                    </div>
                  </div>
                </form>
                {this.state.error ? (
                  <div>
                    <p style={{ color: "red" }}>{this.state.error}</p>
                  </div>
                ) : (
                  <div></div>
                )}
              </div>
            </div>
          </div>
        </Draggable>
      </Modal>
    );
  };
}

const CreateFAQFormWrapped = reduxForm({
  form: "Create FAQ",
  validate,
})(CreateFAQ);

const mapStateToPropsPutFaq = (state) => {
  return {
    helpfaqs: state.faqData,
    UniCode: state.userData.UniCode,
  };
};

const CreateFAQMain = connect(mapStateToPropsPutFaq, { putFaq })(
  CreateFAQFormWrapped
);

class HelpFAQProfile extends React.Component {
  state = {
    deleteFAQComponent: "",
    editFAQComponent: "name",
  };
  changeTab = (name) => {
    this.setState({
      deleteFAQComponent: "",
      editFAQComponent: "",
      [name]: "name",
    });
  };
  render() {
    return (
      <div className="ui standard modal visible active">
        <div className="ui secondary pointing menu">
          <button
            className={
              this.state.deleteFAQComponent === "name" ? "item active" : "item"
            }
            onClick={() => this.changeTab("deleteFAQComponent")}
          >
            Delete FAQ
          </button>
          <button
            className={
              this.state.editFAQComponent === "name" ? "item active" : "item"
            }
            onClick={() => this.changeTab("editFAQComponent")}
          >
            Edit FAQ
          </button>
        </div>
        <div className="ui segment">
          {this.state.deleteFAQComponent === "name" ? (
            <DeleteFAQ
              id={this.props.id}
              removePopUpEditDelete={this.props.removePopUpEditDelete}
            />
          ) : (
            <React.Fragment />
          )}
          {this.state.editFAQComponent === "name" ? (
            <EditFAQ
              id={this.props.id}
              item={this.props.item}
              removePopUpEditDelete={this.props.removePopUpEditDelete}
            />
          ) : (
            <React.Fragment />
          )}
        </div>
      </div>
    );
  }
}

class Help extends React.Component {
  state = {
    myProfile: false,
    helpfaqs: [],
    addFaq: false,
    editFAQ: false,
    deleteFAQComponent: "",
    editFAQComponent: "name",
    editFAQId: -1,
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };

  componentDidMount = () => {
    this.setState({
      helpfaqs: this.props.helpfaqs,
    });
  };

  componentDidUpdate = (previousProps) => {
    if (previousProps.helpfaqs != this.props.helpfaqs) {
      this.setState({
        helpfaqs: this.props.helpfaqs,
      });
    }
  };

  editFAQ = () => {
    this.setState({ editFAQ: !this.state.editFAQ });
  };

  editFAQAddPopUp = (id) => {
    this.setState({
      editFAQId: id,
      editFAQ: !this.state.editFAQ,
    });
  };

  removePopUpEditDelete = () => {
    this.setState({
      editFAQ: false,
    });
  };
  renderFAQ = (searchFAQ) => {
    console.log("render faq");
    let faqs = this.state.helpfaqs;
    return faqs.map((item) => {
      //console.log("item FAQ", item);
      let { idUniFAQ, Question, Answer, Category } = item;
      if (
        item.Category.toLowerCase().includes(
          searchFAQ ? searchFAQ.toLowerCase() || "" : ""
        )
      ) {
        return (
          <tr key={idUniFAQ}>
            <Modal
              open={this.state.editFAQ && this.state.editFAQId === idUniFAQ}
              onClose={() => this.editFAQ()}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              aria-hidden="true"
            >
              <Draggable>
                <div
                  tabIndex="-1"
                  style={{
                    paddingLeft: 200,
                    paddingTop: 200,
                    width: 250,
                  }}
                >
                  <HelpFAQProfile
                    id={idUniFAQ}
                    item={item}
                    removePopUpEditDelete={this.removePopUpEditDelete}
                  />
                </div>
              </Draggable>
            </Modal>
            <td>{Category}</td>
            <td>{Question}</td>
            <td>{Answer}</td>
            <td>
              <button onClick={() => this.editFAQAddPopUp(idUniFAQ)}>
                <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
              </button>
            </td>
          </tr>
        );
      } else {
        return null;
      }
    });
  };

  removePopUp = () => {
    //console.log("Called");
    this.setState({
      addFaq: !this.state.addFaq,
      searchCategory: "",
    });
  };
  render() {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          style={{ overflow: "auto", maxHeight: 600 }}
          dimmer={"inverted"}
        >
          <MyProfile myProfile={data} removeMyProfile={this.removeMyProfile} />
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">Help</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input
              type="search"
              onChange={(e) => {
                this.setState({ searchCategory: e.target.value });
              }}
              placeholder="Search here"
            />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img
              src={this.props.profileImageUrl}
              width="50px"
              height="50px"
              alt=""
            />
            <div>
              <h6>{name}</h6>
              <small>Admin</small>
            </div>
          </div>
        </header>

        <main>
          <div style={{ paddingLeft: "20" }}>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ addFaq: true })}
            >
              Add FAQ
            </button>
          </div>
          <div className="table-content m-3">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Category</th>
                  <th scope="col">Question</th>
                  <th scope="col">Answer</th>
                  <th scope="col">Edit/Delete</th>
                </tr>
              </thead>
              <tbody>{this.renderFAQ(this.state.searchCategory)}</tbody>
            </table>
          </div>

          {!this.state.addFaq ? (
            <React.Fragment />
          ) : (
            <CreateFAQMain
              addFaq={this.state.addFaq}
              removePopUp={this.removePopUp}
            />
          )}
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log("State.helpfaq", state.faqData);
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
    helpfaqs: state.faqData,
  };
};
export default connect(mapStateToProps, null)(Help);

//export default Help;
