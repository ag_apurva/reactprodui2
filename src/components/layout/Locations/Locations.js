import React, { useState, useEffect } from "react";
import MyProfile from "../popups/MyProfile2";
import { connect } from "react-redux";
import CreateLocation from "./createLocation";
import { Modal } from "semantic-ui-react";
const Locations = function (props) {
  const [locations, setLocations] = useState([]);
  const [myProfile, setMyProfile] = useState(false);
  const [searchName, setSearchName] = useState("");
  const [createLocationPopUpFlag, setCreateLocationPopUpFlag] = useState(false);
  const [locationProfileFlag, setLocationProfileFlag] = useState(false);
  const [selectedLocation, setSelectedLocation] = useState(-1);
  const removeMyProfile = function () {
    setMyProfile(false);
  };
  useEffect(() => {
    setLocations(props.locations);
  }, [props.locations]);

  const renderLocations = () => {
    return locations.map((item, index) => {
      const ClassRoomName = item.ClassRoomName || " ";
      const ClassRoomDetail = item.ClassRoomDetail || " ";
      const ClassRoomType = item.ClassRoomType || " ";

      return (
        <tr key={index}>
          {createLocationPopUpFlag ? "" : ""}
          <td>{ClassRoomName}</td>
          <td>{ClassRoomDetail}</td>
          <td>{ClassRoomType}</td>
          {/* <td>
            <button
              onClick={() => {
                setLocationProfileFlag(true);
                setSelectedLocation(index);
              }}
            >
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>
          </td> */}
        </tr>
      );
    });
  };
  return (
    <div className="main-content bg-light">
      <Modal
        open={myProfile}
        onClose={() => setMyProfile(false)}
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
      >
        <MyProfile
          myProfile={props.userData}
          removeMyProfile={() => removeMyProfile()}
        />
      </Modal>
      <header>
        <h4>
          <label htmlFor="nav-toggel">
            <span>
              <i className="fa fa-bars" aria-hidden="true"></i>
            </span>
          </label>
          <span className="name">Locations</span>
        </h4>

        <div className="search-wrapper">
          <span>
            <i className="fa fa-search" aria-hidden="true"></i>
          </span>
          <input
            type="search"
            placeholder="Search here"
            onChange={(e) => setSearchName(e.target.value)}
          />
        </div>

        <div className="user-wrapper" onClick={() => setMyProfile(true)}>
          <img src={props.profileImageUrl} width="50px" height="50px" alt="" />
          <div>
            <h6>{props.userData.name}</h6>
            <small>Admin</small>
          </div>
        </div>
      </header>

      <main>
        <div className="filter-wrapper"></div>

        <div className="table-content m-3">
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">ClassRoomName</th>
                <th scope="col">ClassRoomDetail</th>
                <th scope="col">ClassRoomType</th>
                {/* <th scope="col">Edit/Delete</th> */}
              </tr>
            </thead>
            <tbody>{renderLocations()}</tbody>
          </table>
        </div>
        <button
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          onClick={() => setCreateLocationPopUpFlag(true)}
        >
          Add Location
        </button>
        {createLocationPopUpFlag ? (
          <div>
            <CreateLocation
              createLocationPopUpFlag={createLocationPopUpFlag}
              setCreateLocationPopUpFlag={setCreateLocationPopUpFlag}
            />
          </div>
        ) : (
          <div></div>
        )}
      </main>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    locations: state.locations,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, null)(Locations);

/*
ClassRoomName
ClassRoomDetail
ClassRoomType
*/
