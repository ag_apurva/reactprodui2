import { Formik, Field, ErrorMessage } from "formik";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { connect } from "react-redux";
import * as yup from "yup";
import { useState, useEffect } from "react";

import { createLocation } from "../../../actions/scheduleActions";

const CreateLocation = function (props) {
  const [locations, setLocations] = useState([]);
  const validationSchema = yup.object().shape({
    classRoomName: yup.string().required("Class Room Name is Required"),
    classRoomDetail: yup.string().required("Class Room Detail is Required"),
    classRoomType: yup.string().required("Class Room Type is Required"),
  });
  useEffect(() => {
    setLocations(props.locations);
  }, [props.locations]);
  return (
    <Modal
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
      open={props.createLocationPopUpFlag}
      onClose={() => props.setCreateLocationPopUpFlag(false)}
    >
      <Modal.Header>Create Location</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{
            classRoomName: "",
            classRoomDetail: "",
            classRoomType: "",
          }}
          onSubmit={async (formValues) => {
            console.log("formValues", formValues);
            try {
              let data = await props.createLocation(
                {
                  UniCode: props.UniCode,
                  ClassRoomName: formValues.classRoomName,
                  ClassRoomDetail: formValues.classRoomDetail,
                  ClassRoomType: formValues.classRoomType,
                },
                locations
              );
              console.log("location created", data);
              props.setCreateLocationPopUpFlag(false);
            } catch (e) {
              console.log("error creating location", e);
            }
          }}
          validationSchema={validationSchema}
          validate={(formValues) => {
            let errors = {};
            if (!formValues.classRoomName) {
              errors.classRoomName = "*Please provide a Class Room Name";
            }
            if (formValues.classRoomName) {
              //console.log("locations", locations);
              const classRoom = locations.find((location) => {
                return (
                  location.ClassRoomName.toLowerCase() ===
                  formValues.classRoomName.toLowerCase()
                );
              });
              if (classRoom) {
                errors.classRoomName = "*This  Class Room Name Exists";
              }
            }
            if (!formValues.classRoomDetail) {
              errors.classRoomDetail = "*Please provide a Class Room Detail";
            }
            if (!formValues.classRoomType) {
              errors.classRoomType = "*Please provide a Class Room Type";
            }

            return errors;
          }}
        >
          {(formik) => {
            return (
              <Form>
                <FormField>
                  <label>Class Room Name</label>
                  <Field name="classRoomName" />
                  <ErrorMessage name="classRoomName" />
                </FormField>
                <FormField>
                  <label>Class Room Detail</label>
                  <Field name="classRoomDetail" />
                  <ErrorMessage name="classRoomDetail" />
                </FormField>
                <FormField>
                  <label>Class Room Type</label>
                  <Field name="classRoomType" />
                  <ErrorMessage name="classRoomType" />
                </FormField>
                <Button type="submit">Submit</Button>
              </Form>
            );
          }}
        </Formik>
      </Modal.Content>
      <Modal.Actions></Modal.Actions>
    </Modal>
  );
};
const mapStateToProps = (state) => {
  return {
    locations: state.locations,
    UniCode: state.userData.UniCode,
  };
};
export default connect(mapStateToProps, { createLocation })(CreateLocation);
