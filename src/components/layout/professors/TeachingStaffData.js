import React from "react";
import { connect } from "react-redux";

import EditTeacher from "./EditTeacher.js";
import CreateTeacher from "./CreateTeacher.js";
import logo from "./attendance.png";
import MyProfile from "../popups/MyProfile2";
import {
  loadProfessorData,
  deleteProfessor,
} from "../../../actions/scheduleActions";
import { Modal, Segment, Button } from "semantic-ui-react";
class DeleteProfessorComponent extends React.Component {
  state = {
    error: "",
    createTeacher: false,
  };

  onSubmit = async () => {
    try {
      await this.props.deleteProfessor(
        this.props.UniCode,
        this.props.UserEmail,
        this.props.professors
      );
      this.props.editProfessorProfile();
    } catch (e) {
      this.setState({
        error: e,
      });
      setTimeout(() => {
        this.setState({ error: "" });
      }, 5000);
    }
  };
  render() {
    console.log(
      "this",
      this.props.UniCode,
      this.props.UserEmail,
      this.props.UserName
    );
    return (
      <React.Fragment>
        <Modal.Content>
          <p>Are you sure you want to delete {this.props.UserName} ? </p>
          <button
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
            onClick={() => this.onSubmit()}
          >
            Delete {this.props.UserName}
          </button>
        </Modal.Content>
        <Modal.Actions>
          {this.state.error ? (
            <div>
              <p style={{ color: "red" }}>{this.state.error}</p>
            </div>
          ) : (
            <p></p>
          )}
        </Modal.Actions>
      </React.Fragment>
    );
  }
}

const mapStateToPropsDeleteProfessor = (state) => {
  return {
    professors: state.professors,
  };
};

const DeleteProfessor = connect(mapStateToPropsDeleteProfessor, {
  deleteProfessor,
})(DeleteProfessorComponent);

/* this is baiscally the popup when we wan to edit or delete profile.
   the last column in each professor profile */
class ProfessorsProfile extends React.Component {
  state = {
    deleteprofile: "",
    editProfileProfessor: "name",
  };

  changeTab = (name) => {
    this.setState({
      deleteprofile: "",
      editProfileProfessor: "",
      [name]: "name",
    });
  };

  // render() {
  //   return (
  //     <div>
  //       <div className="ui standard modal visible active">
  //         <div className="ui secondary pointing menu">
  //           <button
  //             className={
  //               this.state.deleteprofile === "name" ? "item active" : "item"
  //             }
  //             onClick={() => this.changeTab("deleteprofile")}
  //           >
  //             DeleteProfessor
  //           </button>
  //           <button
  //             className={
  //               this.state.editProfileProfessor === "name"
  //                 ? "item active"
  //                 : "item"
  //             }
  //             onClick={() => this.changeTab("editProfileProfessor")}
  //           >
  //             Edit Professor Profile
  //           </button>
  //         </div>
  //         <div className="ui segment">
  //           {this.state.deleteprofile === "name" ? (
  //             <DeleteProfessor
  //               UserName={this.props.UserName}
  //               UserEmail={this.props.UserEmail}
  //               UniCode={this.props.UniCode}
  //               editProfessorProfile={this.props.editProfessorProfile}
  //             />
  //           ) : (
  //             <React.Fragment />
  //           )}
  //           {this.state.editProfileProfessor === "name" ? (
  //             <EditTeacher
  //               editProfessorProfile={this.props.editProfessorProfile}
  //             />
  //           ) : (
  //             <React.Fragment />
  //           )}
  //         </div>
  //       </div>
  //     </div>
  //   );
  // }

  render() {
    return (
      <React.Fragment>
        <Modal.Header>
          <Button
            underline
            className={
              this.state.deleteprofile === "name" ? "item active" : "item"
            }
            onClick={() => this.changeTab("deleteprofile")}
          >
            DeleteProfessor
          </Button>
          <Button
            className={
              this.state.editProfileProfessor === "name"
                ? "item active"
                : "item"
            }
            onClick={() => this.changeTab("editProfileProfessor")}
          >
            Edit Professor Profile
          </Button>
        </Modal.Header>
        <Segment>
          {this.state.deleteprofile === "name" ? (
            <DeleteProfessor
              UserName={this.props.UserName}
              UserEmail={this.props.UserEmail}
              UniCode={this.props.UniCode}
              editProfessorProfile={this.props.editProfessorProfile}
            />
          ) : (
            <React.Fragment />
          )}
          {this.state.editProfileProfessor === "name" ? (
            <EditTeacher
              editProfessorProfile={this.props.editProfessorProfile}
            />
          ) : (
            <React.Fragment />
          )}
        </Segment>
      </React.Fragment>
    );
  }
}

class TeachingStaffData extends React.Component {
  state = {
    professors: [],
    myProfile: false,
    addProfessors: false,
    editProfessor: false,
    selectedEmail: "",
  };

  componentDidMount = () => {
    this.setState({
      professors: this.props.professors,
    });
  };

  componentDidUpdate = (previousProps) => {
    if (previousProps.professors !== this.props.professors) {
      this.setState({
        professors: this.props.professors,
      });
    }
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };

  editProfessorProfile = (data) => {
    this.setState({
      editProfessor: !this.state.editProfessor,
    });
  };

  loadProfessorProfile = (data) => {
    this.props.loadProfessorData(data);
  };

  renderProfessors = () => {
    let professors = this.state.professors;
    return professors.map((item) => {
      let { UserEmail, UserName, UserPhone } = item;
      let UserImage = logo;
      if (item.UserImage !== "") {
        UserImage = item.UserImage;
      }

      return (
        <tr key={UserEmail}>
          <Modal
            dimmer={"inverted"}
            open={
              this.state.editProfessor && this.state.selectedEmail === UserEmail
            }
            onClose={() => this.editProfessorProfile()}
          >
            <ProfessorsProfile
              UserName={UserName}
              UserEmail={UserEmail}
              UniCode={this.props.UniCode}
              editProfessorProfile={this.editProfessorProfile}
            />
          </Modal>
          <td>
            <span>
              <img src={UserImage} width="50px" height="50px" alt="" />
            </span>
          </td>
          <td>
            <span>{UserName}</span>
          </td>
          <td>{UserEmail}</td>
          <td>{UserPhone ? UserPhone.substring(1, 12) : ""}</td>
          <td>
            <button
              onClick={() => {
                this.setState({
                  selectedEmail: UserEmail,
                });
                this.loadProfessorProfile(item);
                this.editProfessorProfile();
              }}
            >
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  };

  closeProfessor = () => {
    this.setState({
      addProfessors: false, //!this.state.addProfessors,
    });
  };

  render() {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          style={{ overflow: "auto", maxHeight: 600 }}
          dimmer={"inverted"}
        >
          <MyProfile myProfile={data} removeMyProfile={this.removeMyProfile} />
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">TeachingStaffData</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img
              src={this.props.profileImageUrl}
              width="50px"
              height="50px"
              alt=""
            />
            <div>
              <h6>{name}</h6>
              <small>Admin</small>
            </div>
          </div>
        </header>

        <main>
          <div className="filter-wrapper"></div>

          <div className="table-content m-3">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Profile Picture</th>
                  <th scope="col">Name</th>
                  <th scope="col">UserEmail</th>
                  <th scope="col">UserContact</th>
                  <th>Edit/Delete</th>
                </tr>
              </thead>
              <tbody>{this.renderProfessors()}</tbody>
            </table>
          </div>

          <button
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
            onClick={() => this.setState({ addProfessors: true })}
          >
            Add Professor
          </button>
          {!this.state.addProfessors ? (
            <React.Fragment />
          ) : (
            <CreateTeacher
              open={this.state.addProfessors}
              close={this.closeProfessor}
            />
          )}
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log("teachers", state.professors);
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
    professors: state.professors,
    UniCode: state.userData.UniCode,
  };
};

export default connect(mapStateToProps, { loadProfessorData })(
  TeachingStaffData
);
