import React from "react";
import { connect } from "react-redux";
import validator from "validator";
import moment from "moment";
import { Field, reduxForm } from "redux-form";
import { editTeacher } from "../../../actions/scheduleActions";
import { Modal } from "semantic-ui-react";
const validate = (formValues) => {
  let error = {};
  if (!formValues.email) {
    error.email = "Please provide an Email";
  }
  if (!formValues.name) {
    error.name = "Please provide a name";
  }
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  // if (!formValues.primaryContactNumber) {
  //   error.primaryContactNumber = "Please provide a Phone Name";
  // }
  // if (
  //   formValues.primaryContactNumber &&
  //   formValues.primaryContactNumber.length !== 10
  // ) {
  //   error.primaryContactNumber = "* Please provide a 10 digit Phone Name";
  // }
  if (!formValues.registrationNumber) {
    error.registrationNumber = "Please provide a registrationNumber";
  }

  if (!formValues.phone_number) {
    error.phone_number = "Please provide a Phone Name";
  }
  if (formValues.phone_number && formValues.phone_number.length !== 10) {
    error.phone_number = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phone_number) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phone_number)) {
      error.phone_number =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }

  // if (!formValues.primaryContactName) {
  //   error.primaryContactName = "Please provide a Primary Contact Name";
  // }
  if (!formValues.userdob) {
    error.userdob = "Please provide a dateOfbirth";
  }
  if (formValues.userdob) {
    var a = moment(formValues.userdob);
    var b = moment(new Date());
    if (b.diff(a, "years") < 16) {
      error.userdob = "Please provide a professor age of more than 15 years";
    }
    if (b.diff(a, "years") > 70) {
      error.userdob = "Please provide a professor age of less than 70 years";
    }
  }

  if (!formValues.address) {
    error.address = "Please provide a Address";
  }
  if (!formValues.pincode) {
    error.pincode = "Please provide a Pincode";
  }
  if (formValues.pincode) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{3}$/;
    if (!filter.test(formValues.pincode)) {
      error.pincode = "*provide a 6 digit PinCode";
    }
  }
  if (formValues.pincode && formValues.pincode.length !== 6) {
    error.pincode = "* Please provide a 6 digit PinCode";
  }
  if (!formValues.bloodGroup) {
    error.bloodGroup = "Please provide a Blood Group";
  }
  return error;
};

class EditTeacher extends React.Component {
  state = { error: "" };
  onSubmit = async (formValues) => {
    console.log("formvalues", formValues);
    try {
      await this.props.editTeacher(
        formValues,
        this.props.professors,
        this.props.UniCode
      );
      this.props.editProfessorProfile();
    } catch (e) {
      this.setState({
        error: e,
      });
      setTimeout(() => {
        this.setState({ error: "" });
        this.props.editProfessorProfile();
      }, 10000);
    }
  };
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPincodeInput = ({ input, placeholder, meta, type, label }) => {
    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{5}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  // render() {
  //   return (
  //     <div
  //       style={{
  //         height: 600,
  //         overflow: "auto",
  //         "text-align": "justify",
  //       }}
  //     >
  //       <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
  //         <Field
  //           label="Professor Name"
  //           type="text"
  //           name="name"
  //           component={this.renderEmailInput}
  //           placeholder="Professor Name"
  //           disabled={false}
  //         />
  //         <Field
  //           label="Professor Email"
  //           type="text"
  //           name="email"
  //           component={this.renderEmailInput}
  //           placeholder="Professor Email"
  //           disabled={true}
  //         />
  //         <Field
  //           label="Phone Number"
  //           type="text"
  //           name="phone_number"
  //           component={this.renderPhoneInput}
  //           placeholder="xxxxxxxxxx"
  //           disabled={false}
  //         />
  //         <Field
  //           label="User Dob"
  //           type="date"
  //           name="userdob"
  //           component={this.renderEmailInput}
  //           placeholder="User DoB"
  //           disabled={false}
  //         />
  //         {/* <Field
  //           label="Primary Contact Number"
  //           type="text"
  //           name="primaryContactNumber"
  //           component={this.renderPhoneInput}
  //           placeholder="Primary Contact Number"
  //           disabled={false}
  //         />
  //         <Field
  //           label="Primary Contact Name"
  //           type="text"
  //           name="primaryContactName"
  //           component={this.renderEmailInput}
  //           placeholder="Primary Contact Name"
  //           disabled={false}
  //         /> */}
  //         <Field
  //           label="Registration Number"
  //           type="text"
  //           name="registrationNumber"
  //           component={this.renderEmailInput}
  //           placeholder="Registration Number"
  //           disabled={true}
  //         />
  //         <Field
  //           label="Address"
  //           type="text"
  //           name="address"
  //           component={this.renderEmailInput}
  //           placeholder="Address"
  //           disabled={false}
  //         />
  //         <Field
  //           label="PinCode"
  //           type="text"
  //           name="pincode"
  //           component={this.renderPincodeInput}
  //           placeholder="PinCode"
  //           disabled={false}
  //         />
  //         <Field
  //           label="BloodGroup"
  //           type="text"
  //           name="bloodGroup"
  //           component={this.renderEmailInput}
  //           placeholder="BloodGroup"
  //           disabled={false}
  //         />
  //         <div className="row">
  //           <div className="col-12">
  //             <button
  //               // disabled={!this.props.valid}
  //               type="submit"
  //               className="btn btn-primary mb-2"
  //               style={{ backgroundColor: "#4cadad" }}
  //             >
  //               Save
  //             </button>
  //             {this.state.error ? (
  //               <div>
  //                 <p style={{ color: "red" }}>{this.state.error}</p>
  //               </div>
  //             ) : (
  //               <div></div>
  //             )}
  //           </div>
  //         </div>
  //       </form>
  //     </div>
  //   );
  // }

  render() {
    return (
      <Modal.Content>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
          <Field
            label="Professor Name"
            type="text"
            name="name"
            component={this.renderEmailInput}
            placeholder="Professor Name"
            disabled={false}
          />
          <Field
            label="Professor Email"
            type="text"
            name="email"
            component={this.renderEmailInput}
            placeholder="Professor Email"
            disabled={true}
          />
          <Field
            label="Phone Number"
            type="text"
            name="phone_number"
            component={this.renderPhoneInput}
            placeholder="xxxxxxxxxx"
            disabled={false}
          />
          <Field
            label="User Dob"
            type="date"
            name="userdob"
            component={this.renderEmailInput}
            placeholder="User DoB"
            disabled={false}
          />
          {/* <Field
            label="Primary Contact Number"
            type="text"
            name="primaryContactNumber"
            component={this.renderPhoneInput}
            placeholder="Primary Contact Number"
            disabled={false}
          />
          <Field
            label="Primary Contact Name"
            type="text"
            name="primaryContactName"
            component={this.renderEmailInput}
            placeholder="Primary Contact Name"
            disabled={false}
          /> */}
          <Field
            label="Registration Number"
            type="text"
            name="registrationNumber"
            component={this.renderEmailInput}
            placeholder="Registration Number"
            disabled={true}
          />
          <Field
            label="Address"
            type="text"
            name="address"
            component={this.renderEmailInput}
            placeholder="Address"
            disabled={false}
          />
          <Field
            label="PinCode"
            type="text"
            name="pincode"
            component={this.renderPincodeInput}
            placeholder="PinCode"
            disabled={false}
          />
          <Field
            label="BloodGroup"
            type="text"
            name="bloodGroup"
            component={this.renderEmailInput}
            placeholder="BloodGroup"
            disabled={false}
          />
          <div className="row">
            <div className="col-12">
              <button
                // disabled={!this.props.valid}
                type="submit"
                className="btn btn-primary mb-2"
                style={{ backgroundColor: "#4cadad" }}
              >
                Save
              </button>
              {this.state.error ? (
                <div>
                  <p style={{ color: "red" }}>{this.state.error}</p>
                </div>
              ) : (
                <div></div>
              )}
            </div>
          </div>
        </form>
      </Modal.Content>
    );
  }
}

const mapStateToProps = (state) => {
  if (state.professorDataEdit) {
    return {
      initialValues: {
        name: state.professorDataEdit.UserName,
        email: state.professorDataEdit.UserEmail,
        phone_number: state.professorDataEdit.UserPhone
          ? state.professorDataEdit.UserPhone.substring(1, 12)
          : "",
        userdob:
          moment(state.professorDataEdit.UserDOB).format("YYYY-MM-DD") || "",
        primaryContactNumber: state.professorDataEdit.UserParentContactNo
          ? state.professorDataEdit.UserParentContactNo.substring(1, 12)
          : "",
        primaryContactName: state.professorDataEdit.UserParentName,
        registrationNumber: state.professorDataEdit.UserRegistrationNo,
        address: state.professorDataEdit.UserAddress,
        pincode: state.professorDataEdit.UserPinCode,
        bloodGroup: state.professorDataEdit.BloodGroup,
      },
      UniCode: state.userData.UniCode,
      professors: state.professors,
    };
  } else {
    return {
      initialValues: {},
      UniCode: state.userData.UniCode,
      professors: state.professors,
    };
  }
};

const FormWrappedEditProfessor = reduxForm({
  form: "edit professor profile",
  validate,
  enableReinitialize: true,
})(EditTeacher);

export default connect(mapStateToProps, { editTeacher })(
  FormWrappedEditProfessor
);

/*
address: "Address"
​bloodGroup: "B +ve"
​email: "v48@v.com"
​name: "Rajesh Prof 2"
​phone_number: "1234567891"
​pincode: "789654"
​primaryContactName: "parnet"
​primaryContactNumber: "9113445671"
​registrationNumber: "1234c"
​userdob: "1954-05-10"


BloodGroup: "B +ve"
​ChangeId: 2
​IsTrial: 0
​LastUpdateEpochTime: 1622007868
​TrialEndDate: null
​UniCode: "TR027"
​UserActive: "True"
​UserAddress: "Address"
​UserCode: "USR075"
​UserDOB: "1954-05-10T00:00:00.000Z"
​UserDesignation: null
​UserEmail: "v48@v.com"
​UserImage: ""
​UserName: "Rajesh Prof 2"
​UserParentContactNo: "+9113445671"
​UserParentName: "parnet"
​UserPhone: "+1234567891"
​UserPhoto: null
​UserPinCode: "789654"
​UserRegistrationNo: "1234c"
​UserRole: "Professor"
​UserRollNo: 123467
​created_at: "2021-03-07T20:38:36.000Z"
​idUserInfo: 75
​updated_at: "2021-03-07T20:38:36.000Z"

*/
