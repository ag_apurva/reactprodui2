import React from "react";
import { connect } from "react-redux";
import validator from "validator";
import moment from "moment";
import { Field, reduxForm } from "redux-form";
//import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import { createProfessor } from "../../../actions/scheduleActions";

import { Modal } from "semantic-ui-react";
const validate = (formValues) => {
  let error = {};
  if (!formValues.email) {
    error.email = "Please provide an Email";
  }
  if (!formValues.name) {
    error.name = "Please provide a name";
  }
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  // if (!formValues.primaryContactNumber) {
  //   error.primaryContactNumber = "Please provide a Phone Name";
  // }
  // if (
  //   formValues.primaryContactNumber &&
  //   formValues.primaryContactNumber.length !== 10
  // ) {
  //   error.primaryContactNumber = "* Please provide a 10 digit Phone Name";
  // }
  if (!formValues.registrationNumber) {
    error.registrationNumber = "Please provide a Registration Number";
  }

  if (!formValues.phone_number) {
    error.phone_number = "Please provide a Phone Name";
  }
  if (formValues.phone_number && formValues.phone_number.length !== 10) {
    error.phone_number = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phone_number) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phone_number)) {
      error.phone_number = "*Please provide a 10 digit Phone Number";
    }
  }
  // if (!formValues.primaryContactName) {
  //   error.primaryContactName = "Please provide a Primary Contact Name";
  // }
  if (!formValues.userdob) {
    error.userdob = "Please provide a Date Of Birth";
  }
  if (formValues.userdob) {
    var a = moment(formValues.userdob);
    var b = moment(new Date());
    if (b.diff(a, "years") < 16) {
      error.userdob = "Please provide a professor age of more than 15 years";
    }
    if (b.diff(a, "years") > 70) {
      error.userdob = "Please provide a professor age of less than 70 years";
    }
  }

  if (!formValues.address) {
    error.address = "Please provide a Address";
  }
  if (!formValues.pincode) {
    error.pincode = "Please provide a Pincode";
  }

  if (formValues.pincode && formValues.pincode.length !== 6) {
    error.pincode = "* Please provide a 6 digit PinCode";
  }
  if (formValues.pincode) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{3}$/;
    if (!filter.test(formValues.pincode)) {
      error.pincode = "*provide a 6 digit PinCode";
    }
  }

  if (!formValues.bloodGroup) {
    error.bloodGroup = "Please provide a Blood Group";
  }
  return error;
};

class CreateTeacher extends React.Component {
  state = { error: "" };

  onSubmit = async (formValues) => {
    //console.log("formvalues", formValues);
    formValues["UniCode"] = this.props.UniCode;
    try {
      await this.props.createProfessor(formValues, this.props.professors);
      this.props.close();
    } catch (e) {
      this.setState({
        error: e,
      });
      setTimeout(() => {
        this.setState({ error: "" });
        this.props.close();
      }, 20000);
    }
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPincodeInput = ({ input, placeholder, meta, type, label }) => {
    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{5}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  render = () => {
    return (
      <Modal
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
        open={this.props.open}
        onClose={() => this.props.close()}
      >
        <Modal.Header>Add Teacher</Modal.Header>
        <Modal.Content>
          <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
            <Field
              label="Professor Name"
              type="text"
              name="name"
              component={this.renderEmailInput}
              disabled={false}
            />
            <Field
              label="Professor Email"
              type="text"
              name="email"
              component={this.renderEmailInput}
              disabled={false}
            />
            <Field
              label="Phone Numbe"
              type="text"
              name="phone_number"
              component={this.renderPhoneInput}
              disabled={false}
            />
            <Field
              label="User Dob"
              type="date"
              name="userdob"
              component={this.renderEmailInput}
              disabled={false}
            />
            <Field
              label="Registration Number"
              type="text"
              name="registrationNumber"
              component={this.renderEmailInput}
              disabled={false}
            />
            <Field
              label="Address"
              type="text"
              name="address"
              component={this.renderEmailInput}
              disabled={false}
            />
            <Field
              label="PinCode"
              type="text"
              name="pincode"
              component={this.renderPincodeInput}
              disabled={false}
            />
            <Field
              label="BloodGroup"
              type="text"
              name="bloodGroup"
              component={this.renderEmailInput}
              disabled={false}
            />
            <div className="row">
              <div className="col-12">
                <button
                  // disabled={!this.props.valid}
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  Save
                </button>
              </div>
            </div>
          </form>
        </Modal.Content>
        <Modal.Actions>
          {" "}
          {this.state.error ? (
            <div>
              <p style={{ color: "red" }}>{this.state.error}</p>
            </div>
          ) : (
            <div></div>
          )}
        </Modal.Actions>
      </Modal>
    );
  };
}

// class CreateTeacher extends React.Component {
//   state = { error: "" };

//   onSubmit = async (formValues) => {
//     //console.log("formvalues", formValues);
//     formValues["UniCode"] = this.props.UniCode;
//     try {
//       await this.props.createProfessor(formValues, this.props.professors);
//       this.props.close();
//     } catch (e) {
//       this.setState({
//         error: e,
//       });
//       setTimeout(() => {
//         this.setState({ error: "" });
//         this.props.close();
//       }, 20000);
//     }
//   };

//   renderError({ error, touched }) {
//     if (touched && error) {
//       return (
//         <div className="input-group">
//           <div
//             className="input-group-prepend"
//             style={{ backgroundColor: "red" }}
//           ></div>
//           <p style={{ color: "red" }}>{error}</p>
//         </div>
//       );
//     }
//   }
//   renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
//     return (
//       <div className="form-group">
//         <label>{label}</label>
//         <div className="input-group">
//           <div className="input-group-prepend"></div>
//           <input
//             className="form-control"
//             type={type}
//             placeholder={placeholder}
//             {...input}
//             disabled={disabled}
//           />
//         </div>
//         {this.renderError(meta)}
//       </div>
//     );
//   };
//   renderPincodeInput = ({ input, placeholder, meta, type, label }) => {
//     return (
//       <div className="form-group password-field">
//         <label>{label}</label>
//         <div className="input-group">
//           <div className="input-group-prepend"></div>
//           <input
//             required=""
//             className="form-control"
//             pattern="^[1-9][0-9]{5}$"
//             type={type}
//             placeholder={placeholder}
//             {...input}
//           />
//         </div>
//         {this.renderError(meta)}
//       </div>
//     );
//   };
//   renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
//     return (
//       <div className="form-group password-field">
//         <label>{label}</label>
//         <div className="input-group">
//           <div className="input-group-prepend"></div>
//           <input
//             required=""
//             className="form-control"
//             pattern="^[1-9][0-9]{9}$"
//             type={type}
//             placeholder={placeholder}
//             {...input}
//             disabled={disabled}
//           />
//         </div>
//         {this.renderError(meta)}
//       </div>
//     );
//   };

//   render = () => {
//     console.log("reder Professor");
//     return (
//       <Modal
//         open={this.props.open}
//         onClose={() => this.props.close()}
//         aria-labelledby="simple-modal-title"
//         aria-describedby="simple-modal-description"
//         aria-hidden="true"
//       >
//         <div
//           style={{
//             paddingLeft: 400,
//             paddingTop: 100,
//           }}
//         >
//           <div>
//             <div className="ui standard modal visible active">
//               <div className="header">Create Teacher</div>
//               <div
//                 className="content"
//                 style={{
//                   height: 600,
//                   overflow: "auto",
//                   "text-align": "justify",
//                 }}
//               >
//                 <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
//                   <Field
//                     label="Professor Name"
//                     type="text"
//                     name="name"
//                     component={this.renderEmailInput}
//                     placeholder="Professor Name"
//                     disabled={false}
//                   />
//                   <Field
//                     label="Professor Email"
//                     type="text"
//                     name="email"
//                     component={this.renderEmailInput}
//                     placeholder="Professor Email"
//                     disabled={false}
//                   />
//                   <Field
//                     label="Phone Numbe"
//                     type="text"
//                     name="phone_number"
//                     component={this.renderPhoneInput}
//                     placeholder="xxxxxxxxxx"
//                     disabled={false}
//                   />
//                   <Field
//                     label="User Dob"
//                     type="date"
//                     name="userdob"
//                     component={this.renderEmailInput}
//                     placeholder="User DoB"
//                     disabled={false}
//                   />
//                   {/* <Field
//                     label="Primary Contact Number"
//                     type="text"
//                     name="primaryContactNumber"
//                     component={this.renderPhoneInput}
//                     placeholder="Primary Contact Number"
//                     disabled={false}
//                   />
//                   <Field
//                     label="Primary Contact Name"
//                     type="text"
//                     name="primaryContactName"
//                     component={this.renderEmailInput}
//                     placeholder="Primary Contact Name"
//                     disabled={false}
//                   /> */}
//                   <Field
//                     label="Registration Number"
//                     type="text"
//                     name="registrationNumber"
//                     component={this.renderEmailInput}
//                     placeholder="Registration Number"
//                     disabled={false}
//                   />
//                   <Field
//                     label="Address"
//                     type="text"
//                     name="address"
//                     component={this.renderEmailInput}
//                     placeholder="Address"
//                     disabled={false}
//                   />
//                   <Field
//                     label="PinCode"
//                     type="text"
//                     name="pincode"
//                     component={this.renderPincodeInput}
//                     placeholder="PinCode"
//                     disabled={false}
//                   />
//                   <Field
//                     label="BloodGroup"
//                     type="text"
//                     name="bloodGroup"
//                     component={this.renderEmailInput}
//                     placeholder="BloodGroup"
//                     disabled={false}
//                   />
//                   <div className="row">
//                     <div className="col-12">
//                       <button
//                         // disabled={!this.props.valid}
//                         type="submit"
//                         className="btn btn-primary mb-2"
//                         style={{ backgroundColor: "#4cadad" }}
//                       >
//                         Save
//                       </button>
//                       {this.state.error ? (
//                         <div>
//                           <p style={{ color: "red" }}>{this.state.error}</p>
//                         </div>
//                       ) : (
//                         <div></div>
//                       )}
//                     </div>
//                   </div>
//                 </form>
//               </div>
//             </div>
//           </div>
//         </div>
//       </Modal>
//     );
//   };
// }

const FormWrappedEditProfessor = reduxForm({
  form: "create professor profile",
  validate,
  enableReinitialize: true,
})(CreateTeacher);

const mapStateToProps = (state) => {
  return {
    UniCode: state.userData.UniCode,
    professors: state.professors,
  };
};

export default connect(mapStateToProps, { createProfessor })(
  FormWrappedEditProfessor
);

/*
BloodGroup: "B +ve"
​ChangeId: 2
​IsTrial: 0
​LastUpdateEpochTime: 1622005942
​TrialEndDate: null
​UniCode: "TR027"
​UserActive: "True"
​UserAddress: null
UserCode: "USR074"
UserDOB: "1957-03-10T00:00:00.000Z"
UserDesignation: null
UserEmail: "v45@v.com"
UserName: "studentName1"
UserParentContactNo: "1234567800"
UserParentName: "parnet"
UserPhone: "+1234567891"
UserPhoto: null
UserPinCode: null
​UserRegistrationNo: "1234"
​UserRole: "Professor"
​UserRollNo: 1234
​created_at: "2021-03-07T20:31:47.000Z"
​idUserInfo: 74
​updated_at: "2021-03-07T20:31:47.000Z"


*/
