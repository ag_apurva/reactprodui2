import React, { useState, useEffect } from "react";
import MyProfile from "../popups/MyProfile2";

import { useSelector, useDispatch } from "react-redux";
import "react-datetime/css/react-datetime.css";
import { loadResultForStudent } from "../../../reducers/result";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField, List } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import moment from "moment";
const Results = function (props) {
  const [myProfile, setMyProfile] = useState(false);
  const UniCode = useSelector((state) => state.userData.UniCode);
  let studentsData = useSelector((state) => state.students);
  let studentResult = useSelector((state) => state.result.studentResult);
  studentsData = studentsData.map((item) => {
    return { UserCode: item.UserCode, Name: item.name };
  });
  console.log("studentResult", studentResult);
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.userData);
  const profileImageUrl = useSelector((state) => state.uploadedImage.data);

  const removeMyProfile = () => {
    setMyProfile(false);
  };
  const renderResult = function () {
    return studentResult.map((item, index) => {
      let { ExamType, Marks, SubjectName, WeightAge } = item;
      return (
        <tr key={index}>
          <td>{ExamType}</td>
          <td>{Marks}</td>
          <td>{item.OutOf}</td>
          <td>{SubjectName}</td>
          <td>{item.relativeResult}</td>
          <td>{WeightAge}</td>
        </tr>
      );
    });
  };

  return (
    <div className="main-content bg-light">
      <Modal
        open={myProfile}
        onClose={() => setMyProfile(false)}
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
      >
        <MyProfile myProfile={userData} removeMyProfile={removeMyProfile} />
      </Modal>
      <header>
        <h4>
          <label htmlFor="nav-toggel">
            <span>
              <i className="fa fa-bars" aria-hidden="true"></i>
            </span>
          </label>
          <span className="name">Result</span>
        </h4>

        <div className="user-wrapper" onClick={() => setMyProfile(true)}>
          <img src={profileImageUrl} width="50px" height="50px" alt="" />
          <div>
            <h6>{userData["UserName"]}</h6>
            <small>Admin</small>
          </div>
        </div>
      </header>
      <main>
        {/* <div className="filter-wrapper">
          <div className="filter">
            <label>Stream</label>
            <select className="form-control" onChange={(e) => submitResult(e)}>
              <option value="">Select</option>
              {studentsData.map((person) => (
                <option key={person.UserCode} value={person.UserCode}>
                  {person.Name}
                </option>
              ))}
            </select>
          </div>
          <div className="filter fil-btn">
            <label></label>
            <button
              type="submit"
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
            >
              GO
            </button>
          </div>
        </div> */}
        <div className="filter-wrapper">
          <Formik
            initialValues={{
              studentCode: "",
            }}
            onSubmit={(formValues) => {
              dispatch(loadResultForStudent(formValues.studentCode, UniCode));
            }}
            validate={(formValues) => {
              let errors = {};
              if (!formValues.studentCode) {
                errors.studentCode = "* Please choose a student Name";
              }

              return errors;
            }}
          >
            <Form>
              <div className="">
                <label>Stream</label>
                <Field type="text" name="studentCode">
                  {(props) => {
                    return (
                      <select className="form-control" {...props.field}>
                        <option value="">Select</option>
                        {studentsData.map((person) => (
                          <option key={person.UserCode} value={person.UserCode}>
                            {person.Name}
                          </option>
                        ))}
                      </select>
                    );
                  }}
                </Field>
                <ErrorMessage name="studentCode">
                  {(errorMsg) => {
                    return <p style={{ color: "red" }}>{errorMsg}</p>;
                  }}
                </ErrorMessage>
              </div>
              <div className="fil-btn">
                <button
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  GO
                </button>
              </div>
            </Form>
          </Formik>
        </div>
        <div className="table-content m-3">
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">ExamType</th>
                <th scope="col">​​Marks</th>
                <th scope="col">​​OutOf</th>
                <th scope="col">​​Subject Name</th>
                <th scope="col">Marks Relative</th>
                <th scope="col">WeightAge</th>
              </tr>
            </thead>
            <tbody>{renderResult()}</tbody>
          </table>
        </div>
      </main>
    </div>
  );
};

export default Results;
