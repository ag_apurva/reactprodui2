import EditProfile from "./EditProfile";
import React, { useState, useEffect } from "react";

import { Modal, Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { downLoadImage, alertAction } from "../../../actions/authActions";
var { Storage } = require("aws-amplify");

class UplaodProfilePhoto extends React.Component {
  state = { isSpinner: false };
  handleChange = async (e, path) => {
    const file = e.target.files[0];
    console.log("handleChange", file.size);
    if (file.size > 1048576) {
      console.log("file.size");
      alert("Please image size of less than 1 MB");
    } else {
      this.setState({ isSpinner: true });
      try {
        // Upload the file to s3 with private access level.
        const data = await Storage.put(path, file, {
          contentType: "image/jpg",
        });
        console.log("uploadImage", data);
        this.props.downLoadImage(path);
        this.setState({ isSpinner: false });
        // Retrieve the uploaded file to display
      } catch (err) {
        this.setState({ isSpinner: false });
        console.log(err);
      }
    }
  };
  render() {
    let path =
      this.props.myProfile.UniCode +
      "/" +
      this.props.myProfile.UserCode +
      "/profileImage.jpg";
    console.log("this.props uploadprofilephoto", path);
    return (
      <div>
        {this.state.isSpinner ? (
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        ) : (
          <React.Fragment />
        )}
        <input
          type="file"
          accept="image/jpg"
          onChange={(evt) => this.handleChange(evt, path)}
        />{" "}
        Upload Image (less than 1MB)
        {/* <img src={this.state.imageUrl} alt="" /> */}
      </div>
    );
  }
}

const UplaodProfilePhotoMain = connect(null, { downLoadImage })(
  UplaodProfilePhoto
);

const MyProfile = function (props) {
  const [editProfile, setEditProfile] = useState("name");
  const [uploadProfilePhoto, setUploadProfilePhoto] = useState("");
  const [uploadUniversityLogo, setUploadUniversityLogo] = useState("");

  return (
    <React.Fragment>
      <Modal.Header>
        <Button
          className={editProfile === "name" ? "item active" : "item"}
          onClick={() => {
            setEditProfile("name");
            setUploadProfilePhoto("");
            setUploadUniversityLogo("");
          }}
        >
          EditProfile
        </Button>
        <Button
          className={uploadProfilePhoto === "name" ? "item active" : "item"}
          onClick={() => {
            setEditProfile("");
            setUploadProfilePhoto("name");
            setUploadUniversityLogo("");
          }}
        >
          UploadProfilePhoto
        </Button>
        <Button
          className={uploadUniversityLogo === "name" ? "item active" : "item"}
          onClick={() => {
            setEditProfile("");
            setUploadProfilePhoto("");
            setUploadUniversityLogo("name");
          }}
        >
          Upload University Logo
        </Button>
      </Modal.Header>
      <Modal.Content>
        {editProfile === "name" ? (
          <EditProfile
            myProfile={props.myProfile}
            removeMyProfile={props.removeMyProfile}
          />
        ) : (
          <React.Fragment />
        )}
        {uploadProfilePhoto === "name" ? (
          <UplaodProfilePhotoMain myProfile={props.myProfile} />
        ) : (
          <React.Fragment />
        )}
        {uploadUniversityLogo === "name" ? (
          <UplaodProfilePhotoMain myProfile={props.myProfile} />
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
    </React.Fragment>
  );
};

export default MyProfile;
