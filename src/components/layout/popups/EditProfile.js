import { FormField, Button } from "semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { Form } from "formik-semantic-ui-react";
import { useSelector, useDispatch, connect } from "react-redux";
import moment from "moment";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import { useState } from "react";
import { saveMyProfile } from "../../../actions/scheduleActions";
const EditProfile = function (props) {
  const [error, setError] = useState("");
  const userData = useSelector((state) => state.userData);
  console.log("userData", userData);
  const onSubmit = async function (formValues) {
    try {
      formValues["UserPhone"] = "+" + formValues["UserPhone"];

      await props.saveMyProfile(formValues, userData);
      props.removeMyProfile();
    } catch (e) {
      setError(JSON.stringify(e));
      setTimeout(() => {
        props.removeMyProfile();
      }, 10000);
    }
  };
  return (
    <Formik
      initialValues={{
        UniCode: userData.Uni_Code,
        UserCode: userData.UserCode,
        UserEmail: userData.UserEmail,
        UserName: userData.UserName,
        UserDesignation: userData.UserDesignation,
        UserAddress: userData.UserAddress,
        UserPinCode: userData.UserPinCode,
        UserPhone: userData.UserPhone
          ? userData.UserPhone.substring(1, 12)
          : "",
        BloodGroup: userData.BloodGroup,
        UserDOB: moment(userData.UserDOB).format("YYYY-MM-DD"),
      }}
      validate={(formValues) => {
        const errors = {};
        if (!formValues.UserName) {
          errors.UserName = "* Please provide a Name";
        }
        if (!formValues.UserDesignation) {
          errors.UserDesignation = "* Please provide a Designation";
        }
        if (!formValues.UserAddress) {
          errors.UserAddress = "* Please provide a Address";
        }
        if (!formValues.UserPinCode) {
          errors.UserPinCode = "* Please provide a PinCode";
        }
        if (!formValues.UserPhone) {
          errors.UserPhone = "* Please provide a PhoneNumber";
        }
        if (formValues.UserPinCode) {
          let filter = /^[1-9]{1}[0-9]{2}[0-9]{3}$/;
          if (!filter.test(formValues.UserPinCode)) {
            errors.UserPinCode = "* Please provide a 6 digit PinCode";
          }
        }
        if (formValues.UserPhone) {
          let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
          if (!filter.test(formValues.UserPhone)) {
            errors.UserPhone = "* Please provide a 10 digit PhoneNumber";
          }
        }
        return errors;
      }}
      onSubmit={onSubmit}
    >
      <Form>
        <FormField>
          <label htmlFor="idName">Name</label>
          <Field id="idName" name="UserName" />
          <ErrorMessage name="UserName" />
        </FormField>
        <FormField>
          <label htmlFor="idEmail">UserName</label>
          <Field id="idEmail" type="email" name="UserEmail" disabled={"true"} />
        </FormField>
        <FormField>
          <label htmlFor="idDesignation">Designation</label>
          <Field id="idDesignation" name="UserDesignation" />
          <ErrorMessage name="UserDesignation" />
        </FormField>
        <FormField>
          <label htmlFor="idPhoneNumber">PhoneNumber</label>
          <Field id="idPhoneNumber" name="UserPhone" />
          <ErrorMessage name="UserPhone" />
        </FormField>
        <FormField>
          <label htmlFor="idDOB">User DOB</label>
          <Field id="idDOB" name="UserDOB">
            {(props) => {
              const { setFieldValue } = props.form;
              return (
                <Datetime
                  dateFormat="YYYY-MM-DD"
                  timeFormat={false}
                  {...props.field}
                  onChange={(event) => {
                    setFieldValue(
                      "UserDOB",
                      moment(new Date(event._d)).format("YYYY-MM-DD")
                    );
                  }}
                />
              );
            }}
          </Field>
          <ErrorMessage name="UserDOB" />
        </FormField>
        <FormField>
          <label htmlFor="idAddress">Address</label>
          <Field id="idAddress" name="UserAddress" />
          <ErrorMessage name="UserAddress" />
        </FormField>
        <FormField>
          <label htmlFor="idPinCode">PinCode</label>
          <Field id="idPinCode" name="UserPinCode" />
          <ErrorMessage name="UserPinCode" />
        </FormField>

        <FormField>
          <label htmlFor="idBloodGroup">BloodGroup</label>
          <Field id="idBloodGroup" name="BloodGroup" />
          <ErrorMessage name="BloodGroup" />
        </FormField>
        <FormField>
          <Button type="submit">Submit</Button>
        </FormField>
        {error ? <p>{error}</p> : ""}
      </Form>
    </Formik>
  );
};
const mapStateToProps = (state) => {
  return { userData: state.userData };
};
export default connect(mapStateToProps, { saveMyProfile })(EditProfile);

/*
{
        username: "username",
        email: "v@v.vom",
        designation: "designation",
      }

let updateProfileObj = {
  UserName: formValues.name,
  UserEmail: formValues.username,
  BloodGroup: formValues.bloodGroup,
  "​UserAddress": formValues.address,
  "​UserDOB": formValues.userdob,
  UserDesignation: formValues.designation,
  UserParentContactNo: formValues.parentPhoneNumber
    ? "+" + formValues.parentPhoneNumber
    : "",
  "​UserParentName": formValues.parentName,
  UserPinCode: formValues.pincode,
  "​UserPhone": formValues.phone_number
    ? "+" + formValues.phone_number
    : "",
  UniCode: this.props.UniCode,
  UserCode: this.props.myProfile.UserCode,
};

{
    "UniCode": userData.Uni_Code ,
    "UserCode":  userData.UserCode,
    "Name":  userData.UserName,
    "Designation": userData.UserDesignation,
    "Address": userData.UserAddress,
    "PinCode" : userData.UserPinCode,
    "PhoneNumber": userData.UserPhone,
    "BloodGroup": userData.BloodGroup,
    "DOB": userData.UserDOB
}
BloodGroup: "B -ve"
ChangeId: 2
IsTrial: 1
LastUpdateEpochTime: 1625765861
TrialEndDate: "0000-00-00"
UniCode: "TR021"
Uni_Code: "TR021"
Uni_Country: "abcd"
Uni_Logo: "Null"
Uni_Name: "abcd"
Uni_PostalCode: "123456"
UserActive: "True"
UserAddress: "vishalAddress"
UserCode: "USR052"
UserDOB: "1992-10-15T00:00:00.000Z"
UserDesignation: "DesignationCHeck1"
UserEmail: "vishalg48519941994@gmail.com"
UserFBToken: null
UserName: "VishalCheck1"
UserParentContactNo: "+7788991212"
UserParentName: "name33"
UserPhone: "+1122334455"
UserPhoto: null
UserPinCode: "123456"
UserRegistrationNo: null
UserRole: "Admin"
UserRollNo: null
created_at: "2021-03-26T21:14:08.000Z"
idUniversityInfo: 150
idUserInfo: 165
updated_at: "2021-03-26T21:14:08.000Z"



*/
