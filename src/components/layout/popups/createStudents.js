import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import validator from "validator";
import { alertAction } from "../../../actions/authActions";
import AlertComponent from "../../auth/AlertComponent";
import { createStudent } from "../../../actions/appSyncActions";
import moment from "moment";

const validate = (formValues) => {
  let error = {};
  if (!formValues.email) {
    error.email = "Please provide an Email";
  }
  if (!formValues.name) {
    error.name = "Please provide a name";
  }
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  if (!formValues.phoneNumber) {
    error.phoneNumber = "Please provide a Phone Name";
  }
  if (formValues.phoneNumber && formValues.phoneNumber.length !== 10) {
    error.phoneNumber = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.registrationNumber) {
    error.registrationNumber = "Please provide a registrationNumber";
  }
  if (!formValues.parentPhoneNumber) {
    error.parentPhoneNumber = "Please provide a Parent Phone Number";
  }
  if (
    formValues.parentPhoneNumber &&
    formValues.parentPhoneNumber.length !== 10
  ) {
    error.parentPhoneNumber = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.parentName) {
    error.parentName = "Please provide a parentName";
  }
  if (!formValues.rollNo) {
    error.rollNo = "Please provide a rollNo";
  }
  if (!formValues.dateOfbirth) {
    error.dateOfbirth = "Please provide a dateOfbirth";
  }

  if (formValues.dateOfbirth) {
    var a = moment(formValues.dateOfbirth);
    var b = moment(new Date());
    if (b.diff(a, "years") < 16) {
      error.dateOfbirth = "Please provide a student age of more than 15 years";
    }
    if (b.diff(a, "years") > 50) {
      error.dateOfbirth = "Please provide a student age of less than 50 years";
    }
  }

  if (!formValues.admissionCode) {
    error.admissionCode = "Please provide a Admission Code";
  }
  if (!formValues.departmentName) {
    error.departmentName = "Please provide a Department Name";
  }
  if (!formValues.semester) {
    error.semester = "Please provide a Semester";
  }
  if (!formValues.division) {
    error.division = "Please provide a Division";
  }
  if (!formValues.address) {
    error.address = "Please provide a Address";
  }
  if (!formValues.pincode) {
    error.pincode = "Please provide a Pincode";
  }
  if (!formValues.bloodGroup) {
    error.bloodGroup = "Please provide a Blood Group";
  }
  return error;
};

class CreateStudents extends React.Component {
  state = {
    classes: [],
    classesDivisionArray: [],
    admissions: [],
    departments: [],
    disableDepartment: true,
    disableDivision: true,
    disableSemester: true,
    admissionYear: "",
    departmentCode: "",
    admissionCode: "",
    semester: "",
    isSpinner: false,
    partialDivision: [],
    partialSemester: [],
    partialDepartments: [],
    admissionsDept: [],
    classesSemester: [],
  };

  componentDidMount = () => {
    let partialAdmissionsYear = [];

    //filtering the admission array to have unique admission year also.
    let tempAdmissionsYear = this.props.admissions.filter((item) => {
      if (!partialAdmissionsYear.includes(item.admissionYear)) {
        partialAdmissionsYear.push(item.admissionYear);
        return true;
      } else {
        return false;
      }
    });
    this.setState({
      classes: this.props.classes,
      admissionsDept: this.props.admissions,
      admissions: tempAdmissionsYear,
      departments: this.props.departments,
      classesSemester: this.props.classes, //semester
      classesDivision: this.props.classes, // division
    });
  };

  componentDidUpdate = (previousProps) => {
    if (previousProps.admissions !== this.props.admissions) {
      let partialAdmissionsYear = [];

      //filtering the admission array to have unique admission year also.
      let tempAdmissionsYear = this.props.admissions.filter((item) => {
        if (!partialAdmissionsYear.includes(item.admissionYear)) {
          partialAdmissionsYear.push(item.admissionYear);
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        admissions: tempAdmissionsYear,
        admissionsDept: this.props.admissions,
      });
    }

    if (previousProps.departments !== this.props.departments) {
      this.setState({
        departments: this.props.departments,
      });
    }

    if (previousProps.classes !== this.props.classes) {
      this.setState({
        classesSemester: this.props.classes, //semester
        classesDivision: this.props.classes, // division
      });
    }
  };
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  renderEmailInput = ({ input, placeholder, meta, type, label, iconClass }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <input
            className="form-control"
            name="code"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPincodeInput = ({ input, placeholder, meta, type, label }) => {
    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{5}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPhoneInput = ({ input, placeholder, meta, type, label }) => {
    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  onSubmit = async (formValues) => {
    console.log("formvalues", formValues);
    let studentdata = {
      email: formValues.email,
      name: formValues.name,
      rollNo: formValues.rollNo,
      dateOfbirth: formValues.dateOfbirth,
      parentName: formValues.parentName,
      phoneNumber: "+" + formValues.phoneNumber,
      parentPhoneNumber: "+" + formValues.parentPhoneNumber,
      registrationNumber: formValues.registrationNumber,
      address: formValues.address,
      pincode: formValues.pincode,
      bloodGroup: formValues.bloodGroup,
      adminCode: this.props.UniCode,
      admissionCode: this.state.admissionCode,
      semester: formValues.semester,
      division: formValues.division,
    };
    this.setState({ isSpinner: true });
    console.log("this.props.classes", this.props.classes);
    try {
      await this.props.createStudent(studentdata, this.props.classes);
      this.setState({ isSpinner: false });
      this.props.removeStudentPopUpBox();
    } catch (e) {
      this.setState({ isSpinner: false });
      this.props.alertAction(e);
      // console.log("error", e);
      //this.props.removeStudentPopUpBox();
    }
  };
  onChangeCheck = (e) => {
    if (e.target.name === "admissionYear") {
      let admissionsDepartmentFiltered = this.state.admissionsDept;
      let tempDepartmentCodes = [];
      admissionsDepartmentFiltered = admissionsDepartmentFiltered.filter(
        (item) => {
          if (item.admissionYear == e.target.value) {
            tempDepartmentCodes.push(item.departmentCode);
            return true;
          } else {
            return false;
          }
        }
      );
      let partialDepartments = this.state.departments;
      console.log("partialDepartments", partialDepartments);
      partialDepartments = partialDepartments.filter((item) => {
        if (tempDepartmentCodes.includes(item.departmentCode)) {
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        partialDepartments: partialDepartments,
        admissionYear: e.target.value,
        disableDepartment: false,
      });
    }

    if (e.target.name === "departmentName") {
      let departmentCode = this.state.departments.find((item) => {
        return item.departmentName == e.target.value;
      });

      //finding out the admission code corresponsing to department and admission Year.
      let admissionCode = this.state.admissionsDept.filter((item) => {
        if (
          item.departmentCode == departmentCode.departmentCode &&
          item.admissionYear == this.state.admissionYear
        ) {
          return true;
        } else {
          return false;
        }
      });

      // finding out the classes corresponding to admission code and making sure that semester is unique.
      let semesters = [];
      let classes = this.state.classesSemester.filter((item) => {
        if (
          !semesters.includes(item.semester) &&
          item.admissionCode == admissionCode[0].admissionCode
        ) {
          semesters.push(item.semester);
          return true;
        } else {
          return false;
        }
      });

      console.log(
        "this.state.disableSemester false",
        this.state.disableSemester
      );
      this.setState({
        disableSemester: false,
        departmentCode: departmentCode.departmentCode,
        admissionCode: admissionCode[0].admissionCode,
        partialSemester: classes,
      });

      console.log(
        "this.state.disableSemester true",
        this.state.disableSemester
      );
    }

    if (e.target.name === "semester") {
      let divisions = [];

      let classesDivision = this.state.classesDivision.filter((item) => {
        if (
          !divisions.includes(item.division) &&
          item.admissionCode == this.state.admissionCode &&
          item.semester == e.target.value
        ) {
          divisions.push(item.division);
          return true;
        } else {
          return false;
        }
      });
      // DEBUG
      //console.log("classesDivision", classesDivision);
      this.setState({
        disableDivision: false,
        partialDivision: classesDivision,
      });
    }
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label htmlFor="dropDownSelect">Select a {label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((person) => (
              <option key={person.item} value={person.item}>
                {person.item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  render = () => {
    return (
      <div>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
          style={{
            height: 600,
            overflow: "auto",
            "text-align": "justify",
          }}
        >
          <div className="header">Student Creation</div>
          <div className="content">
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
              <Field
                label="Student email"
                type="email"
                name="email"
                component={this.renderEmailInput}
              />
              <Field
                disabled={false}
                label="Admission Year"
                name="admissionYear"
                onChange={this.onChangeCheck}
                component={this.renderInput}
                people={this.state.admissions.map(function (item) {
                  return { item: item.admissionYear };
                })}
              />
              <Field
                disabled={this.state.disableDepartment}
                label="Department Name"
                name="departmentName"
                component={this.renderInput}
                onChange={this.onChangeCheck}
                people={this.state.partialDepartments.map(function (item) {
                  return { item: item.departmentName };
                })}
              />
              <Field
                disabled={this.state.disableSemester}
                label="semester"
                name="semester"
                onChange={this.onChangeCheck}
                component={this.renderInput}
                people={this.state.partialSemester.map(function (item) {
                  return { item: item.semester };
                })}
              />
              <Field
                disabled={this.state.disableDivision}
                label="Division"
                name="division"
                component={this.renderInput}
                people={this.state.partialDivision.map(function (item) {
                  return { item: item.division };
                })}
              />
              <Field
                label="Student name"
                type="text"
                name="name"
                component={this.renderEmailInput}
                placeholder="name"
              />
              <Field
                label="Phone number"
                type="text"
                name="phoneNumber"
                component={this.renderPhoneInput}
                placeholder="phoneNumber"
              />
              <Field
                label="DOB"
                type="date"
                name="dateOfbirth"
                component={this.renderEmailInput}
                placeholder="dateOfbirth"
              />
              <Field
                label="Roll No"
                type="text"
                name="rollNo"
                component={this.renderEmailInput}
                placeholder="Roll Number"
              />
              <Field
                label="Parent Name"
                type="text"
                name="parentName"
                component={this.renderEmailInput}
                placeholder="Parent Name"
              />
              <Field
                label="Parent Phone number"
                type="text"
                name="parentPhoneNumber"
                component={this.renderPhoneInput}
                placeholder="parentPhoneNumber"
              />
              <Field
                label="Registration number"
                type="text"
                name="registrationNumber"
                component={this.renderEmailInput}
                placeholder="Registration number"
              />
              <Field
                label="Address"
                type="text"
                name="address"
                component={this.renderEmailInput}
                placeholder="Address"
              />
              <Field
                label="Pincode"
                type="text"
                name="pincode"
                component={this.renderPincodeInput}
                placeholder="Pincode"
              />
              <Field
                disabled={false}
                label="Blood Group"
                name="bloodGroup"
                onChange={this.onChangeCheck}
                component={this.renderInput}
                people={[
                  { item: "A+" },
                  { item: "B+" },
                  { item: "C+" },
                  { item: "A-" },
                  { item: "B-" },
                  { item: "C-" },
                ]}
              />
              <button
                className="btn btn-primary mb-2"
                style={{ backgroundColor: "#4cadad" }}
                type="submit"
              >
                Submit
              </button>
              <AlertComponent />
            </form>
            {this.state.isSpinner ? (
              <div className="ui active inverted dimmer">
                <div className="ui loader"></div>
              </div>
            ) : (
              <React.Fragment />
            )}
          </div>
        </div>
      </div>
    );
  };
}

const formWrapped = reduxForm({
  form: "create Students",
  validate,
})(CreateStudents);

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    UniCode: state.userData.UniCode,
    admissions: state.admissions,
    classes: state.classes,
    departments: state.departments,
  };
};

export default connect(mapStateToProps, {
  createStudent,
  alertAction,
})(formWrapped);
