import EditProfile from "./EditProfile";
import React, { useState } from "react";
import { connect } from "react-redux";
// import { updateProfileData } from "../../../actions/appSyncActions";
// import AlertComponent from "../../auth/AlertComponent";
// import moment from "moment";
// import { Field, reduxForm } from "redux-form";
// import validator from "validator";
// import { load } from "../../../actions";
import { downLoadImage, alertAction } from "../../../actions/authActions";
var { Storage } = require("aws-amplify");

class UplaodProfilePhoto extends React.Component {
  state = { isSpinner: false };
  handleChange = async (e, path) => {
    const file = e.target.files[0];
    console.log("handleChange", file.size);
    if (file.size > 1048576) {
      console.log("file.size");
      alert("Please image size of less than 1 MB");
    } else {
      this.setState({ isSpinner: true });
      try {
        // Upload the file to s3 with private access level.
        const data = await Storage.put(path, file, {
          contentType: "image/jpg",
        });
        console.log("uploadImage", data);
        this.props.downLoadImage(path);
        this.setState({ isSpinner: false });
        // Retrieve the uploaded file to display
      } catch (err) {
        this.setState({ isSpinner: false });
        console.log(err);
      }
    }
  };
  render() {
    let path =
      this.props.myProfile.UniCode +
      "/" +
      this.props.myProfile.UserCode +
      "/profileImage.jpg";
    console.log("this.props uploadprofilephoto", path);
    return (
      <div>
        {this.state.isSpinner ? (
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        ) : (
          <React.Fragment />
        )}
        <input
          type="file"
          accept="image/jpg"
          onChange={(evt) => this.handleChange(evt, path)}
        />{" "}
        Upload Image (less than 1MB)
        {/* <img src={this.state.imageUrl} alt="" /> */}
      </div>
    );
  }
}

const UplaodProfilePhotoMain = connect(null, { downLoadImage })(
  UplaodProfilePhoto
);

// class MyProfile extends React.Component {
//   state = {
//     editProfile: "name",
//     uploadProfilePhoto: "",
//     uploadUniversityLogo: "",
//   };
//   someFunct = (name) => {
//     this.setState({
//       editProfile: "",
//       uploadProfilePhoto: "",
//       uploadUniversityLogo: "",
//       [name]: "name",
//     });
//   };

//   render() {
//     return (
//       <div>
//         <div
//           onClick={(e) => e.stopPropagation()}
//           className="ui standard modal visible active"
//         >
//           <div className="ui secondary pointing menu">
//             <button
//               className={
//                 this.state.editProfile === "name" ? "item active" : "item"
//               }
//               onClick={() => this.someFunct("editProfile")}
//             >
//               EditProfile
//             </button>
//             <button
//               className={
//                 this.state.uploadProfilePhoto === "name"
//                   ? "item active"
//                   : "item"
//               }
//               onClick={() => this.someFunct("uploadProfilePhoto")}
//             >
//               UploadProfilePhoto
//             </button>
//             <button
//               className={
//                 this.state.uploadUniversityLogo === "name"
//                   ? "item active"
//                   : "item"
//               }
//               onClick={() => this.someFunct("uploadUniversityLogo")}
//             >
//               Upload University Logo
//             </button>
//           </div>
//           <div className="ui segment">
//             {this.state.editProfile === "name" ? (
//               <EditProfile
//                 myProfile={this.props.myProfile}
//                 removeMyProfile={this.props.removeMyProfile}
//               />
//             ) : (
//               <React.Fragment />
//             )}
//             {this.state.uploadProfilePhoto === "name" ? (
//               <UplaodProfilePhotoMain myProfile={this.props.myProfile} />
//             ) : (
//               <React.Fragment />
//             )}
//             {this.state.uploadUniversityLogo === "name" ? (
//               <UplaodProfilePhotoMain myProfile={this.props.myProfile} />
//             ) : (
//               <React.Fragment />
//             )}
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

const MyProfile = function (props) {
  const [editProfile, setEditProfile] = useState("name");
  const [uploadProfilePhoto, setUploadProfilePhoto] = useState("");
  const [uploadUniversityLogo, setUploadUniversityLogo] = useState("");

  return (
    <div>
      <div
        onClick={(e) => e.stopPropagation()}
        className="ui standard modal visible active"
      >
        <div className="ui secondary pointing menu">
          <button
            className={editProfile === "name" ? "item active" : "item"}
            onClick={() => {
              setEditProfile("name");
              setUploadProfilePhoto("");
              setUploadUniversityLogo("");
            }}
          >
            EditProfile
          </button>
          <button
            className={uploadProfilePhoto === "name" ? "item active" : "item"}
            onClick={() => {
              setEditProfile("");
              setUploadProfilePhoto("name");
              setUploadUniversityLogo("");
            }}
          >
            UploadProfilePhoto
          </button>
          <button
            className={uploadUniversityLogo === "name" ? "item active" : "item"}
            onClick={() => {
              setEditProfile("");
              setUploadProfilePhoto("");
              setUploadUniversityLogo("name");
            }}
          >
            Upload University Logo
          </button>
        </div>
        <div className="ui segment">
          {editProfile === "name" ? (
            <EditProfile
              myProfile={props.myProfile}
              removeMyProfile={props.removeMyProfile}
            />
          ) : (
            <React.Fragment />
          )}
          {uploadProfilePhoto === "name" ? (
            <UplaodProfilePhotoMain myProfile={props.myProfile} />
          ) : (
            <React.Fragment />
          )}
          {uploadUniversityLogo === "name" ? (
            <UplaodProfilePhotoMain myProfile={props.myProfile} />
          ) : (
            <React.Fragment />
          )}
        </div>
      </div>
    </div>
  );
};

export default MyProfile;
/*
  render() {
    return (
      <div>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="ui secondary pointing menu">
            <button
              className={
                this.state.editProfile === "name" ? "item active" : "item"
              }
              onClick={() => this.someFunct("editProfile")}
            >
              EditProfile
            </button>
            <button
              className={
                this.state.uploadProfilePhoto === "name"
                  ? "item active"
                  : "item"
              }
              onClick={() => this.someFunct("uploadProfilePhoto")}
            >
              UploadProfilePhoto
            </button>
            <button
              className={
                this.state.uploadUniversityLogo === "name"
                  ? "item active"
                  : "item"
              }
              onClick={() => this.someFunct("uploadUniversityLogo")}
            >
              Upload University Logo
            </button>
          </div>
          <div className="ui segment">
            {this.state.editProfile === "name" ? (
              <EditProfile
                myProfile={this.props.myProfile}
                removeMyProfile={this.props.removeMyProfile}
              />
            ) : (
              <React.Fragment />
            )}
            {this.state.uploadProfilePhoto === "name" ? (
              <UplaodProfilePhotoMain myProfile={this.props.myProfile} />
            ) : (
              <React.Fragment />
            )}
            {this.state.uploadUniversityLogo === "name" ? (
              <UplaodProfilePhotoMain myProfile={this.props.myProfile} />
            ) : (
              <React.Fragment />
            )}
          </div>
        </div>
      </div>
    );
  }
*/
