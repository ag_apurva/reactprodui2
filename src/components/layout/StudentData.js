/*
this component displays student related information
DeleteStudent -> to delete the students directly calls the deleteStudent mutation 
                  which deletes data from cognito and database.

EditStudents -> calls updateStudent mutation and updates the particular mutation.this also gets passed 
                Unicode of Admin,the refresh data of all the students data when a student gets updated.
*/

import React from "react";
import { Redirect } from "react-router-dom";
import MyProfile from "./popups/MyProfile2";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import { Modal as MM } from "semantic-ui-react";
import ImagePopUp from "./ImagePopUp";
import moment from "moment";
import CreateStudents from "./popups/createStudents";
import { loadStudentData } from "../../actions";
import { Field, reduxForm } from "redux-form";
import { deleteStudent, updateStudent } from "../../actions/appSyncActions";
import { alertAction, removeAlert } from "../../actions/authActions";
import AlertComponent from "../auth/AlertComponent";
import logo from "./attendance.png";

class DeleteStudent extends React.Component {
  deleteStudent = async (code, email) => {
    this.props.removePopUp();
    try {
      await this.props.deleteStudent(code, email, this.props.students);
    } catch (e) {
      this.props.alertAction(e);
      setTimeout(() => {
        this.props.removeAlert();
      }, 6000);
    }
  };

  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };

  render() {
    return (
      <div>
        <div className="content">
          <p>Are you sure you want to delete {this.props.myProfile.name} ? </p>
          <button
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
            onClick={() =>
              this.deleteStudent(this.props.UniCode, this.props.myProfile.email)
            }
          >
            Delete {this.props.myProfile.name}
          </button>
          <AlertComponent />
        </div>
      </div>
    );
  }
}
const mp = (state) => {
  return {
    UniCode: state.userData.UniCode,
    students: state.students,
  };
};
const DeleteStudentRedux = connect(mp, {
  deleteStudent,
  alertAction,
  removeAlert,
})(DeleteStudent);

const validate = (formValues) => {
  let error = {};
  if (!formValues.name) {
    error.name = "* Please provide a full Name";
  }
  if (!formValues.phone_number) {
    error.phone_number = "Please provide a Phone Name";
  }
  if (formValues.phone_number && formValues.phone_number.length !== 10) {
    error.phone_number = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.parentPhoneNumber) {
    error.parentPhoneNumber = "Please provide a Parent Phone Name";
  }
  if (
    formValues.parentPhoneNumber &&
    formValues.parentPhoneNumber.length !== 10
  ) {
    error.phone_number = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.parentName) {
    error.parentName = "Please provide a Parent Name";
  }
  if (!formValues.registrationNumber) {
    error.registrationNumber = "Please provide a Registration Number";
  }

  if (!formValues.rollNo) {
    error.rollNo = "Please provide a Registration Number";
  }

  if (!formValues.userdob) {
    error.userdob = "Please provide a userdob";
  }
  if (formValues.userdob) {
    var a = moment(formValues.userdob);
    var b = moment(new Date());
    if (b.diff(a, "years") < 16) {
      error.userdob = "Please provide a student age of more than 15 years";
    }
    if (b.diff(a, "years") > 50) {
      error.dateOfbirth = "Please provide a student age of less than 50 years";
    }
  }

  if (
    formValues.parentPhoneNumber &&
    formValues.parentPhoneNumber.length !== 10
  ) {
    error.parentPhoneNumber = "* Please provide a 10 digit Parent  Phone Name";
  }
  if (!formValues.address) {
    error.address = "Please provide a Address";
  }
  if (!formValues.pincode) {
    error.pincode = "Please provide a Pincode";
  }
  if (!formValues.bloodGroup) {
    error.bloodGroup = "Please provide a Blood Group";
  }
  return error;
};

/*
the form to edit the students profile.
this component also gets passed the univeristyid(UniCode) of the admin
so that whenever any students data gets changed its calls the 
student data query and reload student data.
*/
class EditStudents extends React.Component {
  componentDidMount() {
    this.props.loadStudentData(this.props.myProfile); // this basically calls an actions(loadStudentData) which loads students intital values reducerStudentProfile
    //console.log("A student profile for edit opened", this.props.myProfile);
  }
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }
  onSubmit = async (formValues) => {
    //console.log("formvalues", formValues);

    try {
      formValues.UniCode = this.props.UniCode;
      await this.props.updateStudent(formValues, this.props.students);
      this.props.removePopUp();
    } catch (e) {
      this.props.alertAction(e);
      setTimeout(() => {
        this.props.removeAlert();
      }, 6000);
    }

    // try {
    //   await this.props.createStudent(studentdata, this.props.classes);
    //   this.setState({ isSpinner: false });
    //   this.props.removeStudentPopUpBox();
    // } catch (e) {
    //   this.setState({ isSpinner: false });
    //   this.props.alertAction(e);
    //   // console.log("error", e);
    //   //this.props.removeStudentPopUpBox();
    // }
  };
  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPincodeInput = ({ input, placeholder, meta, type, label }) => {
    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{5}$"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };
  renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  render() {
    return (
      <div
        style={{
          height: 600,
          overflow: "auto",
          "text-align": "justify",
        }}
      >
        <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
          <Field
            label="Student Name"
            type="text"
            name="name"
            component={this.renderEmailInput}
            placeholder="Student Name"
            disabled={false}
          />
          <Field
            label="Student Email"
            type="text"
            name="username"
            component={this.renderEmailInput}
            placeholder="Student Email"
            disabled={true}
          />
          <Field
            label="Phone Number"
            type="text"
            name="phone_number"
            component={this.renderPhoneInput}
            placeholder="xxxxxxxxxx"
            disabled={false}
          />
          <Field
            label="User Dob"
            type="date"
            name="userdob"
            component={this.renderEmailInput}
            placeholder="User DoB"
            disabled={false}
          />
          <Field
            label="Roll No"
            type="text"
            name="rollNo"
            component={this.renderEmailInput}
            placeholder="Roll No"
            disabled={false}
          />

          <Field
            label="ParentPhoneNumber"
            type="text"
            name="parentPhoneNumber"
            component={this.renderPhoneInput}
            placeholder="ParentPhoneNumber"
            disabled={false}
          />

          <Field
            label="Parent Name"
            type="text"
            name="parentName"
            component={this.renderEmailInput}
            placeholder="Parent Name"
            disabled={false}
          />

          <Field
            label="Registration Number"
            type="text"
            name="registrationNumber"
            component={this.renderEmailInput}
            placeholder="Registration Number"
            disabled={false}
          />

          <Field
            label="Address"
            type="text"
            name="address"
            component={this.renderEmailInput}
            placeholder="Address"
            disabled={false}
          />

          <Field
            label="PinCode"
            type="text"
            name="pincode"
            component={this.renderPincodeInput}
            placeholder="PinCode"
            disabled={false}
          />

          <Field
            label="BloodGroup"
            type="text"
            name="bloodGroup"
            component={this.renderEmailInput}
            placeholder="BloodGroup"
            disabled={false}
          />
          <div className="row">
            <div className="col-12">
              <button
                // disabled={!this.props.valid}
                type="submit"
                className="btn btn-primary mb-2"
                style={{ backgroundColor: "#4cadad" }}
              >
                Save
              </button>
            </div>
          </div>
          <AlertComponent />
        </form>
      </div>
    );
  }
}

const FormWrappedEditStudent = reduxForm({
  form: "edit student profile",
  validate,
  enableReinitialize: true,
})(EditStudents);

const mapStateToPropsEditStudents = (state) => {
  if (state.studentProfileValues.data) {
    return {
      initialValues: {
        name: state.studentProfileValues.data.name || "",
        username: state.studentProfileValues.data.email || "",
        phone_number: state.studentProfileValues.data.phoneNumber
          ? state.studentProfileValues.data.phoneNumber.toString().slice(1, 11)
          : "",
        registrationNumber:
          state.studentProfileValues.data.registrationNumber || "",
        rollNo: state.studentProfileValues.data.rollNo || "",
        userdob:
          moment(state.studentProfileValues.data.dateOfbirth).format(
            "YYYY-MM-DD"
          ) || "",
        parentName: state.studentProfileValues.data.parentName || "",
        parentPhoneNumber:
          state.studentProfileValues.data.parentPhoneNumber
            .toString()
            .slice(1, 11) || "",
        address: state.studentProfileValues.data.address || "",
        pincode: state.studentProfileValues.data.pincode || "",
        bloodGroup: state.studentProfileValues.data.bloodGroup || "",
      },
      UniCode: state.userData.UniCode,
      students: state.students,
    };
  } else {
    return {
      initialValues: {},
      UniCode: state.userData.UniCode,
      students: state.students,
    };
  }
};

const StudentEditProfile = connect(mapStateToPropsEditStudents, {
  loadStudentData,
  updateStudent,
  alertAction,
  removeAlert,
})(FormWrappedEditStudent);

/* this is baiscally the popup when we wan to edit or delete profile.
   the last column in each student profile */
class StudentsProfile extends React.Component {
  state = {
    deleteprofile: "",
    editProfileStudent: "name",
  };
  someFunct = (name) => {
    this.setState({
      deleteprofile: "",
      editProfileStudent: "",
      [name]: "name",
    });
  };

  render() {
    return (
      <div>
        <div className="ui standard modal visible active">
          <div className="ui secondary pointing menu">
            <button
              className={
                this.state.deleteprofile === "name" ? "item active" : "item"
              }
              onClick={() => this.someFunct("deleteprofile")}
            >
              DeleteStudent
            </button>
            <button
              className={
                this.state.editProfileStudent === "name"
                  ? "item active"
                  : "item"
              }
              onClick={() => this.someFunct("editProfileStudent")}
            >
              Edit Student Profile
            </button>
          </div>
          <div className="ui segment">
            {this.state.deleteprofile === "name" ? (
              <DeleteStudentRedux
                removePopUp={this.props.removePopUp}
                myProfile={this.props.myProfile}
              />
            ) : (
              <React.Fragment />
            )}
            {this.state.editProfileStudent === "name" ? (
              <StudentEditProfile
                myProfile={this.props.myProfile}
                removePopUp={this.props.removePopUp}
              />
            ) : (
              <React.Fragment />
            )}
          </div>
        </div>
      </div>
    );
  }
}

//this represents all the students data
class StudentData extends React.Component {
  state = {
    myProfile: false,
    addStudents: false,
    students: [],
    editStudentPopUp: false,
    studentEmail: "",
  };
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  componentDidMount() {
    this.setState({ students: this.props.students });
  }

  /* this will be called everytime any student data gets updated, 
     remember evrytime,whenever a student is updated,
     the reload students action inappsyncactions gets called,to bring all the students data.     */
  componentDidUpdate = (previousProps) => {
    if (previousProps.studentData !== this.props.studentData) {
      this.setState({ posts: this.props.studentData.data });
    }
    if (previousProps.students !== this.props.students) {
      this.setState({ students: this.props.students });
    }
  };

  editStudentsAddPopUp = (email) => {
    console.log("edit students add popup", email);
    this.setState({
      editStudentPopUp: true,
      studentEmail: email,
    });
  };
  editStudentsRemovePopUp = (email) => {
    this.setState({
      editStudentPopUp: false,
    });
  };
  setImagePopUp = (email) => {
    this.setState({ imagePopUp: !this.state.imagePopUp, studentEmail: email });
  };
  renderStudents = () => {
    console.log("renderStudents");
    let students = this.state.students;
    return students.map((item) => {
      let {
        email,
        name,
        phoneNumber,
        registrationNumber,
        rollNo,
        semester,
        division,
        subYear,
      } = item;
      let UserImage = logo;
      if (item.UserImage !== "") {
        UserImage = item.UserImage;
      }
      return (
        <tr key={email}>
          <Modal
            open={
              this.state.editStudentPopUp && this.state.studentEmail === email
            }
            onClose={() => this.setState({ editStudentPopUp: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <div>
              <div
                style={{
                  paddingLeft: 400,
                  paddingTop: 100,
                }}
              >
                <StudentsProfile
                  myProfile={item}
                  removePopUp={this.editStudentsRemovePopUp}
                />
              </div>
            </div>
          </Modal>
          {this.state.imagePopUp && this.state.studentEmail === email ? (
            <ImagePopUp
              UserImage={UserImage}
              imagePopUp={this.state.imagePopUp}
              setImagePopUp={this.setImagePopUp}
            />
          ) : (
            <React.Fragment />
          )}
          <td>
            <span>
              <button onClick={() => this.setImagePopUp(email)}>
                <img src={UserImage} width="50px" height="50px" alt="" />
              </button>
            </span>
          </td>
          <td>
            <span>{name}</span>
          </td>
          <td>{phoneNumber}</td>
          <td>{registrationNumber}</td>
          <td>{rollNo}</td>
          <td>{semester}</td>
          <td>{division}</td>
          <td>{subYear}</td>
          <td>
            <button onClick={() => this.editStudentsAddPopUp(email)}>
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  };

  removeStudentPopUpBox = () => {
    this.setState({ addStudents: false });
  };
  render = () => {
    if (this.props.isAuthenticated) {
      let data = this.props.userData;
      let name = data["UserName"];
      // let universityName = data["Uni_Name"];
      return (
        <div className="main-content bg-light">
          <header>
            <h4>
              <label htmlFor="nav-toggel">
                <span>
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </span>
              </label>
              <span className="name">Student Data</span>
            </h4>

            <div className="search-wrapper">
              <span>
                <i className="fa fa-search" aria-hidden="true"></i>
              </span>
              <input type="search" placeholder="Search here" />
            </div>

            <div className="user-wrapper" onClick={this.myProfile}>
              <img
                src={this.props.profileImageUrl}
                width="50px"
                height="50px"
                alt=""
              />
              <div>
                <h6>{name}</h6>
                <small>Admin</small>
              </div>
            </div>
          </header>
          <MM
            open={this.state.myProfile}
            onClose={() => this.setState({ myProfile: false })}
            style={{ overflow: "auto", maxHeight: 600 }}
            dimmer={"inverted"}
          >
            <MyProfile
              myProfile={data}
              removeMyProfile={this.removeMyProfile}
            />
          </MM>
          <Modal
            open={this.state.addStudents}
            onClose={() => this.setState({ addStudents: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <div>
              <div
                style={{
                  paddingLeft: 600,
                  paddingTop: 100,
                  width: 250,
                }}
              >
                <CreateStudents
                  removeStudentPopUpBox={this.removeStudentPopUpBox}
                />
              </div>
            </div>
          </Modal>
          <main>
            <div className="filter-wrapper">
              <div className="filter">
                <label>Stream</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. computer"
                />
              </div>
              <div className="filter">
                <label>Batch</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. morning"
                />
              </div>
              <div className="filter">
                <label>ClassName</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. first year"
                />
              </div>
              <div className="filter">
                <label>Division</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. A"
                />
              </div>
              <div className="filter fil-btn">
                <label></label>
                <button
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  GO
                </button>
              </div>
            </div>

            <div className="table-content m-3">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Profile Picture</th>
                    <th scope="col">Name</th>
                    <th scope="col">PhoneNumber</th>
                    <th scope="col">RegNumber</th>
                    <th scope="col">Roll</th>
                    <th scope="col">Semester</th>
                    <th scope="col">Division</th>
                    <th scope="col">SubYear</th>
                    <th>Edit/Delete</th>
                  </tr>
                </thead>
                <tbody>{this.renderStudents()}</tbody>
              </table>
            </div>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ addStudents: true })}
            >
              Add Student
            </button>
          </main>
        </div>
      );
    } else {
      return <Redirect push to="/" />;
    }
  };
}

const mapStateToProps = (state) => {
  // console.log("students data changed maptoprops", state.uploadedImage);
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    // studentData: state.studentData,
    students: state.students,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, {})(StudentData);
