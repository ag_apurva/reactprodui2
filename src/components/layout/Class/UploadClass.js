import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Modal, Button, List } from "semantic-ui-react";
import CsvDownloader from "react-csv-downloader";
import CSVReader from "react-csv-reader";
import { Parser } from "json2csv";
import moment from "moment";
import { createClassesUpload } from "../../../reducers/batch";

const UploadClassData = function (props) {
  const [downloadTemplate, setDownloadTemplate] = useState(true);
  const [uploadData, setUploadData] = useState(false);
  const [datas, setDatas] = useState([]);
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const classes = useSelector((state) => state.classes);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const [errors, setErrors] = useState([]);
  const [errorsCSV, setCSVErrors] = useState("");
  const [errorProcessed, setErrorsProcessed] = useState(false);
  const dispatch = useDispatch();
  const columns = [
    {
      id: "AdmissionCodeSelect",
      displayName: "Admission Code Select",
    },
    {
      id: "SubYearSelect",
      displayName: "SubYear Select",
    },
    {
      id: "SemisterSelect",
      displayName: "Semister Select",
    },

    {
      id: "DivisionSelect",
      displayName: "Division Select",
    },
    {
      id: "SubDivisionSelect",
      displayName: "Sub Division Select",
    },
    {
      id: "admissionCode",
      displayName: "Admission Code",
    },
    {
      id: "semister",
      displayName: "Semister",
    },
    {
      id: "subYear",
      displayName: "(Choose)SubYear",
    },
    {
      id: "division",
      displayName: "(Choose)Division",
    },
  ];
  useEffect(() => {
    console.log("useEffect called");
    let data = [];

    let admissionsCode = admissionsData.map((item) => {
      return item.admissionCode;
    });
    const semister = [1, 2, 3, 4, 5, 6, 7, 8];
    const subYEar = [1, 2, 3, 4];
    const division = ["A", "B", "C", "D"];
    //console.log("admissionsCode", admissionsCode);
    let itemSize = Math.max(admissionsCode.length, 8);
    for (let i = 0; i < itemSize; i++) {
      let obj = {};
      obj["admissionCode"] =
        i >= admissionsCode.length ? "" : admissionsCode[i];
      obj["semister"] = i >= semister.length ? "" : semister[i];
      obj["subYear"] = i >= subYEar.length ? "" : subYEar[i];
      obj["division"] = i >= division.length ? "" : division[i];
      data.push(obj);
    }
    setDatas(data);
    //console.log("setDatas", data);
  }, []);

  const handleChange = async (data) => {
    //console.log("data", data);
    const dataMap = [];
    let errors = [];
    data.forEach((items, index) => {
      let obj = {};
      Object.keys(items).forEach((item) => {
        if (
          (item === "Admission Code Select" ||
            item === "SubYear Select" ||
            item === "Semister Select" ||
            item === "Division Select" ||
            item === "Sub Division Select") &&
          items[item] &&
          items[item] != ""
        ) {
          obj[
            JSON.parse(JSON.stringify(item.replace(/[\n\r]+/g, "")))
          ] = JSON.parse(JSON.stringify(items[item].replace(/[\n\r]+/g, "")));
        }
      });
      console.log("obj", obj);

      let found = false;
      classes.forEach((item) => {
        console.log("item", item["admissionCode"]);
        console.log("item", item["semester"]);
        console.log("item", item["​division"]);
        console.log("item", item["​subDivision"]);
        console.log("item", item["subYear"]);
      });
      // classes.forEach((iC) => {
      //   console.log("itmeCode", iC, obj);
      //   console.log(
      //     iC["admissionCode"],
      //     obj["Admission Code Select"],
      //     iC["admissionCode"] == obj["Admission Code Select"]
      //   );
      //   console.log("iC", iC["subYear"] == obj["SubYear Select"]);
      //   console.log(
      //     iC["semester"],
      //     obj["Semister Select"],
      //     iC["semester"] == obj["Semister Select"]
      //   );
      //   console.log(
      //     iC["​division"],
      //     obj["Division Select"],
      //     iC["​division"] == obj["Division Select"]
      //   );
      //   console.log(
      //     iC["​subDivision"],
      //     obj["Sub Division Select"],
      //     iC["​subDivision"] == obj["Sub Division Select"]
      //   );
      //   if (
      //     iC["admissionCode"] == obj["Admission Code Select"] &&
      //     iC["subYear"] == obj["SubYear Select"] &&
      //     iC["​semester"] == obj["Semister Select"] &&
      //     iC["​division"] == obj["Division Select"] &&
      //     iC["​subDivision"] == obj["Sub Division Select"]
      //   ) {
      //     console.log(
      //       "TRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRue",
      //       "TRUe"
      //     );
      //     errors.push(obj);
      //     found = true;
      //   }
      // });
      /*processing each object to check for validation
       */
      if (found) {
        if (
          obj["SubYear Select"] === "1" &&
          (obj["Semister Select"] === "2" || obj["Semister Select"] === "1")
        ) {
          dataMap.push(obj);
        } else if (
          obj["SubYear Select"] === "2" &&
          (obj["Semister Select"] === "3" || obj["Semister Select"] === "4")
        ) {
          dataMap.push(obj);
        } else if (
          obj["SubYear Select"] === "3" &&
          (obj["Semister Select"] === "4" || obj["Semister Select"] === "5")
        ) {
          dataMap.push(obj);
        } else if (
          obj["SubYear Select"] === "4" &&
          (obj["Semister Select"] === "7" || obj["Semister Select"] === "8")
        ) {
          dataMap.push(obj);
        } else {
          errors.push(obj);
        }
      }
    });

    if (errors.length > 0) {
      setErrorsProcessed(true);
      const json2csvParser = new Parser();
      const csv = json2csvParser.parse(errors);
      setCSVErrors(csv);
    }
    if (dataMap.length > 0) {
      try {
        let formValues = {};
        formValues["data"] = dataMap;
        formValues["UniCode"] = UniCode;
        let data = await dispatch(createClassesUpload(formValues));
        console.log("data", data);
      } catch (e) {
        console.log("error class upload csv", data);
      }
    }
    console.log("dataMap", dataMap);
  };
  const config = {
    quotes: false, //or array of booleans
    quoteChar: '"',
    escapeChar: '"',
    delimiter: ",",
    header: true,
    newline: "\n\r",
    skipEmptyLines: false, //other option is 'greedy', meaning skip delimiters, quotes, and whitespace.
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      <Modal.Header>
        <Button
          onClick={() => {
            setUploadData(false);
            setDownloadTemplate(true);
          }}
        >
          DownLoadTemplete
        </Button>
        <Button
          onClick={() => {
            setUploadData(true);
            setDownloadTemplate(false);
          }}
        >
          Upload csv
        </Button>
      </Modal.Header>
      <Modal.Content>
        {downloadTemplate ? (
          <Button>
            <CsvDownloader
              filename="UploadClassData"
              extension=".csv"
              columns={columns}
              datas={datas}
              text="Donwload Template"
            />
          </Button>
        ) : (
          <React.Fragment />
        )}
        {uploadData ? (
          <div>
            {/* {spinner ? (
              <div className="ui active inverted dimmer">
                <div className="ui loader"></div>
              </div>
            ) : (
              <React.Fragment />
            )} */}
            <CSVReader
              onFileLoaded={(data, fileInfo) => handleChange(data, fileInfo)}
              parserOptions={config}
            />
            Upload File (less than 10MB)
          </div>
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
      <Modal.Actions>
        {errorProcessed ? (
          <a
            href={
              "data:attachment/csv;charset=utf-8," +
              encodeURIComponent(errorsCSV)
            }
            download="DepartmentsErrors.csv"
            role="button"
          >
            <Button>Download Erros CSV</Button>
          </a>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};

export default UploadClassData;
