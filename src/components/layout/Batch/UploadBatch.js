import { useSelector, useDispatch } from "react-redux";
import { Modal, Button, List } from "semantic-ui-react";
import moment from "moment";
import { createAdmissionsUpload } from "../../../reducers/batch";
import "react-datetime/css/react-datetime.css";
import CSVReader from "react-csv-reader";
import CsvDownloader from "react-csv-downloader";
import React, { useState, useEffect } from "react";
import { Parser } from "json2csv";
const UploadBatchData = function (props) {
  const [downloadTemplate, setDownloadTemplate] = useState(true);
  const [uploadData, setUploadData] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [errorsCSV, setCSVErrors] = useState("");
  const [errors, setErrors] = useState([]);
  const departmentsData = useSelector((state) => state.departments);
  const departments = departmentsData.map((item) => item.departmentName);

  const [errorProcessed, setErrorsProcessed] = useState(false);
  const [datas, setDatas] = useState([]);

  const UniCode = useSelector((state) => state.userData.UniCode);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log("useEffect called");
    let data = [];

    departments.forEach((item) => {
      let obj = { departments: item };
      data.push(obj);
    });

    const years = [
      2016,
      2017,
      2018,
      2019,
      2020,
      2021,
      2022,
      2023,
      2024,
      2025,
      2026,
    ];
    //console.log("setDatas", data);

    data.map((item, index) => {
      if (index + 1 > years.length) {
      } else {
        item.admissionYear = years[index];
      }
    });
    if (years.length > data.length) {
      let index = data.length;
      years.forEach((item, index) => {
        if (index < data.length) {
        } else {
          let tempObj = { admissionYear: item };
          data.push(tempObj);
        }
      });
    }
    //console.log("datas", data);
    setDatas(data);
  }, []);

  const handleChange = async (data) => {
    //const file = JSON.parse(JSON.stringify(data).replace(/\n|\r/g, ""));
    const dataMap = [];
    let errors = [];
    data.forEach((items, index) => {
      let obj = {};
      Object.keys(items).forEach((item) => {
        if (
          (item === "Admission Year" ||
            item === "Department Name" ||
            item === "Degree Offered" ||
            item === "Start Date(YYYY-MM-DD)" ||
            item === "End Date(YYYY-MM-DD)") &&
          items[item] &&
          items[item] != ""
        ) {
          obj[
            JSON.parse(JSON.stringify(item.replace(/[\n\r]+/g, "")))
          ] = JSON.parse(JSON.stringify(items[item].replace(/[\n\r]+/g, "")));
        }
      });
      if (
        Object.keys(obj).length === 5 &&
        moment(obj["End Date(YYYY-MM-DD)"]) >
          moment(obj["Start Date(YYYY-MM-DD)"])
      ) {
        let found = false;
        departmentsData.forEach((item) => {
          if (item.departmentName == obj["Department Name"]) {
            found = true;
            obj["departmentCode"] = item["departmentCode"];
          }
        });
        if (found) {
          dataMap.push(obj);
        } else {
          errors.push(obj);
        }
      } else {
        errors.push(obj);
      }
    });
    console.log("dataMap", dataMap);
    let mainObj = [];
    dataMap.forEach((item) => {
      let obj = {};
      obj["admissionYear"] = item["Admission Year"];
      obj["departmentName"] = item["Department Name"];
      obj["degreeOffered"] = item["Degree Offered"];
      obj["academicStartData"] = item["Start Date(YYYY-MM-DD)"];
      obj["academicEndData"] = item["End Date(YYYY-MM-DD)"];
      obj["departmentCode"] = item["departmentCode"];
      mainObj.push(obj);
    });
    if (errors.length > 1) {
      setErrorsProcessed(true);
      //props.onClose();
    }

    let formValues = {};
    formValues["data"] = mainObj;
    formValues["UniCode"] = UniCode;
    const json2csvParser = new Parser();
    const csv = json2csvParser.parse(errors);
    setCSVErrors(csv);
    if (errors.length > 0) {
      setErrorsProcessed(true);
    }
    try {
      await dispatch(createAdmissionsUpload(formValues));
      if (errors.length === 0) {
        props.onClose();
      }
    } catch (e) {}
  };
  const config = {
    quotes: false, //or array of booleans
    quoteChar: '"',
    escapeChar: '"',
    delimiter: ",",
    header: true,
    newline: "\n\r",
    skipEmptyLines: false, //other option is 'greedy', meaning skip delimiters, quotes, and whitespace.
  };
  const columns = [
    {
      id: "admissions",
      displayName: "Admission Year",
    },
    {
      id: "departmentName",
      displayName: "Department Name",
    },
    {
      id: "degreeOffered",
      displayName: "Degree Offered",
    },
    {
      id: "startDate",
      displayName: "Start Date(YYYY-MM-DD)",
    },
    {
      id: "endDate",
      displayName: "End Date(YYYY-MM-DD)",
    },
    {
      id: "departments",
      displayName: "(Choose)Department Name's",
    },
    {
      id: "admissionYear",
      displayName: "(Choose)Admission Year",
    },
  ];

  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      {errorProcessed ? (
        <Modal
          open={errorProcessed}
          onClose={() => setErrorsProcessed(false)}
          dimmer={"inverted"}
        >
          <Modal.Header>Data that could not be processed</Modal.Header>
          <Modal.Content>
            <List as="ul">
              {errors.map((item) => (
                <List.Item as="li">{item.Name}</List.Item>
              ))}
            </List>
          </Modal.Content>
          <Modal.Actions>
            <a
              href={
                "data:attachment/csv;charset=utf-8," +
                encodeURIComponent(errorsCSV)
              }
              download="DepartmentsErrors.csv"
              role="button"
            >
              <Button>Download Erros CSV</Button>
            </a>
          </Modal.Actions>
        </Modal>
      ) : (
        <React.Fragment />
      )}
      <Modal.Header>
        <Button
          onClick={() => {
            setUploadData(false);
            setDownloadTemplate(true);
          }}
        >
          DownLoadTemplete
        </Button>
        <Button
          onClick={() => {
            setUploadData(true);
            setDownloadTemplate(false);
          }}
        >
          Upload csv
        </Button>
      </Modal.Header>
      <Modal.Content>
        {downloadTemplate ? (
          <Button>
            <CsvDownloader
              filename="UploadAdmissionData"
              extension=".csv"
              columns={columns}
              datas={datas}
              text="Donwload Template"
            />
          </Button>
        ) : (
          <React.Fragment />
        )}
        {uploadData ? (
          <div>
            {spinner ? (
              <div className="ui active inverted dimmer">
                <div className="ui loader"></div>
              </div>
            ) : (
              <React.Fragment />
            )}
            <CSVReader
              onFileLoaded={(data, fileInfo) => handleChange(data, fileInfo)}
              parserOptions={config}
            />
            Upload File (less than 10MB)
          </div>
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
    </Modal>
  );
};

export default UploadBatchData;
