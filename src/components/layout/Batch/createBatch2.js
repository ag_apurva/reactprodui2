import React, { useState } from "react";
import MyProfile from "./popups/MyProfile2";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { reduxForm } from "redux-form";
import { createAdmission } from "../../actions/appSyncActions";
import { alertAction } from "../../actions/authActions";
import AlertComponent from "../auth/AlertComponent";
import { useSelector, useDispatch } from "react-redux";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField, List } from "semantic-ui-react";
import axios from "axios";

import { Form } from "formik-semantic-ui-react";

const AddBatch = function (props) {
  const departments = useSelector((state) => state.departments);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    console.log("fromValues", formValues);
    let error = {};
    if (!formValues.admissionYear) {
      error.admissionYear = "Please provide an Admission Year";
    }
    console.log("Errors", error);
    return error;
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      <Modal.Header>Batch Creation</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{
            admissionYear: "",
            departmentCode: "",
          }}
          validate={validate}
          onSubmit={(formValues) => {
            console.log("formValues", formValues);
          }}
        >
          <Form>
            <FormField>
              <label htmlFor="AdmissionYear" />
              <Field id="AdmissionYear" name="admissionYear">
                {(props) => {
                  console.log("props", props);
                  return (
                    <select {...props.field}>
                      <option value="">None</option>
                      {[
                        { item: 2019 },
                        { item: 2020 },
                        { item: 2021 },
                        { item: 2022 },
                        { item: 2023 },
                        { item: 2024 },
                        { item: 2025 },
                      ].map((item) => (
                        <option value={item.item}>{item.item}</option>
                      ))}
                    </select>
                  );
                }}
              </Field>
              <ErrorMessage name="admissionYear" />
            </FormField>
            <FormField>
              <label htmlFor="AdmissionYear">Departments</label>
              <Field id="AdmissionYear" name="departmentCode">
                {(props) => {
                  return (
                    <select {...props.field}>
                      <option value="">None</option>
                      {departments.map((item) => (
                        <option value={item.departmentCode}>
                          {item.departmentName}
                        </option>
                      ))}
                    </select>
                  );
                }}
              </Field>
            </FormField>
            <Button type="submit">Submit</Button>
          </Form>
        </Formik>
      </Modal.Content>
    </Modal>
  );
};

const CreateBatch = function (props) {
  const [myProfile, setMyProfile] = useState(false);
  const [addBatch, setAddBatch] = useState(false);
  const [batches, setBatches] = useState([]);
  const admissions = useSelector((state) => state.admissions);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.userData);
  const profileImageUrl = useSelector((state) => state.uploadedImage.data);
  const removeBatchPopUpBox = () => {
    setAddBatch(false);
  };

  const renderBatch = () => {
    return admissions.map((item) => {
      let { admissionCode, admissionYear, departmentCode } = item;
      return (
        <tr key={admissionCode}>
          <td>{admissionCode}</td>
          <td>{admissionYear}</td>
          <td>{departmentCode}</td>
        </tr>
      );
    });
  };
  const removeMyProfile = () => {
    setMyProfile(false);
  };

  return (
    <div className="main-content bg-light">
      <Modal
        open={myProfile}
        onClose={() => setMyProfile(false)}
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
      >
        <MyProfile myProfile={{}} removeMyProfile={removeMyProfile} />
      </Modal>
      <header>
        <h4>
          <label htmlFor="nav-toggel">
            <span>
              <i className="fa fa-bars" aria-hidden="true"></i>
            </span>
          </label>
          <span className="name">Create Batch</span>
        </h4>

        <div className="user-wrapper" onClick={() => setMyProfile(true)}>
          <img src={profileImageUrl} width="50px" height="50px" alt="" />
          <div>
            <h6>{userData["userName"]}</h6>
            <small>Admin</small>
          </div>
        </div>
      </header>
      <main>
        {addBatch ? (
          <AddBatch
            open={addBatch}
            onClose={() => setAddBatch(false)}
            removeBatchPopUpBox={removeBatchPopUpBox}
          />
        ) : (
          <React.Fragment />
        )}

        <div className="table-content m-3">
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Admission Code</th>
                <th scope="col">Admission Year</th>
                <th scope="col">Dept Code</th>
              </tr>
            </thead>
            <tbody>{renderBatch()}</tbody>
          </table>
        </div>
        <button
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          onClick={() => setAddBatch(true)}
        >
          Add Batch
        </button>
      </main>
    </div>
  );
};

export default CreateBatch;

// class CreateBatch extends React.Component {
//   state = {
//     myProfile: false,
//     addBatch: false,
//     batchs: [],
//   };

//   componentDidMount = () => {
//     this.setState({ batchs: this.props.admissions });
//   };
//   componentDidUpdate = (previousProps) => {
//     if (previousProps.admissions !== this.props.admissions) {
//       this.setState({ batchs: this.props.admissions });
//     }
//   };
//   removeBatchPopUpBox = () => {
//     this.setState({ addBatch: false });
//   };
//   myProfile = () => {
//     this.setState({
//       myProfile: true,
//     });
//   };
//   renderBatch() {
//     var batchs = this.state.batchs;
//     return batchs.map((item) => {
//       let { admissionCode, admissionYear, departmentCode } = item;
//       return (
//         <tr key={admissionCode}>
//           <td>{admissionCode}</td>
//           <td>{admissionYear}</td>
//           <td>{departmentCode}</td>
//         </tr>
//       );
//     });
//   }
//   removeMyProfile = () => {
//     this.setState({ myProfile: false });
//   };
//   render() {
//     if (this.props.isAuthenticated) {
//       let data = this.props.userData;
//       let name = data["UserName"];
//       return (
//         <div className="main-content bg-light">
//           <Modal
//             open={this.state.myProfile}
//             onClose={() => this.setState({ myProfile: false })}
//             style={{ overflow: "auto", maxHeight: 600 }}
//             dimmer={"inverted"}
//           >
//             <MyProfile
//               myProfile={data}
//               removeMyProfile={this.removeMyProfile}
//             />
//           </Modal>
//           <header>
//             <h4>
//               <label htmlFor="nav-toggel">
//                 <span>
//                   <i className="fa fa-bars" aria-hidden="true"></i>
//                 </span>
//               </label>
//               <span className="name">Create Batch</span>
//             </h4>

//             <div className="user-wrapper" onClick={this.myProfile}>
//               <img
//                 src={this.props.profileImageUrl}
//                 width="50px"
//                 height="50px"
//                 alt=""
//               />
//               <div>
//                 <h6>{name}</h6>
//                 <small>Admin</small>
//               </div>
//             </div>
//           </header>
//           <main>
//             {this.state.addBatch ? (
//               <AddBatch
//                 open={this.state.addBatch}
//                 onClose={() => this.setState({ addBatch: false })}
//                 removeBatchPopUpBox={this.removeBatchPopUpBox}
//               />
//             ) : (
//               <React.Fragment />
//             )}
//             {/* <Modal
//               open={this.state.addBatch}
//               onClose={() => this.setState({ addBatch: false })}
//               style={{ overflow: "auto", maxHeight: 600 }}
//               dimmer={"inverted"}
//             >
//               <AddBatch
//                 open={this.state.addBatch}
//                 onClose={() => this.setState({ addBatch: false })}
//                 removeBatchPopUpBox={this.removeBatchPopUpBox}
//               />
//             </Modal> */
//}
//             <div className="table-content m-3">
//               <table className="table table-hover">
//                 <thead>
//                   <tr>
//                     <th scope="col">Admission Code</th>
//                     <th scope="col">Admission Year</th>
//                     <th scope="col">Dept Code</th>
//                   </tr>
//                 </thead>
//                 <tbody>{this.renderBatch()}</tbody>
//               </table>
//             </div>
//             <button
//               className="btn btn-primary mb-2"
//               style={{ backgroundColor: "#4cadad" }}
//               onClick={() => this.setState({ addBatch: true })}
//             >
//               Add Batch
//             </button>
//           </main>
//         </div>
//       );
//     } else {
//       return <Redirect push to="/" />;
//     }
//   }
// }

// const mapStateToProps = (state) => {
//   return {

//     userData: state.userData,
//     profileImageUrl: state.uploadedImage.data,
//   };
// };
// export default connect(mapStateToProps, null)(CreateBatch);

// class AddBatch extends React.Component {
//   state = { departments: this.props.departments };

//   renderError({ error, touched }) {
//     if (touched && error) {
//       return (
//         <div className="input-group">
//           <div
//             className="input-group-prepend"
//             style={{ backgroundColor: "red" }}
//           ></div>
//           <p style={{ color: "red" }}>{error}</p>
//         </div>
//       );
//     }
//   }

//   renderInput = ({ input, placeholder, meta, type, label, people }) => {
//     return (
//       <div className="form-group">
//         <label htmlFor="dropDownSelect">Select a {label}</label>
//         <div>
//           <select {...input}>
//             <option value="">Select</option>
//             {people.map((person) => (
//               <option key={person.item} value={person.item}>
//                 {person.item}
//               </option>
//             ))}
//           </select>
//         </div>
//         {this.renderError(meta)}
//       </div>
//     );
//   };

//   renderInputDepartment = ({
//     input,
//     placeholder,
//     meta,
//     type,
//     label,
//     people,
//   }) => {
//     return (
//       <div className="form-group">
//         <label htmlFor="dropDownSelect">Select a {label}</label>
//         <div>
//           <select {...input}>
//             <option value="">Select</option>
//             {people.map((person) => (
//               <option key={person.itemName} value={person.itemCode}>
//                 {person.itemName}
//               </option>
//             ))}
//           </select>
//         </div>
//         {this.renderError(meta)}
//       </div>
//     );
//   };

//   removeMyProfile = () => {
//     this.setState({ myProfile: false });
//   };
//   onSubmit = async (formValues) => {
//     try {
//       await this.props.createAdmission(
//         {
//           admissionYear: formValues.admissionYear,
//           departmentCode: formValues.departmentCode,
//           universityCode: this.props.UniCode,
//         },
//         this.props.admissions
//       );
//       this.props.removeBatchPopUpBox();
//     } catch (e) {
//       this.props.alertAction(e);
//     }
//   };

//   render = () => {
//     return (
//       <React.Fragment>
//         <Modal.Header>Batch Creation</Modal.Header>
//         <Modal.Content>
//           <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
//             <Field
//               label="Admission Year"
//               name="admissionYear"
//               component={this.renderInput}
//               people={[
//                 { item: 2019 },
//                 { item: 2020 },
//                 { item: 2021 },
//                 { item: 2022 },
//                 { item: 2023 },
//                 { item: 2024 },
//                 { item: 2025 },
//               ]}
//             />
//             <Field
//               name="departmentCode"
//               label="departmentName"
//               component={this.renderInputDepartment}
//               people={this.state.departments.map(function (item) {
//                 return {
//                   itemName: item.departmentName,
//                   itemCode: item.departmentCode,
//                 };
//               })}
//             />

//             <button
//               className="btn btn-primary mb-2"
//               style={{ backgroundColor: "#4cadad" }}
//               type="submit"
//             >
//               Submit
//             </button>
//             <AlertComponent />
//           </form>
//         </Modal.Content>
//       </React.Fragment>
//     );
//   };
// }
// const formWrapped = reduxForm({
//   form: "create Batch",
//   validate,
// })(AddBatch);

// const mapStateToPropsAddBatch = (state) => {
//   return {
//     departments: state.departments,
//     UniCode: state.userData.UniCode,
//     admissions: state.admissions,
//   };
// };

// const AddBatchRedux = connect(mapStateToPropsAddBatch, {
//   createAdmission,
//   alertAction,
// })(formWrapped);
