import React, { useState, useEffect } from "react";
import MyProfile from "../popups/MyProfile2";
import { createAdmission } from "../../../actions/appSyncActions";
import { batchActions } from "../../../reducers/batch";
import { useSelector, useDispatch } from "react-redux";
import { Modal } from "semantic-ui-react";
import "react-datetime/css/react-datetime.css";
import AddBatch from "./AddBatch";
import UploadBatch from "./UploadBatch";

const CreateBatch = function (props) {
  const [myProfile, setMyProfile] = useState(false);
  const [addBatch, setAddBatch] = useState(false);
  const [addBatchCSV, setAddBatchCSV] = useState(false);
  const [batches, setBatches] = useState([]);
  const admissions = useSelector((state) => state.admissions);
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.userData);
  const profileImageUrl = useSelector((state) => state.uploadedImage.data);

  console.log("admissionsData", admissionsData);

  const renderBatch = () => {
    return admissionsData.map((item) => {
      let { admissionCode, admissionYear, departmentCode } = item;
      let departmentName = item.departmentName || "";
      return (
        <tr key={admissionCode}>
          <td>{admissionCode}</td>
          <td>{admissionYear}</td>
          <td>{departmentCode}</td>
          <td>{departmentName}</td>
        </tr>
      );
    });
  };
  const removeMyProfile = () => {
    setMyProfile(false);
  };

  return (
    <div className="main-content bg-light">
      <Modal
        open={myProfile}
        onClose={() => setMyProfile(false)}
        style={{ overflow: "auto", maxHeight: 600 }}
        dimmer={"inverted"}
      >
        <MyProfile myProfile={userData} removeMyProfile={removeMyProfile} />
      </Modal>
      <header>
        <h4>
          <label htmlFor="nav-toggel">
            <span>
              <i className="fa fa-bars" aria-hidden="true"></i>
            </span>
          </label>
          <span className="name">Create Batch</span>
        </h4>

        <div className="user-wrapper" onClick={() => setMyProfile(true)}>
          <img src={profileImageUrl} width="50px" height="50px" alt="" />
          <div>
            <h6>{userData["UserName"]}</h6>
            <small>Admin</small>
          </div>
        </div>
      </header>
      <main>
        {addBatch ? (
          <AddBatch open={addBatch} onClose={() => setAddBatch(false)} />
        ) : (
          <React.Fragment />
        )}
        {addBatchCSV ? (
          <UploadBatch
            open={addBatchCSV}
            onClose={() => setAddBatchCSV(false)}
          />
        ) : (
          <React.Fragment />
        )}
        <div className="table-content m-3">
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Admission Code</th>
                <th scope="col">Admission Year</th>
                <th scope="col">Dept Code</th>
                <th scope="col">Dept Name</th>
              </tr>
            </thead>
            <tbody>{renderBatch()}</tbody>
          </table>
        </div>
        <button
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          onClick={() => setAddBatch(true)}
        >
          Add Batch
        </button>
        <button
          className="btn btn-primary mb-2"
          style={{ backgroundColor: "#4cadad" }}
          onClick={() => setAddBatchCSV(true)}
        >
          UploadBatchData
        </button>
      </main>
    </div>
  );
};

export default CreateBatch;
