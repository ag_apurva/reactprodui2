import { createAdmission } from "../../../actions/appSyncActions";
import { createAdmissions } from "../../../reducers/batch";
import { useSelector, useDispatch } from "react-redux";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField, List } from "semantic-ui-react";
import moment from "moment";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import React, { useState } from "react";
import { Form } from "formik-semantic-ui-react";
// import axios from "axios";
// import { Parser } from "json2csv";
// import FileDownload from "js-file-download";
// import CsvDownloader from "react-csv-downloader";
// import UploadBatchData from "./UploadBatch";

const AddBatch = function (props) {
  const [error, setError] = useState("");
  const departments = useSelector((state) => state.departments);
  const UniCode = useSelector((state) => state.userData.UniCode);
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    // console.log("fromValues", formValues);
    let error = {};
    if (!formValues.admissionYear) {
      error.admissionYear = "* Please provide an Admission Year";
    }
    if (!formValues.departmentCode) {
      error.departmentCode = "* Please provide an Department Name";
    }

    if (formValues.admissionYear && formValues.departmentCode) {
      admissionsData.forEach((item) => {
        if (
          item.admissionYear == formValues.admissionYear &&
          item.departmentCode == formValues.departmentCode
        ) {
          error.departmentCode =
            "* This combination of year and department exists";
        }
      });
    }

    if (!formValues.degreeOffered) {
      error.degreeOffered = "* Please provide an Degree Offered";
    }
    if (!formValues.academicStartData) {
      error.academicStartData = "* Please provide an Academic Start Date";
    }
    if (!formValues.academicEndData) {
      error.academicEndData = "* Please provide an Academic End Date";
    }
    if (
      moment(formValues.academicStartData) >= moment(formValues.academicEndData)
    ) {
      error.academicEndData =
        "* Academic End Date should be greater than Academic Start Date ";
    }
    // console.log("Errors", error);
    return error;
  };

  const onSubmit = async (formValues) => {
    let departmentCode = formValues.departmentCode;
    console.log("departments", departments);
    departments.forEach((item) => {
      if (item.departmentCode == departmentCode) {
        formValues["DeptName"] = item.departmentName;
      }
    });
    formValues["academicStartData"] = moment(
      formValues["academicStartData"]
    ).format("YYYY-MM-DD");
    formValues["academicEndData"] = moment(
      formValues["academicEndData"]
    ).format("YYYY-MM-DD");
    formValues["UniCode"] = UniCode;
    console.log("formValues", formValues);
    try {
      let data = await dispatch(createAdmissions(formValues));
      props.onClose();
    } catch (e) {
      setError(e);
    }
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      <Modal.Header>Batch Creation</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{
            admissionYear: "",
            departmentCode: "",
            degreeOffered: "",
            academicStartData: new Date(),
            academicEndData: new Date(),
          }}
          validate={validate}
          onSubmit={onSubmit}
        >
          <Form>
            <FormField>
              <label htmlFor="AdmissionYear" />
              <Field id="AdmissionYear" name="admissionYear">
                {(props) => {
                  return (
                    <select {...props.field}>
                      <option value="">None</option>
                      {[
                        { item: 2019 },
                        { item: 2020 },
                        { item: 2021 },
                        { item: 2022 },
                        { item: 2023 },
                        { item: 2024 },
                        { item: 2025 },
                      ].map((item) => (
                        <option value={item.item}>{item.item}</option>
                      ))}
                    </select>
                  );
                }}
              </Field>
              <ErrorMessage name="admissionYear" />
            </FormField>
            <FormField>
              <label htmlFor="DepartmentCode">Departments</label>
              <Field id="DepartmentCode" name="departmentCode">
                {(props) => {
                  return (
                    <select {...props.field}>
                      <option value="">None</option>
                      {departments.map((item, index) => (
                        <option key={index} value={item.departmentCode}>
                          {item.departmentName}
                        </option>
                      ))}
                    </select>
                  );
                }}
              </Field>
              <ErrorMessage name="departmentCode" />
            </FormField>
            <FormField>
              <label>Degree Offered</label>
              <Field name="degreeOffered" />
              <ErrorMessage name="degreeOffered" />
            </FormField>
            <FormField>
              <label htmlFor="startDate">Academic Start Data</label>
              <Field name="academicStartData" id="startDate">
                {(props) => {
                  const { setFieldValue } = props.form;
                  return (
                    <Datetime
                      {...props.field}
                      dateFormat={true}
                      timeFormat={false}
                      dateFormat="YYYY-MM-DD"
                      onChange={(event) => {
                        setFieldValue(props.field.name, event._d);
                      }}
                    />
                  );
                }}
              </Field>
              <ErrorMessage name="academicStartData" />
            </FormField>
            <FormField>
              <label htmlFor="endDate">Academic End Data</label>
              <Field name="academicEndData" id="endDate">
                {(props) => {
                  const { setFieldValue } = props.form;
                  return (
                    <Datetime
                      {...props.field}
                      dateFormat={true}
                      timeFormat={false}
                      dateFormat="YYYY-MM-DD"
                      onChange={(event) => {
                        setFieldValue(props.field.name, event._d);
                      }}
                    />
                  );
                }}
              </Field>
              <ErrorMessage name="academicEndData" />
            </FormField>
            <Button type="submit">Submit</Button>
          </Form>
        </Formik>
      </Modal.Content>
    </Modal>
  );
};

export default AddBatch;
