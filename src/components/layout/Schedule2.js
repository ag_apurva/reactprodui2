import React from "react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import "../css/student.css";
import "../css/schedule.css";
import Calendar from "react-calendar";
import moment from "moment";
import { Field, reduxForm } from "redux-form";
import AlertComponent from "../auth/AlertComponent";
import { loadClassData } from "../../actions/appSyncActions";
import DateTime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import { alertAction, removeAlert } from "../../actions/authActions";
import MyProfile from "./popups/MyProfile";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import axios from "axios";
const validateTask = (formValues) => {
  let error = {};
  return error;
};

class Task extends React.Component {
  state = {
    profNames: [],
    locations: ["CL 23"],
    subjectNames: ["English"],
    disabled: true,
    scheduleDataInfo: [],
    locationDefault: "",
    profDefault: "",
    subjectDefault: "",
    sttartTime: "",
    endTime: "",
  };

  componentDidMount = () => {
    // this.props.loadTaskDetails(this.props.taskDetails);
    // console.log("this.props.taskDetails", this.props.taskDetails);
    this.setState({
      scheduleDataInfo: this.props.scheduleData,
      locations: this.props.scheduleData.Locations,
      profNames: this.props.scheduleData.ProfNames,
      subjectNames: this.props.scheduleData.Subject,
      locationDefault: this.props.taskDetails.Location,
      profDefault: this.props.taskDetails.ProfName,
      subjectDefault: this.props.taskDetails.SubjName,
      currentDate: new Date(this.props.taskDetails.Date),
      sttartTime: new Date(this.props.taskDetails.newStartTime),
      endTime: new Date(this.props.taskDetails.newEndTime),
    });
  };

  onCHanngeLocation = (event, value) => {
    this.setState({ locationDefault: value });
  };

  onCHanngeProf = (event, value) => {
    this.setState({ profDefault: value });
  };

  onCHanngeSubject = (event, value) => {
    this.setState({ subjectDefault: value });
  };

  renderTextFieldProfessor = ({
    label,
    name,
    input,
    meta: { touched, invalid, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <Autocomplete
      id={name}
      name={name}
      {...input}
      className="filter"
      disabled={disabled}
      onChange={this.onCHanngeProf}
      value={this.state.profDefault}
      options={values}
      getOptionSelected={(option, value) => {
        return option === value;
      }}
      getOptionLabel={(option) => {
        return option;
      }}
      {...custom}
      onInputChange={input.onChange}
      style={{ width: 150 }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );

  renderTextFieldLocation = ({
    label,
    name,
    input,
    meta: { touched, invalid, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <Autocomplete
      id={name}
      name={name}
      {...input}
      className="filter"
      disabled={disabled}
      onChange={this.onCHanngeLocation}
      value={this.state.locationDefault}
      options={values}
      getOptionSelected={(option, value) => {
        return option === value;
      }}
      getOptionLabel={(option) => {
        return option;
      }}
      {...custom}
      onInputChange={input.onChange}
      style={{ width: 150 }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );

  renderTextFieldSubject = ({
    label,
    name,
    input,
    meta: { touched, invalid, error },
    defaultValue,
    values,
    disabled,
    custom,
  }) => (
    <Autocomplete
      id={name}
      name={name}
      {...input}
      className="filter"
      disabled={disabled}
      value={this.state.subjectDefault}
      onChange={this.state.onCHanngeSubject}
      options={values}
      getOptionSelected={(option, value) => {
        return option === value;
      }}
      getOptionLabel={(option) => {
        return option;
      }}
      {...custom}
      onInputChange={input.onChange}
      style={{ width: 150 }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );

  renderTimeField = () => {};

  onSubmit = (formValues) => {
    console.log("formValues", formValues);
  };

  onClick = () => {
    this.setState({ disabled: false });
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  changeStartTime = (value, inputStart) => {
    inputStart.onChange(value._d);
    this.setState({ sttartTime: new Date(value._d) });
  };

  changeEndTime = (value, inputEnd) => {
    inputEnd.onChange(value._d);
    this.setState({ endTime: new Date(value._d) });
  };

  changeDate = (value, inputEnd) => {
    inputEnd.onChange(value._d);
    this.setState({ currentDate: new Date(value._d) });
  };

  dateComponent = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          disabled={disabled}
          {...input}
          dateFormat="YYYY-MM-DD"
          timeFormat={false}
          name={name}
          value={this.state.currentDate}
          onChange={(event) => this.changeDate(event, input)}
        />
      </div>
    );
  };

  timeComponentEndTime = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          disabled={disabled}
          {...input}
          dateFormat={false}
          name={name}
          timeFormat={"hh-mm"}
          value={this.state.endTime}
          onChange={(event) => this.changeEndTime(event, input)}
        />
      </div>
    );
  };

  timeComponentStartTime = ({
    defaultValue,
    input,
    name,
    placeholder,
    meta,
    type,
    label,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <DateTime
          disabled={disabled}
          {...input}
          dateFormat={false}
          name={name}
          timeFormat={"hh-mm"}
          value={this.state.sttartTime}
          onChange={(event) => this.changeStartTime(event, input)}
        />
      </div>
    );
  };

  render = () => {
    console.log("render");
    return (
      <div
        style={{ display: this.props.taskHidden ? "none" : "" }}
        className="col-lg-3 task-wrap bg-white"
      >
        <div className="task-heading pt-2">
          <h4>Task Details</h4>
          <span>
            <button onClick={this.onClick}>Edit/Delete</button>
          </span>
        </div>

        <div className="row task-card">
          <div className="task-inner">
            <div className="task-head" style={{ background: "#ffecc9" }}>
              <small>Class Details</small>
            </div>
          </div>

          <div className="col-12 ">
            <div className="time">
              <div>
                <p>
                  <b>Admission Year </b> {this.props.admissionYear}
                </p>
              </div>
              <div>
                <p>
                  <b>Semester </b> {this.props.semester}
                </p>
              </div>
            </div>
            <div className="time">
              <div>
                <p>
                  <b>Department </b>
                  {this.props.departmentName
                    ? this.props.departmentName
                    : "depat"}
                </p>
              </div>
              <div>
                <p>
                  <b>Division </b> {this.props.division}
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="row task-card">
          <div className="task-inner">
            <div className="task-head" style={{ background: "#ffecc9" }}>
              <small>Timing Details</small>
            </div>
          </div>

          <div className="col-12 ">
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
              <div className="time">
                <div>
                  <Field
                    defaultValue={
                      this.props.taskDetails.Location
                        ? this.props.taskDetails.Location
                        : ""
                    }
                    disabled={this.state.disabled}
                    name="location"
                    values={this.state.locations}
                    component={this.renderTextFieldLocation}
                    label="Location"
                    type="text"
                  />
                </div>
              </div>

              <div className="time">
                <div>
                  <Field
                    defaultValue={
                      this.props.taskDetails.SubjName
                        ? this.props.taskDetails.SubjName
                        : "English"
                    }
                    disabled={this.state.disabled}
                    name="SubjectName"
                    values={this.state.subjectNames}
                    component={this.renderTextFieldSubject}
                    label="Subject Name"
                    type="text"
                  />
                </div>
              </div>

              <div className="time">
                <div>
                  <Field
                    defaultValue={
                      this.props.taskDetails.ProfName
                        ? this.props.taskDetails.ProfName
                        : ""
                    }
                    disabled={this.state.disabled}
                    name="professorName"
                    values={this.state.profNames}
                    component={this.renderTextFieldProfessor}
                    label="Professor"
                    type="text"
                  />
                </div>
              </div>
              <div className="time">
                <div>{}</div>
              </div>

              <div className="time">
                <div>
                  <Field
                    disabled={this.state.disabled}
                    label="Date"
                    value={this.state.currentDate}
                    name="date"
                    component={this.dateComponent}
                    placeholder="Date"
                  />
                </div>
              </div>

              <div className="time">
                <div>
                  <Field
                    disabled={this.state.disabled}
                    defaultValue={this.state.sttartTime}
                    value={this.state.sttartTime}
                    label="Start Time"
                    name="sttartTime"
                    component={this.timeComponentStartTime}
                    placeholder="Start Time"
                  />
                </div>
              </div>

              <div className="time">
                <div>
                  <Field
                    disabled={this.state.disabled}
                    defaultValue={this.state.endTime}
                    value={this.state.endTime}
                    label="End Time"
                    name="endTime"
                    component={this.timeComponentEndTime}
                    placeholder="End Time"
                  />
                </div>
              </div>

              <div className="time">
                <div>
                  <input
                    disabled={this.state.disabled}
                    type="submit"
                    className="btn btn-primary mb-2"
                    style={{ backgroundColor: "#4cadad" }}
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  };
}

const formWrappedTask = reduxForm({
  form: "Task",
  validateTask,
})(Task);

const mapStateToPropsTask = (state) => {
  return {
    scheduleData: state.scheduleDataInformation || {},
  };
};

const validateProfessor = (formValues) => {
  let error = {};
  return error;
};

class ProfessorSubmit extends React.Component {
  state = {
    scheduleData: {},
    isSpinner: false,
  };

  componentDidMount = () => {
    console.log("this.state.scheduleData", this.state.scheduleData);
    this.setState({
      scheduleData: this.props.scheduleData,
    });
  };
  processScheduleData = (unfilteredScheduleData) => {
    var dataMapped = {};
    unfilteredScheduleData.forEach((item) => {
      var itemDate = moment(item.Date).format("YYYY-MM-DD");
      if (!dataMapped.hasOwnProperty(itemDate)) {
        dataMapped[itemDate] = [];
      }
      item["StartTime"] = moment(item.newStartTime);
      item["EndTime"] = moment(item.newEndTime);
      dataMapped[itemDate].push(item);
    });
    // all the keys in dataMapped are the dates in yyyy-mm-dd format.

    var dataMappedMain = {};
    Object.keys(dataMapped).forEach((item) => {
      dataMappedMain[item] = {};
      for (let i = 6; i < 22; i++) {
        dataMappedMain[item][i] = [];
      }
      dataMapped[item].forEach((data) => {
        let startTime = moment(data.StartTime);
        let endTime = moment(data.EndTime);
        let minutes = moment.duration(endTime.diff(startTime)).asMinutes();
        dataMappedMain[item][startTime.hour()].push({
          ...data,
          top: startTime.minutes(),
          height: minutes,
        });
      });
    });

    Object.keys(dataMappedMain).forEach((item) => {
      Object.keys(dataMappedMain[item]).forEach((data) => {
        if (dataMappedMain[item][data].length === 0) {
          dataMappedMain[item][data].push({ height: 0, top: 0 });
        }
      });
    });
    return dataMappedMain;
  };
  componentDidUpdate = (previousProps) => {
    if (previousProps.scheduleData !== this.props.scheduleData) {
      this.setState({
        scheduleData: this.props.scheduleData,
      });
    }
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="filter">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((item) => (
              <option key={item.split(",")[0]} value={item.split(",")[1]}>
                {item.split(",")[0]}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  onSubmit = async (formValues) => {
    console.log("event prof submit", formValues.ProfEmail);

    this.setState({
      isSpinner: true,
    });
    let emailName = "arung@innocurve.com";
    let codeName = "TR021";
    var config = {
      method: "get",
      params: {
        email: formValues.ProfEmail,
        universityCode: "TR021",
      },
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/getschedule",
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      let data = await axios(config);
      console.log("data", data);
      const processedData = this.processScheduleData(data.data);
      console.log("processedData", processedData);
      this.props.renderScheduleData(processedData);
    } catch (e) {
      console.log("e", e);
    }
    this.setState({
      isSpinner: false,
    });
  };

  render = () => {
    console.log(
      "this.state.scheduleData.ProfNames",
      this.state.scheduleData.ProfNames
    );
    return (
      <form
        className="filter-wrapper"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field
          disabled={false}
          label="Admission Year"
          name="ProfEmail"
          onChange={this.onChangeCheck}
          component={this.renderInput}
          people={
            this.state.scheduleData.ProfNames
              ? this.state.scheduleData.ProfNames
              : []
          }
        />
        <div className="filter fil-btn">
          <label></label>
          <button
            type="submit"
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
          >
            Submit
          </button>
          <AlertComponent />
        </div>
        {this.state.isSpinner ? (
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        ) : (
          <React.Fragment />
        )}
      </form>
    );
  };
}

const formSubmitTask = reduxForm({
  form: "Display Professor",
  validateProfessor,
})(ProfessorSubmit);

const mapStateToPropsProfessor = (state) => {
  console.log("state.scheduleDataInformation", state.scheduleDataInformation);
  return {
    scheduleData: state.scheduleDataInformation || {},
  };
};

const TaskMain = connect(mapStateToPropsTask, {})(formWrappedTask);
const ProfessorSubmitTask = connect(
  mapStateToPropsProfessor,
  {}
)(formSubmitTask);

const validate = (formValues) => {
  let error = {};
  if (!formValues.admissionYear) {
    error.admissionYear = "* Please provide a Admission Year";
  }
  if (!formValues.departmentName) {
    error.departmentName = "* Please provide a Department Name";
  }
  if (!formValues.semester) {
    error.semester = "* Please provide a Semester";
  }
  if (!formValues.division) {
    error.division = "* Please provide a Division";
  }
  return error;
};

class Schedule extends React.Component {
  state = {
    scheduleData: {},
    profNames: [],
    locations: [],
    defaultFilter: "Class",
    people: [{ item: "Class" }, { item: "Professor" }, { item: "Location" }],
    openFilter: false,
    taskHidden: true,
    isSpinner: false,
    myProfile: false,
    value: new Date(),
    selectedDate: new Date(),
    departmentName: "", // this is set on onChangeCheck
    semester: "", //  this is set on onChangeCheck
    division: "", //  this is set in onSubmit function
    searchDateMoment: "",
    scheduleData: {},
    scheduleDataList: [],
    processedDateData: {},
    admissions: [],
    admissionYear: "",
    departments: [],
    partialDepartments: [],
    departmentCode: "",
    classesDivision: [],
    partialDivision: [],
    admissionCode: "",
    admissionsDept: [],
    classesSemester: [],
    partialSemester: [],
    disableSemester: true,
    disableDivision: true,
    disableDepartment: true,
    unfilteredScheduleData: [],
    taskDetails: "", // this values gets filled every time there a class is picked
    sttartTime: new Date(),
  };
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };
  onChangeCheck = (e) => {
    if (e.target.name === "admissionYear") {
      let admissionsDepartmentFiltered = this.state.admissionsDept;
      let tempDepartmentCodes = [];
      admissionsDepartmentFiltered = admissionsDepartmentFiltered.filter(
        (item) => {
          if (item.admissionYear === e.target.value) {
            tempDepartmentCodes.push(item.departmentCode);
            return true;
          } else {
            return false;
          }
        }
      );
      let partialDepartments = this.state.departments;
      partialDepartments = partialDepartments.filter((item) => {
        if (tempDepartmentCodes.includes(item.departmentCode)) {
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        partialDepartments: partialDepartments,
        admissionYear: e.target.value,
        disableDepartment: false,
      });
    }

    if (e.target.name === "departmentName") {
      let departmentCode = this.state.departments.find((item) => {
        return item.departmentName === e.target.value;
      });

      //finding out the admission code corresponsing to department and admission Year.
      let admissionCode = this.state.admissionsDept.filter((item) => {
        if (
          item.departmentCode === departmentCode.departmentCode &&
          item.admissionYear === this.state.admissionYear
        ) {
          return true;
        } else {
          return false;
        }
      });

      // finding out the classes corresponding to admission code and making sure that semester is unique.
      let semesters = [];
      let classes = this.state.classesSemester.filter((item) => {
        if (
          !semesters.includes(item.semester) &&
          item.admissionCode === admissionCode[0].admissionCode
        ) {
          semesters.push(item.semester);
          return true;
        } else {
          return false;
        }
      });

      this.setState({
        disableSemester: false,
        departmentCode: departmentCode.departmentCode,
        departmentName: e.target.value,
        admissionCode: admissionCode[0].admissionCode,
        partialSemester: classes,
      });
    }

    if (e.target.name === "semester") {
      let divisions = [];

      let classesDivision = this.state.classesDivision.filter((item) => {
        if (
          !divisions.includes(item.division) &&
          item.admissionCode === this.state.admissionCode &&
          item.semester === e.target.value
        ) {
          divisions.push(item.division);
          return true;
        } else {
          return false;
        }
      });
      // DEBUG
      //console.log("classesDivision", classesDivision);
      this.setState({
        disableDivision: false,
        semester: e.target.value,
        partialDivision: classesDivision,
      });
    }
  };

  renderScheduleData = (scheduleData) => {
    this.setState({
      scheduleData: scheduleData,
      processedDateData:
        scheduleData[moment(new Date()).format("YYYY-MM-DD")] || {},
    });
  };
  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="filter">
        <label htmlFor="dropDownSelect">{label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((person) => (
              <option key={person.item} value={person.item}>
                {person.item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  processScheduleData = (unfilteredScheduleData) => {
    var dataMapped = {};
    unfilteredScheduleData.forEach((item) => {
      var itemDate = moment(item.Date).format("YYYY-MM-DD");
      if (!dataMapped.hasOwnProperty(itemDate)) {
        dataMapped[itemDate] = [];
      }
      item["StartTime"] = moment(item.newStartTime);
      item["EndTime"] = moment(item.newEndTime);
      dataMapped[itemDate].push(item);
    });
    // all the keys in dataMapped are the dates in yyyy-mm-dd format.

    var dataMappedMain = {};
    Object.keys(dataMapped).forEach((item) => {
      dataMappedMain[item] = {};
      for (let i = 6; i < 22; i++) {
        dataMappedMain[item][i] = [];
      }
      dataMapped[item].forEach((data) => {
        let startTime = moment(data.StartTime);
        let endTime = moment(data.EndTime);
        let minutes = moment.duration(endTime.diff(startTime)).asMinutes();
        dataMappedMain[item][startTime.hour()].push({
          ...data,
          top: startTime.minutes(),
          height: minutes,
        });
      });
    });

    /* corresponding to each data there is an hour and corresponding to every hour there is an array.
    {
    '2021-04-01':{
        8: [],
        9: []
      }
    }*/

    Object.keys(dataMappedMain).forEach((item) => {
      Object.keys(dataMappedMain[item]).forEach((data) => {
        if (dataMappedMain[item][data].length === 0) {
          dataMappedMain[item][data].push({ height: 0, top: 0 });
        }
      });
    });
    return dataMappedMain;
  };

  componentDidMount = () => {
    let partialAdmissionsYear = [];

    //filtering the admission array to have unique admission year also.
    let tempAdmissionsYear = this.props.admissions.filter((item) => {
      if (!partialAdmissionsYear.includes(item.admissionYear)) {
        partialAdmissionsYear.push(item.admissionYear);
        return true;
      } else {
        return false;
      }
    });
    this.setState({
      locations: this.props.scheduleData.Locations,
      profNames: this.props.scheduleData.ProfNames,
      scheduleData: [],
      admissions: tempAdmissionsYear,
      admissionsDept: this.props.admissions,
      departments: this.props.departments,
      classesSemester: this.props.classes, //semester
      classesDivision: this.props.classes, // division
    });
  };

  componentDidUpdate = (previousProps) => {
    if (previousProps.admissions !== this.props.admissions) {
      let partialAdmissionsYear = [];
      let tempAdmissionsYear = this.props.admissions.filter((item) => {
        if (!partialAdmissionsYear.includes(item.admissionYear)) {
          partialAdmissionsYear.push(item.admissionYear);
          return true;
        } else {
          return false;
        }
      });
      this.setState({
        admissions: tempAdmissionsYear,
        admissionsDept: this.props.admissions,
      });
    }
    if (previousProps.departments !== this.props.departments) {
      this.setState({
        departments: this.props.departments,
      });
    }
    if (previousProps.classes !== this.props.classes) {
      this.setState({
        classesSemester: this.props.classes, //semester
        classesDivision: this.props.classes, // division
      });
    }
    if (previousProps.scheduleData !== this.props.scheduleData) {
      this.setState({
        locations: this.props.scheduleData.Locations,
        profNames: this.props.scheduleData.ProfNames,
      });
    }
  };

  onScheduleClick = (values) => {
    //console.log("this is for intergation of tasks", values);
    this.setState({ taskHidden: true });
    setTimeout(() => {
      this.setState({ taskHidden: false, taskDetails: values });
    }, 0);
  };

  renderClass = (item, top, height, values) => {
    let subject = "",
      startTime = "",
      endTime = "",
      minutes = "",
      location = "",
      profName = "";
    if (height !== 0) {
      subject = values.SubjName;
      startTime = moment(values.newStartTime).format("h:mm");
      endTime = moment(values.newEndTime).format("h:mm");
      minutes = moment
        .duration(moment(values.newEndTime).diff(moment(values.newStartTime)))
        .asMinutes();
      location = values.Location;
      profName = values.ProfName;
    }
    return (
      <div
        onClick={() => this.onScheduleClick(values)}
        className="hidden"
        style={{ display: height ? "" : "none" }}
        className="row timeline-row"
      >
        <div className="col-md-12 timeline-pad" style={{ top: top + "px" }}>
          <div
            className="timeline-inner"
            id={item % 2 ? "cat-2" : "cat-1"}
            style={{ height: height - 1 + "px" }}
          >
            <div className="t-block">
              <p>
                <strong>{subject}</strong>
              </p>
              <p>
                {startTime} - {endTime} ({minutes} minutes)
              </p>
            </div>
            <div className="t-block">
              <p>
                <small>{location}</small>
              </p>
              <p>{profName}</p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  renderLi = (item) => {
    return (
      <li>
        <span></span>
        {item.data.map((values) =>
          this.renderClass(item.item, values.top, values.height, values)
        )}
        <div className="time">
          <span>{item.item}</span>
        </div>
      </li>
    );
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  onChange = (e) => {
    let value = moment(new Date(e)).format("YYYY-MM-DD");
    this.setState({
      taskHidden: true,
      selectedDate: value,
      processedDateData: this.state.scheduleData[value] || {},
    });
  };

  renderList = () => {
    let dateList = this.state.processedDateData;
    let newList = [];
    Object.keys(dateList).forEach((item) => {
      newList.push({ item, data: dateList[item] });
    });

    // let list = [
    //   {
    //     item: 7,
    //     data: [
    //       { top: 0, height: 30 },
    //       { top: 30, height: 30 },
    //     ],
    //   },
    //   { item: 8, data: [{ top: 0, height: 0 }] },
    //   { item: 9, data: [{ top: 30, height: 60 }] },
    //   { item: 10, data: [{ top: 30, height: 150 }] },
    //   { item: 11, data: [{ top: 30, height: 0 }] },
    //   { item: 12, data: [{ top: 30, height: 0 }] },
    //   { item: 13, data: [{ top: 30, height: 60 }] },
    //   { item: 14, data: [{ top: 0, height: 0 }] },
    //   { item: 15, data: [{ top: 30, height: 60 }] },
    //   { item: 16, data: [{ top: 30, height: 60 }] },
    // ];
    return (
      <ul>
        {newList.map((item) => {
          return this.renderLi(item);
        })}
      </ul>
    );
  };

  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };

  onSubmit = async (formValues) => {
    console.log("formValues", formValues);
    this.props.removeAlert();
    this.setState({ isSpinner: true });
    let classId = this.state.classesDivision.filter((item) => {
      return (
        item.division === formValues.division &&
        item.semester === formValues.semester &&
        item.admissionCode === this.state.admissionCode
      );
    });
    console.log("classId", classId);
    try {
      if (classId.length === 0) {
        console.log("error");
        await new Promise((res, reject) => reject("Class was not found"));
      }

      let data = await this.props.loadClassData({
        UniCode: this.props.UniCode,
        classId: classId[0].idClass,
      });
      let filteredScheduleData = this.processScheduleData(data);
      this.setState({
        taskHidden: true,
        division: formValues.division, // this value is being set last after the form is submiited
        isSpinner: false,
        scheduleData: filteredScheduleData,
        selectedDate: moment(new Date()).format("YYYY-MM-DD"),
        processedDateData:
          filteredScheduleData[moment(new Date()).format("YYYY-MM-DD")] || {},
      });
    } catch (e) {
      this.props.alertAction(e);
      setTimeout(() => {
        this.props.removeAlert();
      }, 5000);
      this.setState({
        isSpinner: false,
        scheduleData: [],
        selectedDate: moment(new Date()).format("YYYY-MM-DD"),
        processedDateData: {},
      });
    }

    //
  };

  incrementDate = () => {
    let value = this.state.selectedDate;
    let currentMomentDate = new moment(new Date(value));
    let incrementedDate = currentMomentDate.add(1, "days");

    this.setState({
      taskHidden: true,
      selectedDate: moment(incrementedDate).format("YYYY-MM-DD"),
      processedDateData:
        this.state.scheduleData[moment(incrementedDate).format("YYYY-MM-DD")] ||
        {},
    });
  };

  decrementDate = () => {
    let value = this.state.selectedDate;
    let currentMomentDate = new moment(new Date(value));
    let decrementedDate = currentMomentDate.subtract(1, "days");

    this.setState({
      taskHidden: true,
      selectedDate: moment(decrementedDate).format("YYYY-MM-DD"),
      processedDateData:
        this.state.scheduleData[moment(decrementedDate).format("YYYY-MM-DD")] ||
        {},
    });
  };

  renderTask = () => {
    return (
      <div
        style={{ display: this.state.taskHidden ? "none" : "" }}
        className="col-lg-3 task-wrap bg-white"
      >
        <div className="task-heading pt-2">
          <h4>Task Details</h4>
          <span>Edit/Delete</span>
        </div>

        <div className="row task-card">
          <div className="task-inner">
            <div className="task-head" style={{ background: "#ffecc9" }}>
              <small>Class Details</small>
            </div>
          </div>

          <div className="col-12 ">
            <div className="time">
              <div>
                <p>
                  <b>Admission Year </b> {this.state.admissionYear}
                </p>
              </div>
              <div>
                <p>
                  <b>Semester </b> {this.state.semester}
                </p>
              </div>
            </div>
            <div className="time">
              <div>
                <p>
                  <b>Department </b> {this.state.departmentName}
                </p>
              </div>
              <div>
                <p>
                  <b>Division </b> {this.state.division}
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="row task-card">
          <div className="task-inner">
            <div className="task-head" style={{ background: "#ffecc9" }}>
              <small>Timing Details</small>
            </div>
          </div>

          <div className="col-12 ">
            <form>
              <div className="time">
                <div>
                  <p>
                    <b>Admission Year </b> {this.state.admissionYear}
                  </p>
                </div>
              </div>
              <div className="time">
                <div>
                  <p>
                    <b>Admission Year </b> {this.state.admissionYear}
                  </p>
                </div>
              </div>
              <div className="time">
                <div>
                  <p>
                    <b>Admission Year </b> {this.state.admissionYear}
                  </p>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  };

  locationSubmit = async (event) => {
    event.preventDefault();
    console.log("locationSubmit", event.target[0].value, this.state.isSpinner);

    this.setState({
      isSpinner: true,
    });

    setTimeout(() => {
      this.setState({
        isSpinner: false,
      });
    }, 50000);
  };

  professorName = (event) => {
    event.preventDefault();
    console.log("professorSubmit", event.target[0].value);
    this.setState({
      isSpinner: true,
    });

    setTimeout(() => {
      this.setState({
        isSpinner: false,
      });
    }, 5);
  };

  optionProfessorCheck = (event) => {
    console.log("optionProfessorCheck", event.target.value);
  };

  optionLocationsCheck = (event) => {
    console.log("optionLocationsCheck", event.target.value);
    this.setState({
      isSpinner: true,
    });

    setTimeout(() => {
      this.setState({
        isSpinner: false,
      });
    }, 5000);
  };

  optionCheck = (event) => {
    console.log("event optionCheck", event.target.value);
    this.setState({
      defaultFilter: event.target.value,
      scheduleData: {},
      processedDateData: {},
    });
  };

  render = () => {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <Draggable>
            <div
              tabIndex="-1"
              style={{
                paddingLeft: 200,
                paddingTop: 200,
                width: 250,
              }}
            >
              <MyProfile
                myProfile={this.props.userData}
                removeMyProfile={this.removeMyProfile}
              />
            </div>
          </Draggable>
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">Schedule</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img src="../image/avt1.jpg" width="50px" height="50px" alt="" />
            <div>
              <h6>{name}</h6>
              <small>Admin</small>
            </div>
          </div>
        </header>

        <main>
          <div>
            <div className="filter-wrapper">
              <label htmlFor="dropDownSelect">Schedule Filter</label>
              <div>
                <select onChange={this.optionCheck}>
                  {this.state.people.map((person) => (
                    <option key={person.item} value={person.item}>
                      {person.item}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            {this.state.defaultFilter === "Class" ? (
              <form
                className="filter-wrapper"
                onSubmit={this.props.handleSubmit(this.onSubmit)}
              >
                <Field
                  disabled={false}
                  label="Admission Year"
                  name="admissionYear"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.admissions.map(function (item) {
                    return { item: item.admissionYear };
                  })}
                />
                <Field
                  disabled={this.state.disableDepartment}
                  label="Department Name"
                  name="departmentName"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.partialDepartments.map(function (item) {
                    return { item: item.departmentName };
                  })}
                />
                <Field
                  disabled={this.state.disableSemester}
                  label="Semester"
                  name="semester"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.partialSemester.map(function (item) {
                    return { item: item.semester };
                  })}
                />
                <Field
                  disabled={this.state.disableDivision}
                  label="Division"
                  name="division"
                  onChange={this.onChangeCheck}
                  component={this.renderInput}
                  people={this.state.partialDivision.map(function (item) {
                    return { item: item.division };
                  })}
                />
                <div className="filter fil-btn">
                  <label></label>
                  <button
                    type="submit"
                    className="btn btn-primary mb-2"
                    style={{ backgroundColor: "#4cadad" }}
                  >
                    GO
                  </button>
                  <AlertComponent />
                </div>
                {this.state.isSpinner ? (
                  <div className="ui active inverted dimmer">
                    <div className="ui loader"></div>
                  </div>
                ) : (
                  <React.Fragment />
                )}
              </form>
            ) : (
              <React.Fragment />
            )}

            {this.state.defaultFilter === "Professor" ? (
              <ProfessorSubmitTask
                renderScheduleData={this.renderScheduleData}
              />
            ) : (
              <React.Fragment />
            )}

            {this.state.defaultFilter === "Location" ? (
              <div>
                <form onSubmit={this.locationSubmit}>
                  <label htmlFor="dropDownSelect">
                    Select Location
                    <div>
                      <select onChange={this.optionLocationsCheck}>
                        {this.state.locations.map((person) => (
                          <option key={person} value={person}>
                            {person}
                          </option>
                        ))}
                      </select>
                    </div>
                  </label>
                  <div>
                    <input type="submit" />
                  </div>
                </form>
              </div>
            ) : (
              <React.Fragment />
            )}
          </div>

          <div className="schedule-content p-3">
            <div className="row mt-4">
              <div className="col-lg-3 ">
                <div className="row">
                  <div className="col-12">
                    <div className="calendar" style={{ borderRadius: "5px" }}>
                      <div className="datepicker">
                        <div className="cal-inner-wrapper">
                          <Calendar
                            value={this.state.value}
                            onChange={this.onChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 mt-5">
                    <div className="category-wrapper">
                      <div className="category">
                        <h4>Category</h4>
                      </div>
                      <div
                        className="search-wrapper mt-4"
                        style={{ paddingLeft: "15px", borderRadius: "10px" }}
                      >
                        <input type="search" placeholder="Search" />
                      </div>

                      <div className="category-list">
                        <ul>
                          <li>
                            <label className="container">
                              Select All
                              <input type="checkbox" />
                              <span className="checkmark"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Batch
                              <input type="checkbox" />
                              <span className="checkmark " id="cat-1"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              {" "}
                              Course
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-2"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              ClassName
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-3"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Division
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-4"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Teaching Staff
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-5"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Type of ClassName
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-6"></span>
                            </label>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className={this.state.taskHidden ? "col-lg-9" : "col-lg-6"}>
                <div
                  className="timeline bg-white"
                  style={{ borderRadius: "5px" }}
                >
                  <div className="timeline-head">
                    <div className="timeline-heading">
                      <h4>
                        {moment(this.state.selectedDate).format("MMMM Do YYYY")}
                      </h4>
                    </div>
                    <div className="date-inc">
                      <span>
                        <button onClick={this.decrementDate}>
                          <img src="../image/1.png" alt="" />
                        </button>
                      </span>
                      <span>
                        <button onClick={this.incrementDate}>
                          <img src="../image/2.png" alt="" />
                        </button>
                      </span>
                    </div>
                  </div>
                  {this.renderList()}
                </div>
              </div>
              {this.state.taskHidden ? (
                <React.Fragment />
              ) : (
                <TaskMain
                  taskHidden={this.state.taskHidden}
                  admissionYear={this.state.admissionYear}
                  departmentName={this.state.departmentName}
                  division={this.state.division}
                  semester={this.state.semester}
                  taskDetails={this.state.taskDetails}
                />
              )}
            </div>
          </div>
        </main>
      </div>
    );
  };
}

const formWrapped = reduxForm({
  form: "Display Schedule",
  validate,
})(Schedule);

const mapStateToProps = (state) => {
  return {
    scheduleData: state.scheduleDataInformation,
    userData: state.userData,
    classes: state.classes,
    admissions: state.admissions,
    departments: state.departments,
    UniCode: state.userData.UniCode,
  };
};

export default connect(mapStateToProps, {
  loadClassData,
  alertAction,
  removeAlert,
})(formWrapped);
