import React, { useState } from "react";
import MyProfile from "./popups/MyProfile2";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Modal } from "semantic-ui-react";
import Draggable from "react-draggable";
import { Field, reduxForm } from "redux-form";
import { createClass } from "../../actions/appSyncActions";
import { alertAction } from "../../actions/authActions";
import AlertComponent from "../auth/AlertComponent";
import UploadClassData from "./Class/UploadClass";

const validate = (formValues) => {
  let error = {};
  if (!formValues.admissionCode) {
    error.admissionCode = "Please provide an AdmissionCode";
  }
  if (!formValues.semister) {
    error.semister = "Please provide an Semistername";
  }
  if (!formValues.division) {
    error.division = "Please provide an Division";
  }
  if (!formValues.subYear) {
    error.subYear = "Please provide an SubYear";
  }
  if (!formValues.quater) {
    error.quater = "Please provide an Quater";
  } //subdivision
  if (!formValues.subdivision) {
    error.subdivision = "Please provide an subdivision";
  } //subdivision
  return error;
};

class AddClass extends React.Component {
  //const [uploadData, setUploadData] = useState(false);
  state = {
    admissions: this.props.admissions,
    disableSemister: true,
    semister: [],
    uploadData: false,
  };
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  onChangeCheck = (e) => {
    //console.log("onChangeCheck", e, e.target.name, e.target.value);
    if (e.target.name === "subYear") {
      if (e.target.value === "1") {
        this.setState({
          disableSemister: false,
          semister: [{ item: 1 }, { item: 2 }],
        });
      }
      if (e.target.value === "2") {
        this.setState({
          disableSemister: false,
          semister: [{ item: 3 }, { item: 4 }],
        });
      }
      if (e.target.value === "3") {
        this.setState({
          disableSemister: false,
          semister: [{ item: 5 }, { item: 6 }],
        });
      }
      if (e.target.value === "4") {
        this.setState({
          disableSemister: false,
          semister: [{ item: 7 }, { item: 8 }],
        });
      }
    }
  };

  renderInput = ({
    input,
    placeholder,
    meta,
    type,
    label,
    people,
    disabled,
  }) => {
    return (
      <div className="form-group">
        <label htmlFor="dropDownSelect">Select a {label}</label>
        <div>
          <select disabled={disabled} {...input}>
            <option value="">Select</option>
            {people.map((person) => (
              <option key={person.item} value={person.item}>
                {person.item}
              </option>
            ))}
          </select>
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderSubdivision = ({
    input,
    placeholder,
    meta,
    type,
    label,
    iconClass,
  }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <input
            className="form-control"
            name="code"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = async (formValues) => {
    console.log("formValues", formValues);
    let classSubmissionData = {
      admissionCode: formValues.admissionCode,
      subYear: formValues.subYear,
      division: formValues.division,
      semester: formValues.semister,
      quarter: null,
      subDivision: formValues.subdivision,
    };
    try {
      await this.props.createClass(classSubmissionData, this.props.classes);
      this.props.removeClassPopUpBox();
    } catch (e) {
      this.props.alertAction(e);
    }
  };
  render() {
    return (
      <React.Fragment>
        <Modal.Header>Class Creation</Modal.Header>
        <Modal.Content>
          <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
            <Field
              name="admissionCode"
              label="AdmissionCode"
              component={this.renderInput}
              people={this.state.admissions.map(function (item) {
                return { item: item.admissionCode };
              })}
            />
            <Field
              label="SubYear"
              name="subYear"
              component={this.renderInput}
              people={[{ item: 1 }, { item: 2 }, { item: 3 }, { item: 4 }]}
              onChange={this.onChangeCheck}
            />
            <Field
              disabled={this.state.disableSemister}
              label="Semister Name"
              name="semister"
              component={this.renderInput}
              people={this.state.semister}
            />
            <Field
              label="Division"
              name="division"
              component={this.renderInput}
              people={[
                { item: "A" },
                { item: "B" },
                { item: "C" },
                { item: "D" },
              ]}
            />
            {/* <Field
                label="Quater"
                name="quater"
                component={this.renderInput}
                people={[{ item: 1 }, { item: 2 }, { item: 3 }, { item: 4 }]}
              /> */}
            <Field
              label="Sub Division"
              type="text"
              name="subdivision"
              component={this.renderSubdivision}
              placeholder="Sub Division"
            />
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              type="submit"
            >
              Submit
            </button>
            <AlertComponent />
          </form>
        </Modal.Content>
      </React.Fragment>
    );
  }
}

const formWrapped = reduxForm({
  form: "create Students",
  validate,
})(AddClass);

const mapStateToPropsRedux = (state) => {
  return { admissions: state.admissions, classes: state.classes };
};
const AddClassesRedux = connect(mapStateToPropsRedux, {
  createClass,
  alertAction,
})(formWrapped);

class CreateClass extends React.Component {
  state = {
    myProfile: false,
    addClasses: false,
    classes: this.props.classes,
  };

  removeClassPopUpBox = () => {
    this.setState({ addClasses: false });
  };
  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  renderClasses() {
    var classes = this.state.classes;
    return classes.map((item) => {
      let {
        idClass,
        admissionCode,
        subYear,
        division,
        semester,
        subDivision,
      } = item;
      return (
        <tr key={idClass}>
          <td>{admissionCode}</td>
          <td>{subYear}</td>
          <td>{division}</td>
          <td>{semester}</td>
          <td>{subDivision}</td>
        </tr>
      );
    });
  }
  render() {
    if (this.props.isAuthenticated) {
      let data = this.props.userData;
      let name = data["UserName"];
      return (
        <div className="main-content bg-light">
          <Modal
            open={this.state.myProfile}
            onClose={() => this.setState({ myProfile: false })}
            style={{ overflow: "auto", maxHeight: 600 }}
            dimmer={"inverted"}
          >
            <MyProfile
              myProfile={data}
              removeMyProfile={this.removeMyProfile}
            />
          </Modal>
          <header>
            {this.state.uploadData ? (
              <UploadClassData
                open={this.state.uploadData}
                onClose={() => this.setState({ uploadData: false })}
              />
            ) : (
              <React.Fragment />
            )}
            <h4>
              <label htmlFor="nav-toggel">
                <span>
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </span>
              </label>
              <span className="name">Create Class</span>
            </h4>

            <div className="user-wrapper" onClick={this.myProfile}>
              <img
                src={this.props.profileImageUrl}
                width="50px"
                height="50px"
                alt=""
              />
              <div>
                <h6>{name}</h6>
                <small>Admin</small>
              </div>
            </div>
          </header>
          <main>
            <Modal
              open={this.state.addClasses}
              onClose={() => this.setState({ addClasses: false })}
              style={{ overflow: "auto", maxHeight: 600 }}
              dimmer={"inverted"}
            >
              <AddClassesRedux removeClassPopUpBox={this.removeClassPopUpBox} />
            </Modal>
            <div className="table-content m-3">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Admission Code</th>
                    <th scope="col">SubYear</th>
                    <th scope="col">Division</th>
                    <th scope="col">Semester</th>
                    <th scope="col">SubDivision</th>
                  </tr>
                </thead>
                <tbody>{this.renderClasses()}</tbody>
              </table>
            </div>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ addClasses: true })}
            >
              Add Class
            </button>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ uploadData: true })}
            >
              Upload Data
            </button>
          </main>
        </div>
      );
    } else {
      return <Redirect push to="/" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    classes: state.classes,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, null)(CreateClass);
