import React, { useState } from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField, Header } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import MyProfile from "./popups/MyProfile2";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { createDepartment } from "../../actions/appSyncActions";
import { Link } from "react-router-dom";
const { Storage } = require("aws-amplify");

const AddDepartmentsRedux = function (props) {
  const [error, setError] = useState("");
  // console.log("departments", props.departments);
  // const departments = props.departments.map((item) => item.departmentName);
  const departments = useSelector((state) => state.departments);
  const UniCode = useSelector((state) => state.userData.UniCode);
  console.log("departments", departments, UniCode);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    let error = {};
    if (!formValues.departmentName) {
      error.departmentName = "* Please provide an Department Name";
    }
    if (formValues.departmentName) {
      if (departments.includes(formValues.departmentName)) {
        error.departmentName = "* Department Exists";
      }
    }
    if (!formValues.departmentHead) {
      error.departmentHead = "* Please provide an Department Head";
    }
    return error;
  };
  const onSubmit = async (formValues) => {
    console.log("formValues", formValues);
    try {
      await dispatch(
        createDepartment(
          {
            departmentName: formValues.departmentName,
            universityCode: props.UniCode,
          },
          props.departments
        )
      );
      props.removeDepartmentPopUpBox();
    } catch (e) {
      setError(JSON.stringify(e));
      setTimeout(() => {
        props.removeDepartmentPopUpBox();
      }, 20000);
    }
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      <Modal.Header>Add Department</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{ departmentName: "" }}
          validate={validate}
          onSubmit={onSubmit}
        >
          <Form>
            <FormField>
              <label htmlFor="deptId">Department Name</label>
              <Field id="deptId" name="departmentName" />
              <ErrorMessage name="departmentName" />
            </FormField>
            <FormField>
              <label htmlFor="depthead">Department Head</label>
              <Field id="depthead" name="departmentHead" />
              <ErrorMessage name="departmentHead" />
            </FormField>
            <Button type="submit">Submit</Button>
          </Form>
        </Formik>
      </Modal.Content>
      <Modal.Actions>
        {error ? <p>{error}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};

// const mapStateToPropsDepartments = (state) => {
//   return {
//     UniCode: state.userData.UniCode,
//     departments: state.departments,
//   };
// };

// const AddDepartmentsRedux = connect(mapStateToPropsDepartments, {
//   createDepartment,
// })(AddDepartment);

const UploadDepartmentData = function (props) {
  const [downloadTemplate, setDownloadTemplate] = useState(true);
  const [uploadData, setUploadData] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const path = "uploads/TR021/Departments/addDeparments.csv";
  const handleChange = async (e, path) => {
    const file = e.target.files[0];
    console.log("file", file);
    if (file.size > 1048576) {
      console.log("file.size");
      alert("Please image size of less than 1 MB");
    } else {
      setSpinner(true);
      try {
        const data = await Storage.put(path, file, {
          contentType: "text/csv",
        });
        console.log("uploadImage", data);
        this.props.downLoadImage(path);
        setSpinner(false);
        // Retrieve the uploaded file to display
      } catch (err) {
        setSpinner(false);
      }
    }
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
    >
      <Modal.Header>
        <Button
          onClick={() => {
            setUploadData(false);
            setDownloadTemplate(true);
          }}
        >
          DownLoadTemplete
        </Button>
        <Button
          onClick={() => {
            setUploadData(true);
            setDownloadTemplate(false);
          }}
        >
          Upload csv
        </Button>
      </Modal.Header>
      <Modal.Content>
        {downloadTemplate ? (
          <Link to="./csv/DepartmentsTemplate.csv" target="_blank" download>
            Download
          </Link>
        ) : (
          <React.Fragment />
        )}
        {uploadData ? (
          <div>
            {spinner ? (
              <div className="ui active inverted dimmer">
                <div className="ui loader"></div>
              </div>
            ) : (
              <React.Fragment />
            )}
            <input
              type="file"
              accept="csv"
              onChange={(evt) => handleChange(evt, path)}
            />
            Upload File (less than 1MB)
          </div>
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
    </Modal>
  );
};

class CreateDepartments extends React.Component {
  state = {
    myProfile: false,
    addDepartments: false,
    departments: [],
    uploadData: false,
  };
  componentDidMount = () => {
    this.setState({ departments: this.props.departments });
  };
  componentDidUpdate = (previousProps) => {
    if (previousProps.departments !== this.props.departments) {
      this.setState({ departments: this.props.departments });
    }
  };
  removeDepartmentPopUpBox = () => {
    this.setState({ addDepartments: false });
  };
  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };
  removeMyProfile = () => {
    this.setState({ myProfile: false });
  };
  renderDepartments() {
    var departments = this.state.departments;
    return departments.map((item) => {
      let { departmentCode, departmentName } = item;
      return (
        <tr key={departmentCode}>
          <td>{departmentCode}</td>
          <td>{departmentName}</td>
        </tr>
      );
    });
  }
  render() {
    if (this.props.isAuthenticated) {
      let data = this.props.userData;
      let name = data["UserName"];
      return (
        <div className="main-content bg-light">
          <Modal
            open={this.state.myProfile}
            onClose={() => this.setState({ myProfile: false })}
            style={{ overflow: "auto", maxHeight: 600 }}
            dimmer={"inverted"}
          >
            <MyProfile
              myProfile={data}
              removeMyProfile={this.removeMyProfile}
            />
          </Modal>
          <UploadDepartmentData
            open={this.state.uploadData}
            onClose={() => this.setState({ uploadData: false })}
          />

          <header>
            <h4>
              <label htmlFor="nav-toggel">
                <span>
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </span>
              </label>
              <span className="name">Create Departments</span>
            </h4>

            <div className="user-wrapper" onClick={this.myProfile}>
              <img
                src={this.props.profileImageUrl}
                width="50px"
                height="50px"
                alt=""
              />
              <div>
                <h6>{name}</h6>
                <small>Admin</small>
              </div>
            </div>
          </header>
          <main>
            {this.state.addDepartments ? (
              <AddDepartmentsRedux
                open={this.state.addDepartments}
                onClose={() => this.setState({ addDepartments: false })}
                removeDepartmentPopUpBox={this.removeDepartmentPopUpBox}
              />
            ) : (
              <React.Fragment />
            )}

            <div className="table-content m-3">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Department Code</th>
                    <th scope="col">Department Name</th>
                  </tr>
                </thead>
                <tbody>{this.renderDepartments()}</tbody>
              </table>
            </div>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ addDepartments: true })}
            >
              Add Department
            </button>
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => this.setState({ uploadData: true })}
            >
              Upload Data
            </button>
            {/* <Link to="./csv/DepartmentsTemplate.csv" target="_blank" download>
              Download
            </Link> */}
          </main>
        </div>
      );
    } else {
      return <Redirect push to="/" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    departments: state.departments,
    userData: state.userData,
    profileImageUrl: state.uploadedImage.data,
  };
};
export default connect(mapStateToProps, null)(CreateDepartments);
