import { Switch, Route } from "react-router-dom";
import React from "react";
import { connect } from "react-redux";
//import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";
import "../css/student.css";
import { logOutAction, removeAlert } from "../../actions/authActions";
// import Modal from "@material-ui/core/Modal";
// import Draggable from "react-draggable";
// import Myprofile from "./myProfile";
// import { API, graphqlOperation } from "aws-amplify";

import Schedule from "./schedule/Schedule";
import Notification from "./Notification";
import Attendance from "./Attendance";
import Contacts from "./Contacts/Contacts";
import ExamResult from "./ExamResult";
import StudentData from "./StudentData";
import TeachingStaffData from "./professors/TeachingStaffData";
import AluminiData from "./AluminiData";
import Help from "./Help/Help";
import StudentCorner from "./StudentCorner";
import CreateDepartments from "./createDepartments";
import CreateBatch from "./Batch/createBatch";
import CreateClass from "./createClass";
import CreateLocation from "./Locations/Locations";
import CreateSubject from "./Subjects/Subjects";
import Results from "./Results/Results";

class Welcome extends React.Component {
  state = {
    schedule: "name",
    attendance: "",
    examresult: "",
    studentData: "",
    teachingstaffdata: "",
    studentcorner: "",
    aluminidata: "",
    help: "",
    contacts: "",
    notification: "",
    logout: "",
    createDepartments: "",
    createBatch: "",
    createClass: "",
    createSubject: "",
    createLocation: "",
  };
  someFunct = (name) => {
    this.props.removeAlert();
    this.setState({
      schedule: "",
      attendance: "",
      examresult: "",
      studentData: "",
      teachingstaffdata: "",
      studentcorner: "",
      aluminidata: "",
      help: "",
      contacts: "",
      notification: "",
      logout: "",
      createDepartments: "",
      createBatch: "",
      createClass: "",
      createSubject: "",
      createLocation: "",
      [name]: "name",
    });
    if (name === "logout") {
      this.props.logOutAction();
    }
  };
  render() {
    console.log("Welcome render");
    return (
      <div className="body">
        <input type="checkbox" id="nav-toggel" />
        <div className="sidebar">
          <div className="sidebar-brand">
            <h3>
              <img src="../image/campus-favicon.png" alt="" />
              <span>Campus Live</span>
            </h3>
          </div>
          <div className="sidebar-menu">
            <ul>
              <li>
                <Link
                  to="/welcome"
                  className={this.state.schedule === "name" ? "active" : ""}
                  onClick={() => this.someFunct("schedule")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Schedule</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/attendance"
                  className={this.state.attendance === "name" ? "active" : ""}
                  onClick={() => this.someFunct("attendance")}
                >
                  <span>
                    <img
                      src="../image/attendance.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Attendance</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/examresult"
                  className={this.state.examresult === "name" ? "active" : ""}
                  onClick={() => this.someFunct("examresult")}
                >
                  <span>
                    <img
                      src="../image/exam1.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Exam Result </span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome"
                  className={this.state.notification === "name" ? "active" : ""}
                  onClick={() => this.someFunct("notification")}
                >
                  <span>
                    <img
                      src="../image/notification.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Notification</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/studentData"
                  className={this.state.studentData === "name" ? "active" : ""}
                  onClick={() => this.someFunct("studentData")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Student Data</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/teachingstaffdata"
                  className={
                    this.state.teachingstaffdata === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("teachingstaffdata")}
                >
                  <span>
                    <img
                      src="../image/teacher.png"
                      width="25px"
                      height="25px"
                      alt=""
                    />
                  </span>
                  <span>Teaching Staff Data</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/studentcorner"
                  className={
                    this.state.studentcorner === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("studentcorner")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Student Corner</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/aluminidata"
                  className={this.state.aluminidata === "name" ? "active" : ""}
                  onClick={() => this.someFunct("aluminidata")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      alt=""
                      style={{ width: "20px", height: "20px" }}
                    />
                  </span>
                  <span>Alumni Data</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/help"
                  className={this.state.help === "name" ? "active" : ""}
                  onClick={() => this.someFunct("help")}
                >
                  <span>
                    <img
                      src="../image/help.png"
                      style={{ width: "25px", height: "25px" }}
                      alt=""
                    />
                  </span>
                  <span>Help/FAQ</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/contacts"
                  className={this.state.contacts === "name" ? "active" : ""}
                  onClick={() => this.someFunct("contacts")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Contacts</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/createDepartments"
                  className={
                    this.state.createDepartments === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("createDepartments")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Create Departments</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/createBatch"
                  className={this.state.createBatch === "name" ? "active" : ""}
                  onClick={() => this.someFunct("createBatch")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Create Batch</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/createClass"
                  className={this.state.createClass === "name" ? "active" : ""}
                  onClick={() => this.someFunct("createClass")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Create Class</span>
                </Link>
              </li>

              <li>
                <Link
                  to="/welcome/createLocation"
                  className={
                    this.state.createLocation === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("createLocation")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Create Location</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/createSubject"
                  className={
                    this.state.createSubject === "name" ? "active" : ""
                  }
                  onClick={() => this.someFunct("createSubject")}
                >
                  <span>
                    <img
                      src="../image/schedule.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Create Subject</span>
                </Link>
              </li>
              <li>
                <Link
                  to="/welcome/contacts"
                  className={this.state.logout === "name" ? "active" : ""}
                  onClick={() => this.someFunct("logout")}
                >
                  <span>
                    <img
                      src="../image/student.png"
                      width="20px"
                      height="20px"
                      alt=""
                    />
                  </span>
                  <span>Logout</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>

        <Switch>
          <Route path="/welcome/notification" exact component={Notification} />
          <Route path="/welcome" exact component={Schedule} />
          <Route path="/welcome/attendance" exact component={Attendance} />
          <Route path="/welcome/studentData" exact component={StudentData} />
          <Route path="/welcome/contacts" exact component={Contacts} />
          <Route path="/welcome/examresult" exact component={Results} />
          <Route
            path="/welcome/teachingstaffdata"
            exact
            component={TeachingStaffData}
          />
          <Route path="/welcome/aluminidata" exact component={AluminiData} />
          <Route path="/welcome/help" exact component={Help} />
          <Route
            path="/welcome/studentcorner"
            exact
            component={StudentCorner}
          />
          <Route
            path="/welcome/createDepartments"
            exact
            component={CreateDepartments}
          />
          <Route path="/welcome/createBatch" exact component={CreateBatch} />
          <Route path="/welcome/createClass" exact component={CreateClass} />
          <Route
            path="/welcome/createSubject"
            exact
            component={CreateSubject}
          />
          <Route
            path="/welcome/createLocation"
            exact
            component={CreateLocation}
          />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};
export default connect(mapStateToProps, { logOutAction, removeAlert })(Welcome);
/*
this is the first which gets loaded when we users signs in.
this file is in development.
*/
