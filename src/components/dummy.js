let event = {};
if (event.resource === "/getschedule") {
  let email = event.queryStringParameters.email;
  let universityCode = event.queryStringParameters.universityCode;

  if (email != "" && universityCode != "") {
    try {
      let selectDataUserInfo =
        "Select * from UserInfo where UserEmail=? and UniCode=?";
      let userData = await getOrder(selectDataUserInfo, [
        email,
        universityCode,
      ]);
      console.log("UserCode", userData);
      let UserCode = userData[0].UserCode;
      let userRole = userData[0].UserRole;
      if (userRole === "Student") {
        let selectClassStudent =
          "Select * from ClassStudent where StudentCode=?";
        let classStudentData = await getOrder(selectClassStudent, UserCode);
        console.log("classStudentData", classStudentData);

        //let selectClassScheduleMonthly = 'Select * from ClassScheduleMonthly where ClassId=?';
        let selectClassScheduleMonthly = g;
        ("Select ClassSchMonId,SchCode,ClassId,UniCode,Date,StartTime,EndTime,SubjCode,SubjName,ProfCode,ProfName,Location,Link,LastUpdatedEpochTime,ChangeId,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where ClassId=?");
        let classScheduleData = await getOrder(
          selectClassScheduleMonthly,
          classStudentData[0].ClassId
        );

        console.log("classScheduleData", classScheduleData);
        res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(classScheduleData),
        };
        return res;
      }
      if (userRole === "Professor") {
        let selectClassScheduleMonthly = `Select AI.AdmissionYear,AI.DeptCode,C.Division,C.Semester,CSM.ClassSchMonId,CSM.ClassId,CSM.Date,CSM.SubjCode,CSM.SubjName,CSM.ProfCode,CSM.ProfName,CSM.Location,CSM.Link,CSM.LastUpdatedEpochTime,CSM.ChangeId,
ADDTIME(CSM.Date, CSM.StartTime) as newStartTime,ADDTIME(CSM.Date, CSM.EndTime) as newEndTime 
from ClassScheduleMonthly AS CSM 
JOIN Class AS C
JOIN AdmissionInfo as AI
where CSM.ClassId=C.idClass and  AI.AdmCode=C.AdmCode  and CSM.ProfCode=?`;
        let classScheduleData = await getOrder(
          selectClassScheduleMonthly,
          UserCode
        );

        console.log("classScheduleData", classScheduleData);
        res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(classScheduleData),
        };
        return res;
      }
    } catch (e) {
      let res = {
        statusCode: 500,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({ error: JSON.stringify(e) }),
      };
      return res;
    }
  } else {
    let obj = {
      error: "All keys not found to query the db(UserEmail or UniCode)",
    };
    let res = {
      statusCode: 400,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers":
          "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
        "Access-Control-Allow-Methods": "GET,OPTIONS",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({ error: obj.error }),
    };
    return res;
  }
}
