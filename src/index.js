import React from "react";
import ReactDOM from "react-dom";
import App from "./components";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import reducer from "./reducers";
import "crypto-js/lib-typedarrays";
import Amplify from "aws-amplify";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";

const composeEnhancers = composeWithDevTools({});
Amplify.configure({
  Auth: {
    identityPoolId: "us-east-2:74ab8fa3-1d13-409b-885b-30f03796c657",
    region: "us-east-2",
    userPoolId: "us-east-2_AlfN2jRqp",
    userPoolWebClientId: "14jgvcd8sqmp88l3aet95o1k6l",
    authenticationFlowType: "USER_SRP_AUTH",
  },
  API: {
    endpoints: [
      {
        name: "preAuthAPIMethods",
        endpoint: "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev",
      },
    ],
  },
  Storage: {
    AWSS3: {
      bucket: "innocurveuniversity", //REQUIRED -  Amazon S3 bucket name
      region: "us-east-2", //OPTIONAL -  Amazon service region
    },
  },
  aws_appsync_graphqlEndpoint:
    "https://bucmhxieufhsvc6zrspqnccmf4.appsync-api.us-east-2.amazonaws.com/graphql",
  aws_appsync_region: "us-east-2",
  aws_appsync_authenticationType: "API_KEY",
  aws_appsync_apiKey: "da2-rch7f2xjp5azhhxj7zx3crffze",
});

const middleware = [thunk];
const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(...middleware))
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector("#root")
);

/*
this is the first file which gets rendered when we first load our app,
We also have amplify configure in this file which is basically the bridge between the 
React and AWS, all the Api and Cognito Configuration are being done over here. 
this file renders the App component which contains all the routes being used in React.

  Auth: {
    identityPoolId: "us-east-2:74ab8fa3-1d13-409b-885b-30f03796c657",
    region: "us-east-2",
    userPoolId: "us-east-2_AlfN2jRqp",
    userPoolWebClientId: "14jgvcd8sqmp88l3aet95o1k6l",
    authenticationFlowType: "USER_PASSWORD_AUTH",
  },
*/
