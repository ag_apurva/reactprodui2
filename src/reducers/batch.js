import { createSlice } from "@reduxjs/toolkit";
import { getClasses } from "../actions/scheduleActions/actions";
import axios from "axios";

const batchSlice = createSlice({
  name: "batchReducer",
  initialState: { admissionsData: [], loaderAdmissionData: false }, //{ counter: 0, showCounter: false },
  reducers: {
    admissionsData(state, action) {
      state.admissionsData = [...action.payload];
    },
    addAdmissions(state, action) {
      state.admissionsData.push(action.payload);
    },
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    toogleCounter(state, action) {
      state.showCounter = !state.showCounter;
    },
    increase(state, action) {
      state.counter = state.counter + action.payload;
    },
  },
});

export const batchActions = batchSlice.actions;

export const asyncAdmissionsData = (code) => {
  return async function (dispatch) {
    let config = {
      method: "get",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/admissions/getadmissions",
      headers: {
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      params: {
        universityCode: code,
      },
    };

    try {
      let data = await axios(config);
      console.log("Data Locations", data.data);
      dispatch(batchActions.admissionsData(data.data));
      /* to be removed later */
      dispatch({
        type: "ADMISSION_DATA",
        payload: data.data,
      });
    } catch (e) {
      console.log("error retriving Locations", e);
    }
  };
};

export const createAdmissions = (formValues) => {
  /*

 { admissionYear: "2022", 
 departmentCode: "DP011", 
 degreeOffered: "Mtech",
  academicStartData: "2021-07-23",
   academicEndData: "2026-07-09" } { admissionYear: "2022", departmentCode: "DP011", degreeOffered: "Mtech", academicStartData: "2021-07-23", academicEndData: "2026-07-09" }
  */
  return async function (dispatch) {
    var axios = require("axios");
    var data = JSON.stringify(formValues);

    var config = {
      method: "post",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/admissions/createadmissions",
      headers: {
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      let data = await axios(config);
      console.log("Data Locations", data.data.data);
      /*
      admissionCode, admissionYear, departmentCode
      */
      let AdmCode = data.data.data;
      let admssionData = {
        UniCode: formValues["UniCode"],
        admissionYear: formValues["admissionYear"],
        departmentCode: formValues["departmentCode"],
        DegreeOffered: formValues["degreeOffered"],
        AcadStartDate: formValues["academicStartData"],
        AcadEndDate: formValues["academicEndData"],
        departmentName: formValues["DeptName"],
        admissionCode: AdmCode,
      };
      dispatch(batchActions.addAdmissions(admssionData));
      return new Promise((resolve, reject) => resolve());
      // /* to be removed later */
      // dispatch({
      //   type: "ADMISSION_DATA",
      //   payload: data.data,
      // });
    } catch (e) {
      console.log("error retriving Locations", e);
      return new Promise((resolve, reject) => reject(e));
    }
  };
};

export const createAdmissionsUpload = (formValues) => {
  return async function (dispatch) {
    var axios = require("axios");
    var data = JSON.stringify(formValues);

    var config = {
      method: "post",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/admissions/uploadadmissions",
      headers: {
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      let data = await axios(config);
      if (data.data.hasOwnProperty("errors")) {
        console.log("error retriving Locations", data.data.errors);
        return new Promise((resolve, reject) => reject(data.data.errors));
      }
      console.log("Data Upload", data.data.data);
      dispatch(asyncAdmissionsData(formValues.UniCode));
      return new Promise((resolve, reject) => resolve());
    } catch (e) {
      console.log("error retriving Locations", e);
      return new Promise((resolve, reject) => reject(e));
    }
  };
};

export const createClassesUpload = (formValues) => {
  return async function (dispatch) {
    var axios = require("axios");
    var data = JSON.stringify(formValues);

    var config = {
      method: "post",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/classes/uploadclasses",
      headers: {
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      let data = await axios(config);
      if (data.data.hasOwnProperty("errors")) {
        console.log("error retriving Locations", data.data.errors);
        return new Promise((resolve, reject) => reject(data.data.errors));
      }
      console.log("Data Upload", data.data.data);
      dispatch(getClasses(formValues.UniCode));
      return new Promise((resolve, reject) => resolve());
    } catch (e) {
      console.log("error retriving Locations", e);
      return new Promise((resolve, reject) => reject(e));
    }
  };
};

export default batchSlice;
// const store = configureStore({
//   reducer: { counter: counterSlice.reducer },
// });
