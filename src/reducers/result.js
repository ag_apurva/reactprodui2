import { createSlice } from "@reduxjs/toolkit";
import { getClasses } from "../actions/scheduleActions/actions";
import axios from "axios";

const resultSlice = createSlice({
  name: "resultReducer",
  initialState: { studentResult: [] }, //{ counter: 0, showCounter: false },
  reducers: {
    resultsData(state, action) {
      state.studentResult = action.payload.studentResult;
    },
  },
});

export const resultActions = resultSlice.actions;

export const loadResultForStudent = function (UserCode, UniCode) {
  return async function (dispatch) {
    dispatch(resultActions.resultsData({ studentResult: [] }));
    const config = {
      method: "get",
      url:
        "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev/results/getresult",
      headers: {},
      params: {
        universityCode: UniCode,
        studentUserCode: UserCode,
      },
    };
    try {
      let data = await axios(config);
      //console.log("datagg", data.data);
      dispatch(resultActions.resultsData({ studentResult: data.data }));
    } catch (e) {}
  };
};

export default resultSlice;
// const store = configureStore({
//   reducer: { counter: counterSlice.reducer },
// });
