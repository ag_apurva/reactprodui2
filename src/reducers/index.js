import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import {
  ID_TOKEN_COGNITO,
  ACCESS_TOKEN_COGNITO,
  REFRESH_TOKEN_COGNITO,
} from "../actions/reducerActionsConsts";
import batchReducer from "./batch";
import resultReducer from "./result";
const idToken = function (token = "", action) {
  if (action.type === ID_TOKEN_COGNITO) {
    return action.payload;
  } else {
    return token;
  }
};

const professorsImages = function (images = [], action) {
  if (action.type === "PROFESSORS_IMAGE") {
    return action.payload;
  } else {
    return images;
  }
};

const accessToken = function (token = "", action) {
  if (action.type === ACCESS_TOKEN_COGNITO) {
    return action.payload;
  } else {
    return token;
  }
};

const refreshToken = function (token = "", action) {
  if (action.type === REFRESH_TOKEN_COGNITO) {
    return action.payload;
  } else {
    return token;
  }
};

const isAuthenticated = function (isAuth = false, action) {
  if (action.type === "IS_AUTHENTICATED") {
    return action.payload;
  }
  return isAuth;
};

const userData = function (user = null, action) {
  if (action.type === "LOGIN_SUCCESS") {
    return action.payload;
  }
  return user;
};

const scheduleDataInformation = function (
  data = { Locations: [], Subject: [], ProfNames: [], error: null },
  action
) {
  if (action.type === "SCHEDULE_DATA_UNIVERSITY") {
    return action.payload;
  }
  return data;
};

const anotherUser = function (user = null, action) {
  if (action.type === "ANOTHER_USER") {
    return action.payload;
  }
  return user;
};

const departments = function (departments = [], action) {
  if (action.type === "DEPARTMENTS_DATA") {
    return action.payload;
  } else if (action.type === "ADD_DEPARTMENT") {
    departments.push(action.payload);
    let newDepartments = departments;
    return newDepartments;
  } else {
    return departments;
  }
};

const admissions = function (admission = [], action) {
  if (action.type === "ADMISSION_DATA") {
    return action.payload;
  } else if (action.type === "ADD_ADMISSION") {
    admission.push(action.payload);
    let newAdmissions = admission;
    return newAdmissions;
  } else {
    return admission;
  }
};
const classes = function (classData = [], action) {
  if (action.type === "CLASS_DATA") {
    return action.payload;
  } else if (action.type === "ADD_CLASS") {
    classData.push(action.payload);
    let newClassData = classData;
    return newClassData;
  } else {
    return classData;
  }
};

const helpfaqs = function (
  faqData = [{ Category: "C", Question: "Q", Answer: "A" }],
  action
) {
  if (action.type === "FAQ_DATA") {
    return action.payload;
  } else {
    return faqData;
  }
};

const students = function (students = [], action) {
  //console.log("ADD_STUDENTS11111", action);
  if (action.type === "STUDENTS_DATA") {
    return action.payload;
  } else if (action.type === "ADD_STUDENTS") {
    //console.log("ADD_STUDENTS", action.payload);
    students.push(action.payload);
    let newStudents = students;
    return newStudents;
  } else {
    return students;
  }
};

const professorDataEdit = function (professorDataEdit = {}, action) {
  if (action.type === "PROFESSOR_DATA_EDIT") {
    return action.payload;
  } else {
    return professorDataEdit;
  }
};

const professors = function (professors = [], action) {
  if (action.type === "PROFESSORS_DATA") {
    return action.payload;
  } else if (action.type === "ADD_PROFESSORS") {
    return [];
  } else {
    return professors;
  }
};

const faqData = function (helpfaqs = [], action) {
  if (action.type === "FAQ_DATA") {
    return action.payload;
  } else if (action.type === "PUT_FAQ") {
    helpfaqs.push(action.payload);
    let newhelpfaqs = JSON.parse(JSON.stringify(helpfaqs));
    return newhelpfaqs;
  } else if (action.type === "LOAD_DATA") {
    return action.payload;
  } else {
    return helpfaqs;
  }
};

const editFaq = function (editFaq = {}, action) {
  if (action.type === "LOAD_FAQ_DATA") {
    return action.payload;
  } else {
    return editFaq;
  }
};

const professorProfile = function (professorData = null, action) {
  if (action.type === "PROFESSOR_DATA") {
    return action.payload;
  } else {
    return professorData;
  }
};

const alertUser = function (alert = {}, action) {
  if (action.type === "LOGIN_FAILED" || action.type === "REGISTER_FAILED") {
    let obj = { isAlert: true, msg: action.payload };
    return obj;
  }
  if (action.type === "ERROR_OCCURED") {
    let obj = { isAlert: true, msg: action.payload };
    return obj;
  }
  if (action.type === "REMOVE_ALERT") {
    let obj = { isAlert: false, msg: "" };
    return obj;
  }
  return alert;
};

const reducerAdminProfile = (state = {}, action) => {
  switch (action.type) {
    case "LOAD":
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

const reducerStudentProfile = (state = {}, action) => {
  switch (action.type) {
    case "LOAD_STUDENT":
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

const uploadedImage = (state = "", action) => {
  switch (action.type) {
    case "PROFILE_IMAGE":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};
const studentData = (state = {}, action) => {
  switch (action.type) {
    case "STUDENTS_DATA":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const contactsData = function (contacts = [], action) {
  if (action.type === "CONTACTS_DATA") {
    return action.payload;
  } else {
    return contacts;
  }
};

const reCallScheduleApi = function (state = false, action) {
  if (action.type === "SCHEDULE_CREATED") {
    console.log("TRUE CALLED");
    return true;
  } else if (action.type === "SCHEDULE_CREATED_DONE") {
    console.log("FALSE CALLED");
    return false;
  } else {
    console.log("FALSE2 CALLED");
    return false;
  }
};

const locations = function (state = [], action) {
  if (action.type === "LOCATIONS") {
    return action.payload;
  } else {
    return state;
  }
};

const subjects = function (state = [], action) {
  if (action.type === "SUBJECTS") {
    return action.payload;
  } else {
    return state;
  }
};

export default combineReducers({
  locations,
  subjects,
  contactsData,
  idToken,
  accessToken,
  refreshToken,
  isAuthenticated,
  userData,
  alertUser,
  form: formReducer,
  anotherUser,
  departments,
  admissions,
  classes,
  students,
  formInitialValues: reducerAdminProfile, //loads the initial values of admin profile
  studentProfileValues: reducerStudentProfile, //loads the initial values of students
  studentData,
  scheduleDataInformation,
  uploadedImage,
  professors,
  helpfaqs,
  professorProfile,
  professorDataEdit,
  professorsImages,
  faqData,
  editFaq,
  reCallScheduleApi,
  batch: batchReducer.reducer,
  result: resultReducer.reducer,
});

/*
this file is responsible for reloding the entire component when certain event occurs.
the dispatch from actions reaches over here, and then passed to the respective component.


*/
